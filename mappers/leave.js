
exports.toModel = (entity) => {
    const model = {
        
        id: entity.id,
        name: entity.firstName + " " + "" + entity.lastName,
        className: entity.className,
        classSection: entity.classSection,
        rollNo: entity.rollNo,
        reason: entity.reason,
        status: entity.status || 'pending',
        otp: entity.otp,
        leaveToken: entity.leaveToken
    }
    return model
}
