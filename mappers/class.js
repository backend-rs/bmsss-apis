
exports.toModel = (entity) => {
    const model = {
        id: entity.id,
    
        dueId: entity.dueId,
        feeChargesId:entity. feeChargesId,
        feeChargesName:entity.feeChargesName,
        userAdmissionId:entity.userAdmissionId,
        userRegId:entity.userRegId,
        dueAmount:entity.dueAmount,
        balanceAmount:entity. balanceAmount,
        month:entity.month,
        status:entity.status
    }
    return model
} 