'use strict'

exports.toModel = (entity) => {
    const model = {
        id: entity.id,
        regNo: entity.regNo,
        admissionNo: entity.admissionNo,
        session: entity.session,
        userType: entity.userType,
        session: entity.session,
        rollNo: entity.rollNo,
        
        userName:entity.userName,
        name: entity.firstName,
        firstName: entity.firstName,
        lastName: entity.lastName || ' ',
        email: entity.email,
        classId: entity.classId,
        className: entity.className,
        classSection: entity.classSection,

        classTeacher: entity.classTeacher,
        subjectTeacher: entity.subjectTeacher,
        classIncharge: entity.classIncharge,

        classGroup: entity.classGroup || 'pri',
        gender: entity.gender || 'male',
        dateOfBirth: entity.dateOfBirth,
        dateOfReg: entity.dateOfReg,
        dateOfAdmission: entity.dateOfAdmission,
        residenceAddress1: entity.residenceAddress1,
        residenceAddress2: entity.residenceAddress2,
        city: entity.city,
        district: entity.district,
        state: entity.state,
        pinCode: entity.pinCode,
        fatherName: entity.fatherName,
        motherName: entity.motherName,
        residenceMobile: entity.residenceMobile,
        status: entity.status,
        active: entity.active || 'true',
        // imageUrl: entity. imageUrl,
        department: entity.department || '',
        designation: entity.designation || '',
        image_url: entity.image_url,
        image_thumbnail: entity.image_thumbnail,
        resize_url: entity.resize_url,
        resize_thumbnail: entity.resize_thumbnail,
        // imageFile: entity.imageFile,
    }
    if (entity.lastName !== "string" || entity.lastName !== null || entity.lastName !== undefined) {
        model.name = `${model.name} ${entity.lastName ? entity.lastName : ''}`
    }

    return model
}

exports.toArrayListModel = (entity) => {
    const model = [
        entity.id,
        entity.firstName,
        entity.email || '',
        entity.mobile || '',


    ]
    return model
}
exports.toLoginModel = (entity) => {
    const model = {
        id: entity.id,
        regNo: entity.regNo,
        admissionNo: entity.admissionNo,
        userName:entity.userName,
        name: entity.firstName,
        classId: entity.classId,
        classSection: entity.classSection,
        email: entity.email,
        token: entity.token,
        userType: entity.userType,
        image_url: entity.image_url,
        image_thumbnail: entity.image_thumbnail,
        resize_url: entity.resize_url,
        resize_thumbnail: entity.resize_thumbnail,
        classIncharge: entity.classIncharge

    }
    if (entity.lastName !== "string" || entity.lastName !== null || entity.lastName !== undefined) {
        model.name = `${model.name} ${entity.lastName ? entity.lastName : ''}`
    }
    return model
}
exports.toSearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toModel(entity)
    })
}
exports.toArraySearchModel = (entities) => {
    return entities.map((entity) => {
        return exports.toArrayListModel(entity)
    })
}

// profileUrl: entity.profileUrl,
// licenecPicUrl: entity.licenecPicUrl,
// licenecNo: entity.licenecNo,
// nation        alIdNumber: entity.nationalIdNumber,
// accountBank: entity.accountBank,
// accountNo: entity.accountNo,
// isDriver: entity.isDriver || false,
// // if (entity.vehicle) {
//     model.vehicle = {
//         id: entity.vehicle.id,
//         title: entity.vehicle.title || '',
//         description: entity.vehicle.description || '',
//         vehicleNo: entity.vehicle.vehicleNo,
//         seater: entity.vehicle.seater,
//         brand: entity.vehicle.brand,
//         year: entity.vehicle.year,
//         model: entity.vehicle.model,
//         color: entity.vehicle.color,
//         vehicleLicenceNo: entity.vehicle.vehicleLicenceNo,
//     }
// }  // if (entity.vehicle) {
//     model.vehicle = {
//         id: entity.vehicle.id,
//         title: entity.vehicle.title || '',
//         description: entity.vehicle.description || '',
//         vehicleNo: entity.vehicle.vehicleNo,
//         seater: entity.vehicle.seater,
//         brand: entity.vehicle.brand,
//         year: entity.vehicle.year,
//         model: entity.vehicle.model,
//         color: entity.vehicle.color,
//         vehicleLicenceNo: entity.vehicle.vehicleLicenceNo,
//     }