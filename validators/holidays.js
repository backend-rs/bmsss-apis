'use strict'
const response = require('../exchange/response')

const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:holidays:create')

    if (!req.body.month) {
        log.end()
        return response.failure(res, ' month  is required')
    }
    if (!req.body.isEvent) {
        log.end()
        return response.failure(res, ' isEvent is required')
    }
    log.end()
    return next()
}

const update = async (req, res, next) => {
    const log = req.context.logger.start('validators:holidays:update')

    if (!req.body.month) {
        log.end()
        return response.failure(res, ' month  is required')
    }
    if (!req.body.year) {
        log.end()
        return response.failure(res, ' year is required')
    }
    if (!req.body.dateOfMonth) {
        log.end()
        return response.failure(res, 'date  is required')
    }
    if (!req.body.title) {
        log.end()
        return response.failure(res, 'title is required')
    }

    if (!req.body.description) {
        log.end()
        return response.failure(res, 'description is required')
    }
    log.end()
    return next()
}


const get = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')
    }
    return next()
}


exports.create = create
exports.get = get
exports.update=update
