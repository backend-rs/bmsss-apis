'use strict'
const response = require('../exchange/response')

const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:classes:create')
        if (req.body.name) {
            const classes = await db.class.find({
                where: {
                    name: req.body.name
                }
            })
            if (classes) {
                return response.failure(res, 'name is Already Registered')
            }
        }
    
    log.end()
    return next()
}



const update = (req, res, next) => {
    const log = req.context.logger.start('validators:classes:update')

    if (!req.params && !req.params.id) {
        log.end()
        return response.failure(res, 'id is required')
    }
  
    

 log.end()
    return next()
}
exports.create=create
exports.update=update
