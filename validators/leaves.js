'use strict'
const response = require('../exchange/response')
const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:leaves:create')
    if (!req.body.userId) {
        log.end()
        return response.failure(res, 'userId is required')
    }
    if (!req.body.leaveType) {
        log.end()
        return response.failure(res, 'leaveType is required')
    }
    if (req.body.leaveType == 'fullDay') {
        if (!req.body.fullDayType) {
            log.end()
            return response.failure(res, 'fullDayType is required')
        }
    }
    if (req.body.fullDayType != null && req.body.fullDayType != undefined && req.body.fullDayType != " ") {
        if (!req.body.fromDate) {
            log.end()
            return response.failure(res, 'fromDate is required')
        }
        if (!req.body.toDate) {
            log.end()
            return response.failure(res, 'toDate is required')
        }
    }
    if (!req.body.reason) {
        log.end()
        return response.failure(res, 'reason is required')
    }
    log.end()
    return next()
}



const verifyOtp = async (req, res, next) => {
    const log = req.context.logger.start('validators:leaves:create')
    if (!req.body.otp) {
        log.end()
        return response.failure(res, 'otp is required')
    }
    if (!req.body.status) {
        log.end()
        return response.failure(res, 'status is required')
    }
    if (!req.body.otp) {
        log.end()
        return response.failure(res, 'leaveToken is required')
    }

    log.end()
    return next()
}



exports.create = create
exports.verifyOtp = verifyOtp