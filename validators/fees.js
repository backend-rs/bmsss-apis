'use strict'
const response = require('../exchange/response')

const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:fees:create')

    if (!req.body.feeChargesId) {
        log.end()
        return response.failure(res, ' feeCharges  is required')
    }
    if (!req.body.admissionNo) {
        log.end()
        return response.failure(res, ' admissionNo is required')
    }
    log.end()
    return next()
}

const update = async (req, res, next) => {
    const log = req.context.logger.start('validators:holidays:update')
    if (!req.params && !req.params.id) {
        return response.failure(res, ' fee id is required')
    }
    if (!req.body.feeChargesId) {
        log.end()
        return response.failure(res, ' fee charges is required')
    }
    if (!req.body.admissionNo) {
        log.end()
        return response.failure(res, 'admission number  is required')
    }
   
    log.end()
    return next()
}




exports.create = create

exports.update=update
