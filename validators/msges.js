'use strict'
const response = require('../exchange/response')
const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:msges:create')
    if (!req.body.userType) {
        return response.failure(res, 'userType is required')
    } else if (req.body.userType == 'student') {
        if (!req.body.classId) {
            log.end()
            return response.failure(res, 'classId is required')
        }
        if (!req.body.askedBy) {
            log.end()
            return response.failure(res, 'askedId is required')
        }
    } else {
        if (!req.body.classes) {
            log.end()
            return response.failure(res, 'classes is required')
        }
    }
    if (!req.body.question) {
        log.end()
        return response.failure(res, 'question is required')
    }
    log.end()
    return next()
}



const sendMsg = async (req, res, next) => {
    const log = req.context.logger.start('validators:msges:create')
    if (!req.body.repliedBy) {
        log.end()
        return response.failure(res, 'reply id is required')
    }
    if (!req.body.id) {
        log.end()
        return response.failure(res, 'msg id is required')
    }
    if (!req.body.answer) {
        log.end()
        return response.failure(res, 'answer is required')
    }

    log.end()
    return next()
}



exports.create = create
exports.sendMsg = sendMsg