'use strict'
const response = require('../exchange/response')

const create = async (req, res, next) => {
    const log = req.context.logger.start('validators:events:create')
    if (!req.body.name) {
        log.end()
        return response.failure(res, 'name is required')
    }

    log.end()
    return next()
}
const getById = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')
    }
    return next()
}

const update = (req, res, next) => {
    if (!req.params && !req.params.id) {
        return response.failure(res, 'id is required')
    }
    return next()
}
exports.create = create
exports.getById = getById
exports.update = update