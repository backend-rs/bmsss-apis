'use strict'
const response = require('../exchange/response')


const login = (req, res, next) => {
    const log = req.context.logger.start('validators:users:login')
    if (!req.body.email&&!req.body.userName) {
        log.end()
        return response.failure(res, 'Email or UserName is required')
    }

    if (!req.body.password) {
        log.end()
        return response.failure(res, 'Password is required')
    }
    log.end()
    return next()
}

const get = (req, res, next) => {
    const log = req.context.logger.start('validators:users:get')

    if (!req.params && !req.params.id) {
        log.end()
        return response.failure(res, 'id is required')
    }
    log.end()
    return next()
}



const resetPassword = (req, res, next) => {
    const log = req.context.logger.start('validators:users:resetPassword')

    if (!req.body.password) {
        log.end()
        return response.failure(res, 'password is required')
    }

    log.end()
    return next()
}


exports.get = get
exports.login = login
exports.resetPassword = resetPassword

