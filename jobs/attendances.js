'use strict'
const cron = require('cron').CronJob
const service = require('../services/attendances')
const attendanceRecordService = require('../services/attendanceRecords')
const configService = require('../services/configs')

// function to get list of sunday of year
const listOfSunday = async (date) => {
    //Declaring array for inserting Sundays
    var sun = [];

    for (var i = 0; i < 12; i++) {

        var totalDays = new Date(date.getFullYear(), i, 0).getDate();

        //looping through days in month
        for (var j = 1; j <= totalDays; j++) {
            var newDate = new Date(date.getFullYear(), i, j)

            //if Sunday
            if (newDate.getDay() == 0) {
                sun.push(newDate);
            }

        }
    }

    return sun;
}

const start = async (startOn) => {
    let job = new cron({
        cronTime: startOn,
        onTick: async () => {

            console.log('running a task at 12 am daily.');

            let sundayList
            let rowExist = {}
            let isRowExist = false
            let isTableExist = false
            let isSundayExist = false

            let holidays
            let monthOfHolidays = {}
            let monthOfHolidayName = []
            let tempMonth
            let isHolidayExist = false
            let index = -1
            let isIndexExist = false

            // table name creation
            let dateObj = new Date()

            let date = dateObj.getDate()
            date = date < 10 ? `0${date}` : date

            let monthName = dateObj.getMonth()
            let month = dateObj.getMonth() + 1
            month = month < 10 ? `0${month}` : month

            let year = dateObj.getFullYear()
            let tempDate = date + "" + month + "" + year;

            let attendanceTable = `attendance${tempDate}`

            // get or create refrence to attendanceRecords table
            await attendanceRecordService.create({
                'tableName': attendanceTable
            })
            // table create
            await sequelize.query(`CREATE TABLE IF NOT EXISTS ${attendanceTable} (id int NOT NULL AUTO_INCREMENT,userId VARCHAR(255),classId VARCHAR(255), section VARCHAR(255),name VARCHAR(255),rollNo VARCHAR(255),status VARCHAR(255), PRIMARY KEY (id))`)
                .then(i => {
                    console.log("table created with table nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", attendanceTable)
                    isTableExist = true
                    return i
                }).catch(err => {
                    response.failure(res, err)
                })

            // after table creation
            let attendance
            if (isTableExist) {

                // find all students
                let users = await db.user.findAll({
                    where: {
                        userType: "student",
                        active: 'true'
                    }
                })
                if (!users) {
                    console.error('User not found')
                    return
                }

                // call listOfSunday fun to get all sundays
                sundayList = await listOfSunday(dateObj)

                // looping through each sunday to mark weekEnd in attendance list
                for (const sundayItem of sundayList) {
                    if (sundayItem) {
                        if (((sundayItem.getMonth() + 1) == month) && ((sundayItem.getDate()) == date)) {
                            isSundayExist = true
                        }
                    }
                }

                // find holidays to mark holiday in attendance
                holidays = await db.holiday.find({
                    where: {
                        year: year
                    }
                })
                if (holidays) {

                    // put all holidayMonths in an object
                    monthOfHolidays = {
                        january: JSON.parse(holidays.january),
                        february: JSON.parse(holidays.february),
                        march: JSON.parse(holidays.march),
                        april: JSON.parse(holidays.april),
                        may: JSON.parse(holidays.may),
                        june: JSON.parse(holidays.june),
                        july: JSON.parse(holidays.july),
                        august: JSON.parse(holidays.august),
                        september: JSON.parse(holidays.september),
                        october: JSON.parse(holidays.october),
                        november: JSON.parse(holidays.november),
                        december: JSON.parse(holidays.december)
                    }
                    // console.log(monthOfHolidays)

                    // put every month from monthOfHolidays into an array named monthOfHolidayName
                    monthOfHolidayName = [monthOfHolidays.january, monthOfHolidays.february, monthOfHolidays.march, monthOfHolidays.april, monthOfHolidays.may, monthOfHolidays.june, monthOfHolidays.july, monthOfHolidays.august, monthOfHolidays.september, monthOfHolidays.october, monthOfHolidays.november, monthOfHolidays.december]

                    // iterate through array(monthOfHolidayName) to match month
                    for (const item of monthOfHolidayName) {
                        if (item) {
                            index = monthOfHolidayName.indexOf(item)

                            // if month match return true
                            if (monthName == index) {
                                isIndexExist = true
                            }
                        }
                    }

                    if (isIndexExist) {

                        // take matched index of array monthOfHolidayName
                        tempMonth = monthOfHolidayName[monthName]

                        if (tempMonth) {
                            // iterate through array(tempMonth) to match date
                            for (const monthItem of tempMonth) {
                                if (monthItem) {
                                    let fromDateParts
                                    let fromHolidayDate
                                    let fromHolidayMonth
                                    let fromHolidayYear
                                    let toDateParts
                                    let toHolidayDate
                                    let toHolidayMonth
                                    let toHolidayYear
                                    if (monthItem.type == "single") {
                                        // split fromDate
                                        fromDateParts = monthItem.fromDate.split('/')
                                        fromHolidayDate = fromDateParts[0]
                                        fromHolidayMonth = fromDateParts[1]
                                        fromHolidayYear = fromDateParts[2]
                                        // if date match return true
                                        if (fromHolidayDate == date) {
                                            isHolidayExist = true
                                        }
                                    }
                                    if (monthItem.type == "multiple") {
                                        // split fromDate
                                        fromDateParts = monthItem.fromDate.split('/')
                                        fromHolidayDate = fromDateParts[0]
                                        fromHolidayMonth = fromDateParts[1]
                                        fromHolidayYear = fromDateParts[2]

                                        // split toDate
                                        toDateParts = monthItem.toDate.split('/')
                                        toHolidayDate = toDateParts[0]
                                        toHolidayMonth = toDateParts[1]
                                        toHolidayYear = toDateParts[2]
                                        for (let i = fromHolidayYear; i <= fromHolidayYear; i++) {
                                            if (i == year) {
                                                for (let j = fromHolidayMonth; j <= toHolidayMonth; j++) {
                                                    // let holidayMonth = i < 10 ? `0${i}` : i
                                                    if (j == month) {
                                                        for (let k = fromHolidayDate; k <= toHolidayDate; k++) {
                                                            // let holidayDate = j < 10 ? `0${j}` : j
                                                            if (k == date) {
                                                                isHolidayExist = true
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // looping through each student to mark atendance
                for (const user of users) {
                    if (user) {
                        let temp = {}
                        temp.userId = user.id,
                            temp.name = user.firstName + " " + user.lastName,
                            temp.rollNo = user.rollNo,
                            temp.classId = user.classId,
                            temp.section = user.classSection
                        // temp.percentage = totalPercentage

                        if ((isSundayExist == true) && (isHolidayExist == false)) {
                            // if sunday
                            temp.status = 'weekEnd'
                        } else if ((isSundayExist == false) && (isHolidayExist == true)) {
                            // if holiday
                            temp.status = 'holiday'
                        } else if ((isSundayExist == true) && (isHolidayExist == true)) {
                            // if sunday & holiday
                            temp.status = 'holiday'
                        } else {
                            // if no sunday & holiday
                            temp.status = 'absent'
                        }

                        await sequelize.query(`SELECT userId, classId,section,rollNo  FROM  ${attendanceTable} WHERE userId='${temp.userId}' AND classId= '${temp.classId}' AND section='${temp.section}' AND rollNo='${temp.rollNo}'`, {
                                type: Sequelize.QueryTypes.SELECT
                            })
                            .then(i => {
                                console.log("In getUserAttendance if exists", i[0])
                                isRowExist = true
                                rowExist = i[0]
                                return i
                            }).catch(err => {
                                response.failure(res, err)
                            })
                        if (rowExist == null && rowExist == undefined) {
                            console.log("In create attendance if not exist", temp)
                            attendance = await service.create(temp, attendanceTable)
                            console.log(attendance)
                        }
                    }
                }
                if ((isSundayExist == false) && (isHolidayExist == false)) {
                    let workingDaysUpdatedAt = date + "/" + month + "/" + year;

                    const context = {
                        logger: require('@open-age/logger')('permit:context:builder')
                    }
                    let config = await db.config.find({
                        where: {
                            organization: "bmsss"
                        }
                    })
                    let detail = {
                        schoolWorkingDays: true,
                        workingDaysUpdatedAt: workingDaysUpdatedAt
                    }
                    if (!config) {
                        throw new Error('orgination not found')
                    } else {
                        let configUpdate = await configService.update(config.id, detail, context)
                    }
                }
            }
        },
        start: true

    })

}
console.log('After job instantiation');
exports.schedule = () => {
    start(`0 5 * * *`)
}