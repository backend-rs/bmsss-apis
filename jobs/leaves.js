'use strict'
const cron = require('cron').CronJob
const service = require('../services/leaves')
const moment = require('moment');
const start = async (startOn) => {

    let job = new cron({
        cronTime: startOn,
        onTick: async () => {
            // console.log('running a task daily.');
            let now = moment();
            let lastDate = now.subtract('1', 'days').format("DD/MM/YYYY");
            console.log(lastDate);
            const leave = await db.leave.destroy({
                where: {
                    $or: [{
                        date: lastDate
                    }, {
                        toDate: lastDate
                    }]
                }
            })
            console.log(leave)
        },
        start: true
    })

}
console.log('After job instantiation');
exports.schedule = () => {
    // start(`0 1 * * * * *`)
    start(`0 2 * * *`)
}