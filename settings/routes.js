'use strict'

const fs = require('fs')
const api = require('../api')
const specs = require('../specs')
var auth = require('../permit')
const validator = require('../validators')
const fileUpload = require('express-fileupload');

var multipart = require('connect-multiparty')
var multipartMiddleware = multipart()


const configure = (app) => {
    //  const log = logger.start('settings:routes:configure')

    app.get('/specs', function (req, res) {
        fs.readFile('./public/specs.html', function (err, data) {
            if (err) {
                return res.json({
                    isSuccess: false,
                    error: err.toString()
                })
            }
            res.contentType('text/html')
            res.send(data)
        })
    })

    app.get('/api/specs', function (req, res) {
        res.contentType('application/json')
        res.send(specs.get())
    })
    // app.use(fileUpload());
    // app.use(multer({ dest: '../temp' }));

    // --------sessions api routes------------
    app.post('/api/sessions', auth.context.builder, auth.context.requiresToken, api.sessions.create)
    app.get('/api/sessions', auth.context.builder, auth.context.requiresToken, api.sessions.get)

    // -------------users routes-----------
    app.get('/api/users/search', auth.context.builder, auth.context.requiresToken, api.users.search)
    app.get('/api/users', auth.context.builder, auth.context.requiresToken, api.users.get)
    app.post('/api/users', multipartMiddleware, auth.context.builder, api.users.create)
    app.post('/api/users/otp', auth.context.builder, api.users.otp)
    app.put('/api/users/forgotPassword', auth.context.builder, api.users.forgotPassword)
    app.post('/api/users/resetPassword', auth.context.builder, auth.context.requiresToken, api.users.resetPassword)
    app.post('/api/users/otp/verify', auth.context.builder, api.users.otpVerify)
    app.get('/api/users/:id', auth.context.builder, auth.context.requiresToken, api.users.getById)
    app.put('/api/users/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.users.update)
    app.post('/api/users/login', auth.context.builder, validator.users.login, api.users.login)
    app.post('/api/users/logout', auth.context.requiresToken, api.users.logout)


    // ---------------- syllabus routes ----------------
    app.post('/api/syllabus', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.syllabus.create)
    app.get('/api/syllabus', auth.context.builder, auth.context.requiresToken, api.syllabus.get)
    app.get('/api/syllabus/:id', auth.context.builder, auth.context.requiresToken, api.syllabus.getById)
    app.put('/api/syllabus/updateOrCreate', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.syllabus.update)


    // ------------------------documents routes------------------------------
    app.post('/api/documents', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.documents.create)
    app.get('/api/documents/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.documents.getById)

    app.post('/api/documents/upload', multipartMiddleware, auth.context.requiresToken, api.documents.upload);



    // --------------------------------- subjects routes -----------------
    app.post('/api/subjects/create', auth.context.builder, validator.subjects.create, auth.context.requiresToken, api.subjects.create)
    app.get('/api/subjects/:id', auth.context.builder, auth.context.requiresToken, validator.subjects.getById, api.subjects.getById)
    app.get('/api/subjects', auth.context.builder, auth.context.requiresToken, api.subjects.get)
    app.put('/api/subjects/update/:id', auth.context.builder, validator.subjects.update, auth.context.requiresToken, api.subjects.update)
    app.delete('/api/subjects/delete/:id', auth.context.builder, auth.context.requiresToken, api.subjects.deleteSubject)



    // ------------------ classes routes -----------------------------------
    app.post('/api/classes/create', auth.context.builder, auth.context.requiresToken, validator.classes.create, api.classes.create)
    app.get('/api/classes/:id', auth.context.builder, auth.context.requiresToken, api.classes.getById)
    app.get('/api/classes', auth.context.builder, auth.context.requiresToken, api.classes.get)
    app.put('/api/classes/update/:id', auth.context.builder, auth.context.requiresToken, validator.classes.create, api.classes.update)
    app.delete('/api/classes/delete/:id', auth.context.builder, auth.context.requiresToken, api.classes.deleteClass)

    // ................................................sections..................................
    app.post('/api/sections', auth.context.builder, validator.sections.create, auth.context.requiresToken, api.sections.create)
    app.get('/api/sections/:id', auth.context.builder, auth.context.requiresToken, validator.sections.getById, api.sections.getById)
    app.get('/api/sections', auth.context.builder, auth.context.requiresToken, api.sections.get)
    app.put('/api/sections/update/:id', auth.context.builder, validator.sections.update, auth.context.requiresToken, api.sections.update)
    //--------------------- homeworks routes ------------------------------
    app.post('/api/homeworks/create', auth.context.builder, auth.context.requiresToken, api.homeworks.create)
    app.get('/api/homeworks/:id', auth.context.builder, auth.context.requiresToken, api.homeworks.getById)
    app.get('/api/homeworks', auth.context.builder, auth.context.requiresToken, api.homeworks.get)
    app.put('/api/homeworks/update/:id', auth.context.builder, auth.context.requiresToken, api.homeworks.update)
    app.delete('/api/homeworks/delete/:id', auth.context.builder, auth.context.requiresToken, api.homeworks.deleteHomework)



    //------------------ attendances routes ---------------------------
    app.post('/api/attendances', auth.context.builder, auth.context.requiresToken, api.attendances.create)
    // app.get('/api/attendances', auth.context.builder, api.attendances.get)
    // app.get('/api/attendances/:id', auth.context.builder, api.attendances.getById)
    // app.put('/api/attendances/update/:id', auth.context.builder, api.attendances.update)

    //------------------ attendencesRecords routes ---------------------------
    app.post('/api/attendanceRecords', auth.context.builder, auth.context.requiresToken, api.attendanceRecords.create)
    // app.get('/api/attendanceRecords', auth.context.builder,auth.context.requiresToken, api.attendanceRecords.get)
    app.get('/api/attendanceRecords/search', auth.context.builder, auth.context.requiresToken, api.attendanceRecords.search)
    // app.put('/api/attendanceRecords/update', auth.context.builder,auth.context.requiresToken, api.attendanceRecords.update)
    app.put('/api/attendanceRecords/update', auth.context.builder, api.attendanceRecords.update)


    //--------------------------------- events routes ------------------------------------------
    app.post('/api/events', multipartMiddleware, auth.context.builder, auth.context.requiresToken, validator.events.create, api.events.create)
    app.get('/api/events', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.events.get)
    app.get('/api/events/:id', multipartMiddleware, auth.context.builder, validator.events.getById, auth.context.requiresToken, api.events.getById)
    app.put('/api/events/update/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, validator.events.update, api.events.update)
    app.delete('/api/events/delete/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.events.deleteFile)
    app.post('/api/events/upload', multipartMiddleware, auth.context.requiresToken, api.events.upload);


    //--------------------------file routes -----------------------------------------
    app.post('/api/events/files', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.files.create)
    app.get('/api/events/files/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.files.getById)
    app.get('/api/files', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.files.getByEvent)
    app.put('/api/events/files/update/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.files.update)
    app.delete('/api/events/files/delete/:id', multipartMiddleware, auth.context.builder, auth.context.requiresToken, api.files.deleteFile)


    //  --------holidays api routes---------------------------------------
    app.post('/api/holidays', auth.context.builder, validator.holidays.create, auth.context.requiresToken, api.holidays.create)
    app.put('/api/holidays', auth.context.builder, auth.context.requiresToken, api.holidays.update)
    app.get('/api/holidays/:id', auth.context.builder, auth.context.requiresToken, api.holidays.getById)
    app.get('/api/holidays', auth.context.builder, auth.context.requiresToken, api.holidays.get)
    app.delete('/api/holidays/delete', auth.context.builder, auth.context.requiresToken, api.holidays.deleteHoliday)


    // -----------birthday api routes-------------
    app.post('/api/birthdays', auth.context.builder, auth.context.requiresToken, api.birthdays.create)
    app.get('/api/birthdays/:id', auth.context.builder, auth.context.requiresToken, api.birthdays.getById)
    app.get('/api/birthdays', auth.context.builder, auth.context.requiresToken, api.birthdays.get)



    // -------------messages routes------------------
    app.post('/api/msges', auth.context.builder, validator.msges.create, auth.context.requiresToken, api.messages.create)
    app.post('/api/msges/sendMsg', auth.context.builder, validator.msges.sendMsg, auth.context.requiresToken, api.messages.sendMsg)
    app.get('/api/msges/:id', auth.context.builder, auth.context.requiresToken, api.messages.getById)
    app.get('/api/msges', auth.context.builder, auth.context.requiresToken, api.messages.get)


    // --------leaves api routes------------
    app.post('/api/leaves', auth.context.builder, validator.leaves.create, auth.context.requiresToken, api.leaves.create)
    app.get('/api/leaves/:id', auth.context.builder, auth.context.requiresToken, api.leaves.getById)
    app.post('/api/leaves/verifyOtp', auth.context.builder, validator.leaves.verifyOtp, api.leaves.verifyOtp)
    app.get('/api/leaves', auth.context.builder, auth.context.requiresToken, api.leaves.get)
    app.put('/api/leaves/update/:id', auth.context.builder,  auth.context.requiresToken, api.leaves.update)

    // --------------fees api routes-----------------
    app.post('/api/fees', auth.context.builder, validator.fees.create, auth.context.requiresToken, api.fees.create)
    app.get('/api/fees/:id', auth.context.builder, auth.context.requiresToken, api.fees.getById)
    app.put('/api/fees/:id', auth.context.builder, validator.fees.update, auth.context.requiresToken, api.fees.update)
    app.get('/api/fees', auth.context.builder, auth.context.requiresToken, api.fees.get)


    // --------------feeCharges api routes-----------------
    app.post('/api/feeCharges', auth.context.builder, auth.context.requiresToken, api.feeCharges.create)
    app.get('/api/feeCharges/:id', auth.context.builder, auth.context.requiresToken, api.feeCharges.getById)
    app.put('/api/feeCharges/:id', auth.context.builder, auth.context.requiresToken, api.feeCharges.update)
    app.get('/api/feeCharges', auth.context.builder, auth.context.requiresToken, api.feeCharges.get)
    app.delete('/api/feeCharges/delete/:id', auth.context.builder, auth.context.requiresToken, api.feeCharges.deleteFeeCharges)



    // --------------feeReceipts api routes-----------------
    app.post('/api/feeReceipts', auth.context.builder, auth.context.requiresToken, api.feeReceipts.create)
    //  app.get('/api/feeReceipts/:id', auth.context.builder, api.feeReceipts.getById)
    app.get('/api/feeReceipts', auth.context.builder, auth.context.requiresToken, api.feeReceipts.get)
    // log.end()

    // --------------------config api routes------------------------
    app.post('/api/configs', auth.context.builder, api.configs.create)
    app.put('/api/configs', auth.context.builder, api.configs.update)
    app.get('/api/configs', auth.context.builder, api.configs.get)
}

exports.configure = configure