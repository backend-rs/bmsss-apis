'use strict'

const express = require('express')
const appConfig = require('config').get('app')
const logger = require('@open-age/logger')('server')
const port = process.env.PORT || appConfig.port || 4000


const app = express()
app.use((err, req, res, next) => {
    if (err) {
        (res.log || log).error(err.stack)
        return res.send(500, {
            error: 'something blew up!'
        })
    }
    next();
})
 app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
  });

const bodyParser = require('body-parser');
// app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json())

app.use(bodyParser.json({limit: "1gb"}));
app.use(bodyParser.text({ limit: '1gb', type: 'text/plain' }));


const boot = () => {
    const log = logger.start('app:boot')
    console.log(`environment:  ${process.env.NODE_ENV}`)
    console.log('starting server')
    app.listen(port, () => {
        console.log(`listening on port: ${port}`)
        log.end()
    })
}

const init = async () => {
    await require('./settings/database').configure(logger)
    await require('./settings/express').configure(app,logger)
    await require('./settings/routes').configure(app,logger)
    await require('./jobs/attendances').schedule()
    await require('./jobs/leaves').schedule()
    
    
    boot()
}

init()
