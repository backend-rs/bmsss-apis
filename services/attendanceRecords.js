'use strict'
const service = require('../services/users')
const configService = require('../services/configs')
const response = require('../exchange/response')

// limit for array
function limit(c) {
    return this.filter((x, i) => {
        if (i <= (c - 1)) {
            return true
        }
    })
}
Array.prototype.limit = limit;

// skip for array
function skip(c) {
    return this.filter((x, i) => {
        if (i > (c - 1)) {
            return true
        }
    })
}
Array.prototype.skip = skip;

// create student annual attendance percentage
const createPercentage = async (userId, context) => {

    let attendancePercentage = {}
    // find user
    let user = await db.user.find({
        where: {
            id: userId
        }
    })
    if (!user) {
        throw new Error('user not found')
    }
    // find config
    let config = await db.config.find({
        where: {
            organization: "bmsss"
        }
    })
    if (!config) {
        throw new Error('orgination not found')
    }

    var studentWorkingDays = user.studentWorkingDays
    var schoolWorkingDays = config.schoolWorkingDays
    let percentageCount = (studentWorkingDays / schoolWorkingDays) * 100
    percentageCount = percentageCount.toPrecision(4)
    let parts = percentageCount.split('.')
    let perTemp1 = parts[0]
    let perTemp2 = parts[1]
    if (perTemp1 < 10) {
        perTemp1 = `0${perTemp1}`
        perTemp2 = perTemp2.substring(1, 3)
        percentageCount = perTemp1 + "." + perTemp2
    } else {
        percentageCount = percentageCount
    }

    console.log(percentageCount)

    attendancePercentage.studentWorkingDays = studentWorkingDays
    attendancePercentage.schoolWorkingDays = schoolWorkingDays
    attendancePercentage.percentageCount = percentageCount

    return attendancePercentage
}
// model pass in response during get students list
const attendanceListResult = async (totalCount, listOfStudents) => {
    var result = {
        totalCount: totalCount,
        attendance: listOfStudents
    }
    return result
}

// model pass in response during get single student attendance
const singleStudentAttendanceList = async (attendancePercentage, monthlyAttendance) => {
    var result = {
        attendance: monthlyAttendance,
        annualPercentage: attendancePercentage.percentageCount,
        schoolWorkingDays: attendancePercentage.schoolWorkingDays,
        studentWorkingDays: attendancePercentage.studentWorkingDays
    }
    return result
}


const build = async (model, context) => {
    // const log = context.logger.start('services/attendanceRecords/build')
    const attendanceRecord = await db.attendanceRecord.build({
        tableName: model.tableName
    }).save()
    // log.end();
    return attendanceRecord
}

const create = async (model, context) => {
    // const log = context.logger.start('services/attendanceRecords/create')
    const isExist = await db.attendanceRecord.findOne({
        where: {
            "tableName": model.tableName
        }
    })
    if (isExist) {
        return isExist
    }
    const attendanceRecord = await build(model, context)
    // log.end();
    return attendanceRecord
}


const search = async (req, context, res) => {
    const log = context.logger.start('services/attendanceRecord/search')

    let params = req.query
    let isTableExist = false
    let attendanceRecord
    let temp = []
    let studentsList = []
    let classes
    let monthlyAttendance = []
    let monthlyDate
    let dateSearch
    let monthYearSearchTables

    // user should be active
    if (context.user.active && context.user.active == "true") {
        // for student
        if (params.monthYear && (params.monthYear !== undefined || params.monthYear !== null)) {
            if (context.user.userType && (context.user.userType === 'student')) {

                let monthYear = params.monthYear
                monthYearSearchTables = monthYear.replace(/\//g, '')

                //find tables of a particular month
                await sequelize.query(`SELECT tableName FROM attendanceRecords WHERE tableName LIKE '%${monthYearSearchTables}%'`, {
                        type: Sequelize.QueryTypes.SELECT
                    })
                    .then(i => {
                        isTableExist = true
                        // console.log(i)
                        temp = i
                        return i
                    }).catch(err => {
                        response.failure(res, 'attendance does not exist for given month ')
                    })

                // list of tables of a month
                if (temp && temp.length !== 0) {
                    for (const item of temp) {
                        if (item.tableName) {

                            // date of a particular month
                            monthlyDate = item.tableName.substring(10, 12)

                            await sequelize.query(`SELECT * FROM ${item.tableName} WHERE userId= '${context.user.id}'`, {
                                    type: Sequelize.QueryTypes.SELECT
                                })
                                .then(i => {
                                    isTableExist = true
                                    // console.log(i)
                                    monthlyAttendance.push({
                                        status: i[0].status,
                                        date: monthlyDate
                                    })
                                    return i
                                }).catch(err => {
                                    console.log('errorrrrrrrrrrrrrr')
                                    response.failure(res, 'student attendance does not exist')
                                })

                            if (isTableExist = true) {
                                monthlyAttendance.sort(function (a, b) {
                                    return a.date - b.date;
                                });
                            }
                        }
                    }

                    let attendancePercentage = await createPercentage(context.user.id, context)
                    attendanceRecord = await singleStudentAttendanceList(attendancePercentage, monthlyAttendance)

                } else {
                    throw new Error('attendance does not exist for given month')
                }
            } else {
                throw new Error('user should be student')
            }
        } else { // for teacher & admin

            // add pagging
            const offset = parseInt(params.count || 10) * (parseInt(params.pageNo || 1) - 1)
            const limit = parseInt(params.count || 10)

            // date in params 
            if (params.date && (params.date !== undefined || params.date !== null)) {
                let tempDate = params.date
                dateSearch = tempDate.replace(/\//g, '')
            } else {
                // if no date in params then current date
                let dateObj = new Date()

                let date = dateObj.getDate()
                date = date < 10 ? `0${date}` : date

                let month = dateObj.getMonth() + 1
                month = month < 10 ? `0${month}` : month

                let year = dateObj.getFullYear()
                dateSearch = date + "" + month + "" + year;
            }
            //table name
            const tableName = `attendance${dateSearch}`

            // search table in attendanceRecord
            attendanceRecord = await db.attendanceRecord.findOne({
                where: {
                    tableName: tableName
                }
            })
            if (attendanceRecord) {
                // if userType=teacher||userType=admin
                if (context.user.userType && (context.user.userType === 'teacher' || context.user.userType === 'admin')) {

                    // if classId,section in params
                    if (params.classId && (params.classId !== undefined || params.classId !== null) && params.section && (params.section !== undefined || params.section !== null) && (params.rollNo == undefined || params.rollNo == null)) {

                        // get list of students of an particular class/section
                        await sequelize.query(`SELECT * FROM ${attendanceRecord.tableName} WHERE classId= '${params.classId}' AND section='${params.section}' ORDER BY cast(rollNo as int) `, {
                                type: Sequelize.QueryTypes.SELECT
                            })
                            .then(i => {
                                isTableExist = true
                                console.log(i)
                                temp = i
                                return i
                            }).catch(err => {
                                response.failure(res, 'attendance not found for given class/section')
                            })

                        if (isTableExist = true) {
                            //get class name from class table for particular student from list
                            for (const item of temp) {
                                if (item) {
                                    classes = await db.class.find({
                                        where: {
                                            id: item.classId
                                        }
                                    })
                                    if (!classes) {
                                        throw new Error('Class not found')
                                    }

                                    // calculate annual student attendance percentage
                                    let attendancePercentage = await createPercentage(item.userId, context)
                                    studentsList.push({
                                        id: item.id,
                                        userId: item.userId,
                                        classId: item.classId,
                                        className: classes.name,
                                        section: item.section,
                                        name: item.name,
                                        rollNo: item.rollNo,
                                        status: item.status,
                                        studentWorkingDays: attendancePercentage.studentWorkingDays,
                                        schoolWorkingDays: attendancePercentage.schoolWorkingDays,
                                        percentageCount: attendancePercentage.percentageCount
                                    })
                                }
                            }
                            // total no. of student class/section
                            let totalCount = studentsList.length

                            // applied paging on student list of particular class/section
                            let listOfStudents = studentsList.skip(offset).limit(limit)

                            attendanceRecord = await attendanceListResult(totalCount, listOfStudents)

                        }

                    } else if (params.classId && (params.classId !== undefined || params.classId !== null) && params.section && (params.section !== undefined || params.section !== null) && params.rollNo && (params.rollNo !== undefined || params.rollNo !== null)) {
                        // if classId ,section & rollNo in params

                        await sequelize.query(`SELECT * FROM ${attendanceRecord.tableName} WHERE classId= '${params.classId}' AND section='${params.section}' AND rollNo='${params.rollNo}'`, {
                                type: Sequelize.QueryTypes.SELECT
                            })
                            .then(i => {
                                isTableExist = true
                                temp = i[0]
                                return i
                            }).catch(err => {
                                response.failure(res, 'attendance not found for given rollNo')
                            })
                        if (isTableExist = true) {
                            if (temp) {
                                //get class name from class table for particular student 
                                classes = await db.class.find({
                                    where: {
                                        id: temp.classId
                                    }
                                })
                                if (!classes) {
                                    throw new Error('Class not found')
                                }

                                // calculate annual student attendance percentage
                                let attendancePercentage = await createPercentage(temp.userId, context)
                                studentsList.push({
                                    id: temp.id,
                                    userId: temp.userId,
                                    classId: temp.classId,
                                    className: classes.name,
                                    section: temp.section,
                                    name: temp.name,
                                    rollNo: temp.rollNo,
                                    status: temp.status,
                                    studentWorkingDays: attendancePercentage.studentWorkingDays,
                                    schoolWorkingDays: attendancePercentage.schoolWorkingDays,
                                    percentageCount: attendancePercentage.percentageCount
                                })

                            }
                            // total no. of student class/section
                            let totalCount = ''

                            attendanceRecord = await attendanceListResult(totalCount, studentsList)
                        }
                    }
                } else {
                    throw new Error('userType should be admin or teacher')
                }
            } else {
                throw new Error('attendance not found  for this date')
            }
        }
    } else {
        throw new Error('user should be active')
    }
    log.end();
    return attendanceRecord
}



const update = async (req, model, context) => {
    const log = context.logger.start(`services/attendanceRecord/update`)
    console.log('model for status updateeeeeeeeeeeeeeeeeeeeeeee:', model)
    let isUpdate = false
    let attendanceRecord
    let params = req.query
    let entity
    let tempDate = params.date
    let dateWithoutSlashes = tempDate.replace(/\//g, '')
    const tableName = `attendance${dateWithoutSlashes}`
    console.log('tablenameeeeeeeeeeeeee:', tableName)

    // get table according to date
    attendanceRecord = await db.attendanceRecord.findOne({
        where: {
            tableName: tableName
        }
    })
    console.log(attendanceRecord)

    if (attendanceRecord) {
        for (const item of model.attendance) {
            if (item) {
                console.log('user in modelllllllllllllll:', item)

                // select student from attendance table 
                await sequelize.query(`SELECT * FROM ${attendanceRecord.tableName}  WHERE classId= '${params.classId}' AND section='${params.section}' AND userId='${item.userId}'`, {
                        type: Sequelize.QueryTypes.SELECT
                    })
                    .then(student => {
                        isUpdate = true
                        console.log(student)
                        entity = student[0]
                        return student
                    }).catch(err => {
                        response.failure(res, 'student attedance not found for given date')
                    })

                // update student atttendence
                await sequelize.query(`UPDATE ${attendanceRecord.tableName}  SET status='${item.status}' WHERE classId= '${params.classId}' AND section='${params.section}' AND userId='${item.userId}'`, {
                        type: Sequelize.QueryTypes.UPDATE
                    })
                    .then(i => {
                        isUpdate = true
                        console.log(i)
                        // entity = i
                        return i
                    }).catch(err => {
                        response.failure(res, 'student attedance not found for given date')
                    })
                // if status in model  
                if (item.status) {
                    let detail = {
                        studentWorkingDays: true,
                        status: item.status,
                        workingDaysUpdatedAt: params.date,
                        beforeUpdate: entity
                    }
                    let user = await service.update(req, item.userId, detail, context)
                }

            }
        }
    } else {
        throw new Error('attendance not found for given date')
    }
    if (isUpdate == true) {
        log.end()
        // console.log()
        return 'Attendance successfully updated'
    }
    log.end();
    return attendanceRecord
}
exports.create = create
exports.search = search
exports.update = update