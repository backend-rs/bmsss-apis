const response = require('../exchange/response')
const set = async (model, feeCharge, context) => {
    const log = context.logger.start('services/feeCharges/set')
    if (model.headId) {
        feeCharge.headId = model.headId
    }
    if (model.headName) {
        feeCharge.headName = model.headName
    }
    log.end()
    await feeCharge.save()
    return feeCharge
}

const build = async (model, context) => {
    const log = context.logger.start('services/feeCharges/build')
    const feeCharges = await db.feeCharge.build({

        headId: model.headId,
        headName: model.headName


    }).save()
    log.end();

    return feeCharges
}

const create = async (model, context) => {
    const log = context.logger.start('services/feeCharges/create')

    const feeCharge = await build(model, context)
    // console.log(feeCharge);
    log.end()
    return feeCharge
}

const get = async (context) => {
    const log = context.logger.start(`services/feeCharges/get`)

    const feeCharge = await db.feeCharge.findAll({})
    log.end()
    return feeCharge
}


const getById = async (query, context) => {
    const log = context.logger.start(`services/feeCharges/get:${query}`)
    if (query.id) {
        const feeCharge = await db.feeCharge.findOne({
            where: {
                id: query.id
            }
        })
        return feeCharge
    }

    log.end()
    return null


}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/feeCharges/update`)

    let feeCharge = await db.feeCharge.findOne({ where: { id: parseInt(id) } })
    if (!feeCharge) {
        log.end()
        throw new Error('USER NOT FOUND')
    }
    else {
        let detail = await set(model.body, feeCharge, context)

        log.end()
        return detail.save()
    }

}

const deleteFeeCharges = async (id, context, res) => {
    const log = context.logger.start(`services/feeCharges/delete/id`)
    let feeCharge = await db.feeCharge.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!feeCharge) {
        log.end()
        throw new Error('feeCharges NOT FOUND')
    }
    else {
        feeCharge = await db.feeCharge.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }
    res.message = 'feeCharge removed sucessfully'
    log.end()
    return(res.message)
    // return response.data(res, '')
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteFeeCharges = deleteFeeCharges