'use strict'
const http = require('http')
const fs = require('fs')
const Jimp = require('jimp')
const path = require('path')
const appRoot = require('app-root-path')
const fileStoreConfig = require('config').get('providers.file-store')
const service = require('../services/files')
const response = require('../exchange/response')
var download = require('download-file')


const set = async (body, req, entity, context) => {
    const log = context.logger.start('services/classes/set')
    let data
    if (req) {
        if (req.files != null && req.files != undefined) {
            if (req.files.file != null && req.files.file != undefined) {

                if (body.shouldImageUpdate == "true") {

                    data = await upload(req.files.file)

                    if (body.name) {
                        entity.name = body.name
                    }
                    if (data.url) {
                        entity.image_url = data.url
                    }
                    if (data.thumbnail) {
                        entity.image_thumbnail = data.thumbnail
                    }
                    if (data.resize_url) {
                        entity.resize_url = data.resize_url
                    }
                    if (data.resize_thumbnail) {
                        entity.resize_thumbnail = data.resize_thumbnail
                    }
                } else {
                    if (body.name) {
                        entity.name = body.name       
                    }
                }
            }
        }
    }

    log.end()
    await entity.save()
    return entity
}

const build = async (req, body, context) => {
    const log = context.logger.start('services/events/build')
    let data = await upload(req.files.file)

    const event = await db.event.build({
        name: body.name,
        shouldImageUpdate: body.shouldImageUpdate || 'false',
        isUpdate: body.isUpdate || 'false',
        image_url: data.url,
        image_thumbnail: data.thumbnail,
        resize_url: data.resize_url,
        resize_thumbnail: data.resize_thumbnail
    }).save()

    log.end();
    return event
}

const create = async (req, body, context) => {
    const log = context.logger.start('services/events/create')
    let event
    if (body.isUpdate == "true") {
        event = await update(req, body.event_id, body, context)
        log.end();
    } else {
        event = await build(req, body, context)
        log.end();
    }
    return event
}

const get = async (context) => {
    const log = context.logger.start(`services/events/get`)

    const event = await db.event.findAll({})
    log.end()
    return event

}

const getById = async (query, context) => {
    const log = context.logger.start(`services/events/getById ${query.id}`)
    var req = {
        query
    }
    if (query.id) {
        const event = await db.event.find({
            where: {
                id: query.id
            }
        })

        let file = await service.getByEvent(req, context)
        event.files = file
        log.end();
        return event
    }
}

const update = async (req, id, body, context) => {
    const log = context.logger.start(`services/events/update/id`)
    const entity = await db.event.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new error('Invalid class')
    }
    var request = {
        query: {
            id: id
        }
    }
    let file = await service.getByEvent(request, req.context)
    let data = []
    if (file) {
        for (const item of file) {
            data.push({
                id: item.id
            })
        }
        entity.files = JSON.stringify(data)

    }
    let event = await set(body, req, entity, context)
    log.end();
    return event.save()
}

const deleteFile = async (id, context, res) => {
    const log = context.logger.start(`services/events/delete/id`)
    const event = await db.event.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (event) {
        await db.event.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }
    res.message = 'File removed sucessfully'
    log.end()
    return response.data(res, '')
}


const upload = async (file, context) => {

    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await move(file.path, destination)

    const thumbnail = await imagethumbnail(destination)

    file.path = destination
    const resize = await resizeImage(file, context)

    return {
        url: url,
        thumbnail: thumbnail,
        resize_url: resize.url,
        resize_thumbnail: resize.thumbnail
    }
}

const move = async (oldPath, newPath) => {
    const copy = (cb) => {
        var readStream = fs.createReadStream(oldPath)
        var writeStream = fs.createWriteStream(newPath)

        readStream.on('error', cb)
        writeStream.on('error', cb)

        readStream.on('close', function () {
            fs.unlink(oldPath, cb)
        })

        readStream.pipe(writeStream)
    }

    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy(err => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    })
                } else {
                    reject(err)
                }
            } else {
                resolve()
            }
        })
    })
}

const resizeImage = async (file, context) => {
    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await Jimp.read(file.path)
        .then(lenna => {
            return lenna
                .resize(256, 256) // resize
                .quality(60) // set JPEG quality
                // .greyscale() // set greyscale
                .write(path.join(destination)) // save
        })
        .catch(err => {
            throw new Error(err)
        })

    const thumbnail = await imagethumbnail(destination)

    return {
        url,
        destination,
        thumbnail
    }
}

const imagethumbnail = (path) => {
    if (!path) {
        return Promise.resolve(null)
    }

    return new Promise((resolve, reject) => {
        return Jimp.read(path).then(function (lenna) {
            if (!lenna) {
                return resolve(null)
            }
            var a = lenna.resize(15, 15) // resize
                .quality(50) // set JPEG quality
                .getBase64(Jimp.MIME_JPEG, function (result, base64, src) {
                    return resolve(base64)
                })
        }).catch(function (err) {
            reject(err)
        })
    })
}

exports.create = create
exports.upload = upload
exports.get = get
exports.getById = getById
exports.deleteFile = deleteFile
exports.update = update












 