'use strict'

const set = async (model, entity, context) => {
    const log = context.logger.start('services/sections/set')
    if (model.status) {
        entity.status = model.status
    }
    if (model.name) {
        entity.name = model.name.toLowerCase()
    }
    log.end()
    await entity.save()
    return entity
}

const build = async (model, context) => {
    const log = context.logger.start('services/sections/build')
    const section = await db.section.build({
        classId: model.classId,
        name: model.name.toLowerCase(),
        status: model.status || 'active'
    }).save()
    log.end()
    return section
}

const create = async (model, context) => {
    const log = context.logger.start('services/sections')
    let section
    if (model) {
        // get section according to class
        if (model.classId && model.name) {
            section = await db.section.findOne({
                where: {
                    name: model.name.toLowerCase(),
                    classId: model.classId
                }
            })
            //   create section
            if (!section) {
                section = await build(model, context)
            } else {
                throw new Error('section already exist')
            }
        }
    }

    log.end()
    return section
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/sections/getById/id ${query.id}`)
    if (query.id) {
        const section = await db.section.find({
            where: {
                id: query.id
            }
        })
        if (!section) {
            throw new Error('Section does not exist')
        }
        log.end()
        return section
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/sections/get`)
    let params = req.query
    let section
    // get section through status
    if (params && (params.status != null && params.status != undefined && params.classId == null && params.classId == undefined)) {
        section = await db.section.findAll({
            where: {
                status: params.status
            }
        })
        log.end()
        // get section through classId
    } else if (params && (params.classId != null && params.classId != undefined && params.status == null && params.status == undefined)) {
        section = await db.section.findAll({
            where: {
                classId: params.classId
            }
        })
        log.end()

        // get section through classId and status   
    } else if (params && (params.classId != null && params.classId != undefined && params.status !== null && params.status !== undefined)) {
        section = await db.section.findAll({
            where: {
                classId: params.classId,
                status: params.status
            }
        })
        log.end()

        // get all sections
    } else {
        section = await db.section.findAll({})
        log.end()
    }
    log.end()
    return section

}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/sections/update/id`)
    // find section by id
    let entity = await db.section.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new Error('invalid section')
    }
    // update section
    let section = await set(model.body, entity, context)
    log.end();
    return section.save()

}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update