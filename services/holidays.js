'use strict'
//  var job=require('../jobs/holidays')
const listOfSunday = async (date) => {
    // var date = new Date();
    var sun = []; //Declaring array for inserting Sundays

    for (var i = 0; i < 12; i++) {

        var totalDays = new Date(date.getFullYear(), i, 0).getDate();

        for (var j = 1; j <= totalDays; j++) { //looping through days in month
            var newDate = new Date(date.getFullYear(), i, j)
            if (newDate.getDay() == 0) { //if Sunday
                sun.push(newDate);
            }
        }
    }

    return sun;
}

const updateHoliday = (model, tempIndex, details) => {

    if (details.type) {
        tempIndex.type = details.type
    }
    if (model.toDate) {
        tempIndex.toDate = model.toDate
    }
    if (model.fromDate) {
        tempIndex.fromDate = model.fromDate
    }
    if (model.title) {
        tempIndex.title = model.title
    }
    if (model.description) {
        tempIndex.description = model.description
    }

    if (details.isEvent) {
        tempIndex.isEvent = details.isEvent
    }
    // tempIndex.save()
    return tempIndex
}

const set = async (model, details, holiday, context) => {
    const log = context.logger.start('services/holidays/set')
    var temp
    switch (model.month) {
        case 1:
            let jan = JSON.parse(holiday.january)
            let janIndex = jan.length
            if (janIndex == 0) {
                jan.push({
                    _id: janIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let janTempIndex= jan[janIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    jan.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = jan[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    jan.push({
                        _id: janTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });

                }
            }
            temp = JSON.stringify(jan)
            holiday.january = temp
            break;
        case 2:
            let feb = JSON.parse(holiday.february)
            let febIndex = feb.length
            if (febIndex == 0) {
                feb.push({
                    _id: febIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let febTempIndex= feb[febIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    feb.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = feb[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    feb.push({
                        _id: febTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }

            temp = JSON.stringify(feb)
            holiday.february = temp

            break;
        case 3:
            let mar = JSON.parse(holiday.march)
            let marIndex = mar.length
            if (marIndex == 0) {
                mar.push({
                    _id: marIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let marTempIndex= mar[marIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    mar.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = mar[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    mar.push({
                        _id: marTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(mar)
            holiday.march = temp
            break;
        case 4:
            let apr = JSON.parse(holiday.april)
            let aprIndex = apr.length
            if (index == 0) {
                apr.push({
                    _id: aprIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let aprTempIndex= apr[aprIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    apr.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = apr[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    apr.push({
                        _id: aprTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(apr)
            holiday.april = temp
            break;
        case 5:
            let mayAarry = JSON.parse(holiday.may)
            let mayIndex = mayAarry.length
            if (mayIndex == 0) {
                mayAarry.push({
                    _id: mayIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let mayTempIndex= may[mayIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    mayAarry.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = mayAarry[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    mayAarry.push({
                        _id: mayTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(mayAarry)
            holiday.may = temp
            break;
        case 6:
            let jun = JSON.parse(holiday.june)
            let junIndex = jun.length
            if (junIndex == 0) {
                jun.push({
                    _id: junIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let junTempIndex= jun[junIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    jun.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = jun[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    jun.push({
                        _id: junTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(jun)
            holiday.june = temp
            break;
        case 7:
            let jul = JSON.parse(holiday.july)
            let julIndex = jul.length
            if (julIndex == 0) {
                jul.push({
                    _id: julIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let julTempIndex= jul[julIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    jul.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = jul[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    jul.push({
                        _id: julTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(jul)
            holiday.july = temp
            break;
        case 8:
            let aug = JSON.parse(holiday.august)
            let augIndex = aug.length
            if (augIndex == 0) {
                aug.push({
                    _id: augIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let augTempIndex= aug[augIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    aug.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = aug[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    aug.push({
                        _id: augTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(aug)
            holiday.august = temp
            break;
        case 9:
            let sep = JSON.parse(holiday.september)
            let sepIndex = sep.length
            if (sepIndex == 0) {
                sep.push({
                    _id: sepIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let sepTempIndex= sep[sepIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    sep.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = sep[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    sep.push({
                        _id: sepTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(sep)
            holiday.september = temp
            break;
        case 10:
            let oct = JSON.parse(holiday.october)
            let octIndex = oct.length
            if (octIndex == 0) {
                oct.push({
                    _id: octIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let octTempIndex= oct[octIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    oct.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = oct[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    oct.push({
                        _id: octTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(oct)
            holiday.october = temp
            break;
        case 11:
            let nov = JSON.parse(holiday.november)
            let novIndex = nov.length
            if (novIndex == 0) {
                nov.push({
                    _id: novIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let novTempIndex= nov[novIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    nov.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = nov[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    nov.push({
                        _id: novTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }

            }
            temp = JSON.stringify(nov)
            holiday.november = temp
            break;
        case 12:
            let dec = JSON.parse(holiday.december)
            let decIndex = dec.length
            if (decIndex == 0) {
                dec.push({
                    _id: decIndex + 1,
                    type: details.type,
                    toDate: model.toDate,
                    fromDate: model.fromDate,
                    title: model.title,
                    description: model.description,
                    isEvent: details.isEvent
                });
            } else {
                // var doesExist;
                let decTempIndex= dec[decIndex-1]._id
                let exstingIndex;
                if (model._id && (model._id !== null && model._id !== undefined)) {
                    dec.forEach((holiday, index) => {
                        if (holiday._id == model._id) {
                            // doesExist = true;
                            exstingIndex = index;
                            let tempIndex = dec[exstingIndex]
                            updateHoliday(model, tempIndex, details)
                        }
                    });
                } else {
                    dec.push({
                        _id: decTempIndex + 1,
                        type: details.type,
                        toDate: model.toDate,
                        fromDate: model.fromDate,
                        title: model.title,
                        description: model.description,
                        isEvent: details.isEvent
                    });
                }
            }
            temp = JSON.stringify(dec)
            holiday.december = temp
            break;
        default:
            throw new error('Invalid month')
    }
    log.end()
    await holiday.save()
    return holiday;

}

const build = async (model, details, isLeapYear, context) => {
    const log = context.logger.start('services/holidays/build')

    switch (model.month) {
        case 1:
            var january = []
            var index = january.length
            january.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            break;

        case 2:
            var february = []
            var index = february.length
            february.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            february = JSON.stringify(february)
            break;
        case 3:
            var march = []
            var index = march.length
            march.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            march = JSON.stringify(march)
            break;

        case 4:
            var april = []
            var index = april.length
            april.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            april = JSON.stringify(april)
            break;
        case 5:
            var may = []
            var index = may.length
            may.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            may = JSON.stringify(may)
            break;
        case 6:
            var june = []
            var index = june.length
            june.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            june = JSON.stringify(june)
            break;
        case 7:
            var july = []
            var index = july.length
            july.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            july = JSON.stringify(july)
            break;
        case 8:
            var august = []
            var index = august.length
            august.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            august = JSON.stringify(august)
            break;
        case 9:
            var september = []
            var index = september.length
            september.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            september = JSON.stringify(september)
            break;
        case 10:
            var october = []
            var index = october.length
            october.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.desc,
                isEvent: details.isEvent
            })
            october = JSON.stringify(october)
            break;
        case 11:
            var november = []
            var index = november.length
            november.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            november = JSON.stringify(november)
            break;
        case 12:
            var december = []
            var index = december.length
            december.push({
                _id: index + 1,
                type: details.type,
                toDate: model.toDate,
                fromDate: model.fromDate,
                title: model.title,
                description: model.description,
                isEvent: details.isEvent
            })
            december = JSON.stringify(december)
            break;
        default:
            throw new error('Invalid month')
    }
    const holiday = await db.holiday.build({
        isLeapYear: isLeapYear,
        year: model.year,
        january: january || "[]",
        february: february || "[]",
        march: march || "[]",
        april: april || "[]",
        may: may || "[]",
        june: june || "[]",
        july: july || "[]",
        august: august || "[]",
        september: september || "[]",
        october: october || "[]",
        november: november || "[]",
        december: december || "[]",


    }).save()
    log.end()

    return holiday
}

const create = async (model, context) => {
    const log = context.logger.start('services/holidays/create')
    let isLeapYear
    let holiday

    if (model.year) {
        isLeapYear = (model.year % 100 === 0) ? (model.year % 400 === 0) : (model.year % 4 === 0);
        console.log(isLeapYear)
    }

    // create holiday
    holiday = await build(model, isLeapYear, context)
    log.end()
    return holiday
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/holidays/getById/id ${query.id}`)
    if (query.id) {
        const holiday = await db.holiday.find({
            where: {
                id: query.id,
            }
        })
        log.end()
        return holiday
    }
}

// get holidays by year
const createTempHoliday = async (holidays) => {
    let months = []
    let january = []
    let february = []
    let march = []
    let april = []
    let may = []
    let june = []
    let july = []
    let august = []
    let september = []
    let october = []
    let november = []
    let december = []
    if (holidays.january) {
        months.push(JSON.parse(holidays.january))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    january.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.february) {
        months.push(JSON.parse(holidays.february))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    february.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.march) {
        months.push(JSON.parse(holidays.march))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    march.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.april) {
        months.push(JSON.parse(holidays.april))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    april.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.may) {
        months.push(JSON.parse(holidays.may))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    may.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.june) {
        months.push(JSON.parse(holidays.june))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    june.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.july) {
        months.push(JSON.parse(holidays.july))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    july.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.august) {
        months.push(JSON.parse(holidays.august))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    august.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.september) {
        months.push(JSON.parse(holidays.september))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    september.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.october) {
        months.push(JSON.parse(holidays.october))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    october.push(item)
                }
            }
        }
        months = []

    }
    if (holidays.november) {
        months.push(JSON.parse(holidays.november))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    november.push(item)
                }
            }
        }
        months = []
    }
    if (holidays.december) {
        months.push(JSON.parse(holidays.december))

        for (const month of months) {
            for (const item of month) {
                if (item) {
                    december.push(item)
                }
            }
        }
        months = []
    }
    var tempHolidays = {
        year: holidays.year,
        january,
        february,
        march,
        april,
        may,
        june,
        july,
        august,
        september,
        october,
        november,
        december,

    }
    return tempHolidays
}

// get holidays by month
const createTempMonth = async (params, month) => {

    if (params.month == 'january') {
        let january = []
        for (const item of month) {
            if (item) {
                january.push(item)
            }
        }
        return january
    }
    if (params.month == 'february') {
        let february = []
        for (const item of month) {
            if (item) {
                february.push(item)
            }
        }
        return february
    }
    if (params.month == 'march') {
        let march = []
        for (const item of month) {
            if (item) {
                march.push(item)
            }
        }
        return march
    }
    if (params.month == 'april') {
        let april = []
        for (const item of month) {
            if (item) {
                april.push(item)
            }
        }
        return april
    }
    if (params.month == 'may') {
        let may = []
        for (const item of month) {
            if (item) {
                may.push(item)
            }
        }
        return may
    }
    if (params.month == 'june') {
        let june = []
        for (const item of month) {
            if (item) {
                june.push(item)
            }
        }
        return june
    }
    if (params.month == 'july') {
        let july = []
        for (const item of month) {
            if (item) {
                july.push(item)
            }
        }
        return july
    }
    if (params.month == 'august') {
        let august = []
        for (const item of month) {
            if (item) {
                august.push(item)
            }
        }
        return august
    }
    if (params.month == 'september') {
        let september = []
        for (const item of month) {
            if (item) {
                september.push(item)
            }
        }
        return september
    }
    if (params.month == 'october') {
        let october = []
        for (const item of month) {
            if (item) {
                october.push(item)
            }
        }
        return october

    }
    if (params.month == 'november') {
        let november = []
        for (const item of month) {
            if (item) {
                november.push(item)
            }
        }
        return november
    }
    if (params.month == 'december') {
        let december = []
        for (const item of month) {
            if (item) {
                december.push(item)
            }
        }
        return december
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/holidays/get`)

    let params = req.query
    var holidays
    let tempHoliday
    let tempMonth
    let month = []

    if (params && (params.month != null && params.year != null)) {

        holidays = await db.holiday.findOne({
            where: {
                year: params.year
            }
        })
        if (!holidays) {
            throw new Error(err)
        } else {

            if (params.month == "january") {
                month = JSON.parse(holidays.january)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth


            } else if (params.month == "february") {
                month = JSON.parse(holidays.february)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth

            } else if (params.month == "march") {
                month = JSON.parse(holidays.march)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth


            } else if (params.month == "april") {
                month = JSON.parse(holidays.april)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "may") {
                month = JSON.parse(holidays.may)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "june") {
                month = JSON.parse(holidays.june)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "july") {
                month = JSON.parse(holidays.july)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "august") {
                month = JSON.parse(holidays.august)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "september") {
                month = JSON.parse(holidays.september)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "october") {
                month = JSON.parse(holidays.october)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "november") {
                month = JSON.parse(holidays.november)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            } else if (params.month == "december") {
                month = JSON.parse(holidays.december)
                tempMonth = await createTempMonth(params, month)
                // month = []
                log.end()
                holidays = tempMonth
            }
        }
        log.end()
    } else {
        holidays = await db.holiday.findOne({
            where: {
                year: params.year
            }
        })
        if (!holidays) {
            log.end()
            throw new Error('HOLIDAY NOT FOUND')
        }
        tempHoliday = await createTempHoliday(holidays)
        holidays = tempHoliday
    }
    log.end()
    return holidays

}

const update = async (model, context) => {
    const log = context.logger.start(`services/holidays/update`)
    let isLeapYear
    let holiday
    let type
    let isEvent = "false"
    if (model.fromDate == model.toDate) {
        type = "single"
    } else {
        type = "multiple"
    }
    if (model.isEvent) {
        isEvent = model.isEvent
    }
    let details = {
        type: type,
        isEvent: isEvent
    }

    if (model.year) {
        isLeapYear = (model.year % 100 === 0) ? (model.year % 400 === 0) : (model.year % 4 === 0);
        console.log(isLeapYear)
    }
    holiday = await db.holiday.find({
        where: {
            year: model.year
        }
    })
    if (!holiday) {
        holiday = await build(model, details, isLeapYear, context)

    } else {
        holiday = await set(model, details, holiday, context)
    }

    log.end()

    return holiday
}

const deleteHoliday = async (model, context, res) => {
    const log = context.logger.start(`services/deleteHoliday/delete`)
    let holidays = await db.holiday.find({
        where: {
            year: model.year
        }
    })
    if (!holidays) {
        throw new Error('holidays are not found for this year')
    } else {
        let month = []
        let existingIndex
        let doesExist=false

        // if january
        if (model.month == "january") {
            month = JSON.parse(holidays.january)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.january=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "february") {
            month = JSON.parse(holidays.february)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.february=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "march") {
            month = JSON.parse(holidays.march)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.march=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "april") {
            month = JSON.parse(holidays.april)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.april=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "may") {
            month = JSON.parse(holidays.may)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.may=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "june") {
            month = JSON.parse(holidays.june)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.june=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "july") {
            month = JSON.parse(holidays.july)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.july=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "august") {
            month = JSON.parse(holidays.august)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.august=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "september") {
            month = JSON.parse(holidays.september)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.september=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "october") {
            month = JSON.parse(holidays.october)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.october=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "november") {
            month = JSON.parse(holidays.november)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.november=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }else if (model.month == "december") {
            month = JSON.parse(holidays.december)
            if (month &&( month.length !== 0)) {
                month.forEach((holiday, index) => {
                    let holidayId=JSON.parse(model.id)
                    if (holiday._id == holidayId) {
                        doesExist = true;
                        existingIndex = index;
                    }
                });
                if(doesExist){
                    month.splice(existingIndex,1);
                    console.log("month:",month)
                }
                holidays.december=JSON.stringify(month)
            }else{
                throw new Error('holiday does not exist for this month')
            }
        }
    }
    log.end()
    holidays.save()
    return holidays
}


exports.create = create
exports.update = update;
exports.getById = getById
exports.get = get
exports.deleteHoliday = deleteHoliday
