'use strict'
const service = require('../services/users')
const sectionService = require('../services/sections')
const response = require('../exchange/response')

const createTempClassObj = async (classes, users) => {
    var classObj = {
        id: classes.id,
        name: classes.name,
        status: classes.status,
        section: users
    }

    return classObj;
}

const set = async (model, entity, context) => {
    const log = context.logger.start('services/classes/set')
    if (model.status) {
        entity.status = model.status
    }
    if (model.name) {
        entity.name = model.name.toLowerCase()
    }
    log.end()
    await entity.save()
    return entity
}

const build = async (model, context) => {
    const log = context.logger.start('services/classes/build')
    const classes = await db.class.build({
        name: model.name.toLowerCase(),
        status: model.status || 'active'
    }).save()

    log.end();
    return classes
}


const create = async (model, context) => {
    const log = context.logger.start('services/classes/create')
    let classes
    if (model.name) {
        classes = await db.class.findOne({
            where: {
                name: model.name.toLowerCase()
            }
        })
        if (!classes) {
            // call build function
            classes = await build(model, context)
        } else {
            throw new Error("Class already exist")
        }
    }
    log.end();

    return classes
}



const getById = async (req, query, context) => {
    const log = context.logger.start(`services/classes/getById ${query.id}`)
    let classes
    let users
    // let sections
    // let sectionResult = []
    let i = 0
    if (query.id) {
        classes = await db.class.findOne({
            where: {
                id: query.id
            }
        })
        if (!classes) {
            throw new error('Class not found')
        }

        // get section from users
        users = await service.get(req, {
            classId: query.id
        }, req.context)
        const tempClassResponseObj = await createTempClassObj(classes, users)

        log.end();
        return tempClassResponseObj
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/classes/get`)
    let params = req.query
    let classes
    let sections
    let sectionResult = []
    let tempClasses = []

    if (params.classId && (params.classId != null && params.classId != undefined) && ((params.status !== null && params.status !== undefined) || (params.status == null && params.status == undefined))) {
        classes = await db.class.findOne({
            where: {
                id: params.classId
            }
        })
        if (!classes) {
            throw new error('Class not found')
        }
        // find sections according to classId or status
        sections = await sectionService.get(req, context)

        if (!sections) {
            throw new Error('Section not found')
        } else {
            for (const item of sections) {
                if (item) {
                    sectionResult.push({
                        classSection: item.name
                    })
                }
            }
        }
        const tempClassResponseObj = await createTempClassObj(classes, sectionResult)
        classes = tempClassResponseObj

    } else if ((params.classId == null && params.classId == undefined) && (params.status !== null && params.status !== undefined)) {
        // get classes according to status
        classes = await db.class.findAll({
            where: {
                status: params.status
            }
        })

    } else {
        // get all classes
        classes = await db.class.findAll({})
        for (const item of classes) {
            if (item) {
                let request = {
                    query: {
                        classId: item.id,
                        status: 'active'
                    }
                }
                sections = await sectionService.get(request, context)

                if (!sections) {
                    throw new Error('Section not found')
                } else {
                    for (const section of sections) {
                        if (section) {
                            sectionResult.push({
                                classSection: section.name
                            })
                        }
                    }
                }
                const tempClassResponseObj = await createTempClassObj(item, sectionResult)
                tempClasses.push(tempClassResponseObj)
                sectionResult = []
            }

        }
        classes = tempClasses
    }
    log.end()
    return classes
}


const update = async (id, model, context) => {
    const log = context.logger.start(`services/classes/update/id`)
    // find one class
    const entity = await db.class.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new error('Invalid class')
    }
    // update class
    let classes = await set(model.body, entity, context)

    log.end();
    return classes.save()
}

const deleteClass = async (id, context, res) => {
    const log = context.logger.start(`services/classes/delete/id`)
    const classes = await db.class.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (classes) {
        await db.class.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }
    res.message = 'Class removed sucessfully'
    log.end()
    return response.data(res, '')
}


exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteClass = deleteClass