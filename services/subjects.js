'use strict'
const response = require('../exchange/response')

const set = async (model, entity, context) => {
    const log = context.logger.start('services/subjects/set')
    if (model.status) {
        entity.status = model.status
    }
    if (model.name) {
        entity.name = model.name.toLowerCase()
    }
    log.end()
    await entity.save()
    return entity
}

const build = async (model, context) => {
    const log = context.logger.start('services/subjects/build')
    const subject = await db.subject.build({
        classId: model.classId,
        name: model.name.toLowerCase(),
        status: model.status || 'active'
    }).save()
    log.end()
    return subject
}

const create = async (model, context) => {
    const log = context.logger.start('services/subjects/create')
    let subject
    if (model) {
        // get subject according to class
        if (model.classId && model.name) {

            subject = await db.subject.findOne({
                where: {
                    name: model.name.toLowerCase(),
                    classId: model.classId
                }
            })
            // create subject
            if (!subject) {
                subject = await build(model, context)

            } else {
                throw new Error('subject name already exist')
            }
        }
    }
    console.log(subject);
    log.end()
    return subject
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/subjects/getById/id ${query.id}`)
    if (query.id) {
        const subject = await db.subject.find({
            where: {
                id: query.id
            }
        })
        log.end()
        return subject
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/subjects/get`)
    let params = req.query
    let subject
    // get subject through status
    if (params && (params.status != null && params.status != undefined && params.classId == null && params.classId == undefined)) {
        subject = await db.subject.findAll({
            where: {
                status: params.status
            }
        })
        log.end()
        // get subject through classId
    } else if (params && (params.classId != null && params.classId != undefined && params.status == null && params.status == undefined)) {
        subject = await db.subject.findAll({
            where: {
                classId: params.classId
            }
        })
        log.end()

        // get subject through classId and status   
    } else if (params && (params.classId != null && params.classId != undefined && params.status !== null && params.status !== undefined)) {
        subject = await db.subject.findAll({
            where: {
                classId: params.classId,
                status: params.status
            }
        })
        log.end()

        // get all subjects
    } else {
        subject = await db.subject.findAll({})
        let result = []
        const map = new Map();
        for (const item of subject) {
            if (!map.has(item.name)) {
                map.set(item.name, true);
                result.push({
                    name: item.name
                })
            }
        }
        subject = result
        log.end()
    }
    log.end()
    return subject

}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/subjects/update/id`)
    // find subject by id
    let entity = await db.subject.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new Error('invalid subject')
    }
    // update subject
    let subject = await set(model.body, entity, context)
    log.end();
    return subject.save()

}

const deleteSubject = async (id, context, res) => {
    const log = context.logger.start(`services/subjects/delete/id`)
    const subject = await db.subject.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (subject) {
        await db.subject.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }
    res.message = 'Subject removed sucessfully'
    log.end()
    return response.data(res, '')
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteSubject = deleteSubject