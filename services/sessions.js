'use strict'


const build = async (model, context) => {
     const log = context.logger.start('services/sessions/build')
    const session = await db.session.build({
        year: model.year
       
    }).save()
     log.end()
    return session
}

const create = async (model, context) => {
     const log = context.logger.start(`services/sessions/build:${model}`)
    
    const session = build(model, context)
    console.log(session);
     log.end()
    return session
}



const get = async (query, context) => {
    const log = context.logger.start(`services/sessions/get:${query}`)
    if (query.id) {
        const session = await db.session.findOne({
            where: {
                id: query.id
            }
            // include: [db.vehicle]
        })
        log.end()
        return session
    }
    if (query.year) {
        const session = await db.session.findAll({
            where: {
                year: query.year,
            }

        })
        log.end()
        return session
    }
}
exports.create = create 
exports.get=get      