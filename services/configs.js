'use strict'

const build = async (model, context) => {
    const log = context.logger.start('services/configs/build')
    const config = await db.config.build({
        dueId: model.dueId
    }).save()

    log.end();
    return config
}


const create = async (model, context) => {
    const log = context.logger.start('services/configs')
    // call build function
    const config = await build(model, context)
    log.end();

    return config
}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/configs/update/id`)
    let config
    // find config
    config = await db.config.findOne({
        where: {
            id: parseInt(id)
        }
    })
    // update dueId
    if (model.updateDueId && model.updateDueId == true) {
        if (config.dueId == null || config.dueId == undefined || config.dueId == "") {
            config.dueId = 12000
            config.dueId = config.dueId + 1
        } else {
            config.dueId += 1
        }
    }
    // update working days with job of school
    if (model.schoolWorkingDays == true) {
        if (config.workingDaysUpdatedAt == null || config.workingDaysUpdatedAt == undefined || config.workingDaysUpdatedAt == "") {
            config.workingDaysUpdatedAt = model.workingDaysUpdatedAt,
                config.schoolWorkingDays += 1
        } else if (config.workingDaysUpdatedAt == model.workingDaysUpdatedAt) {
            config.workingDaysUpdatedAt = model.workingDaysUpdatedAt
        } else {
            config.workingDaysUpdatedAt = model.workingDaysUpdatedAt,
                config.schoolWorkingDays += 1
        }
    }
    // // update config
    // config = await set(model.body, entity, context)

    log.end();
    return config.save()
}

const get = async (req, context) => {
    const log = context.logger.start(`services/configs/get`)
    // get all classes
    const configs = await db.config.findAll({})

    log.end()
    return configs

}

exports.create = create
exports.update = update
exports.get = get






// if (!config) {
//     config = await create(model, context)
//     if (config) {
//         if (config.dueId == null || config.dueId == undefined || config.dueId == "") {
//             config.dueId = 12000
//             config.dueId = config.dueId + 1
//         }
//     }
// } else