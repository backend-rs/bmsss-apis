const auth = require('../permit/auth')
const res = require('../exchange/response')
var moment = require('moment')

const set = async (model, leave, context) => {
    const log = context.logger.start('services/leaves/set')
    if (model.status) {
        leave.status = model.status
    }
    if (model.rejectionReason) {
        leave.rejectionReason = model.rejectionReason
    }
    log.end()
    return leave.save();
}
const createTempLeaveObj = async (leave, user) => {
    var leaveObj = {
        id: leave.id,
        name: user.firstName + " " + " " + user.lastName,
        classId: user.classId,
        class: user.className,
        section: user.classSection,
        rollNo: user.rollNo,
        reason: leave.reason,
        status: leave.status,
        rejectionReason: leave.rejectionReason,
        otp: leave.otp,
        leaveToken: leave.leaveToken,
        leaveType: leave.leaveType,
        fullDayType: leave.fullDayType,
        fromDate: leave.fromDate,
        toDate: leave.toDate
    }
    return leaveObj;
}

const build = async (model, newdate, context) => {
    const log = context.logger.start('services/leaves/build')
    const leave = await db.leave.build({
        userId: model.userId,
        leaveType: model.leaveType,
        fullDayType: model.fullDayType,
        fromDate: model.fromDate,
        toDate: model.toDate,
        otp: model.otp,
        reason: model.reason,
        status: model.status || 'pending',
        leaveToken: model.leaveToken,
        date: newdate
    }).save()
    log.end()
    return leave;
}

const create = async (model, context) => {
    const log = context.logger.start('services/leaves/create')
    let leave
    let tempLeaveResponseObj
    let tempFromDate
    let tempFromDateParts
    let tempToDate
    let tempToDateParts
    let user
    let fixedTime
    let compareTime
    // let dateObj = new Date()
    // let date = dateObj.getDate()
    // let month = dateObj.getMonth() + 1
    // let year = dateObj.getFullYear()
    // let newdate = date + "/" + month + "/" + year
    let nowDate = moment();
    let newdate = nowDate.format("DD/MM/YYYY")
    let currentTime = moment(nowDate.format("h:mma"), "h:mma")

    // console.log(lastDate)

    if (model && model.leaveType == 'halfDay') {

        // check leave already registered or not
        if (model.userId && newdate) {
            leave = await db.leave.findOne({
                where: {
                    userId: model.userId,
                    date: newdate
                }
            })
        }
        if (leave) {
            throw new Error('already_applied')
        } else {
            fixedTime = moment("12:00pm", "h:mma")
            compareTime = currentTime.isBefore(fixedTime)
            if (compareTime == false) {
                throw new Error('apply_leave_before_12:00pm')
            }

            // get otp
            var digits = '0123456789';
            let OTP = '';
            for (let i = 0; i < 6; i++) {
                OTP += digits[Math.floor(Math.random() * 10)];
            }
            model.otp = OTP;
            user = await db.user.findOne({
                where: {
                    id: model.userId
                }
            })

            // get token
            let leaveToken = auth.getToken(user.id, false, context)
            model.leaveToken = leaveToken;

            // call build fuction 
            leave = await build(model, newdate, context)
            tempLeaveResponseObj = await createTempLeaveObj(leave, user)
            log.end()
        }
    } else {
        if (model.fromDate && model.toDate) {

            // if from date is less than current date
            let fromDate = moment(model.fromDate, "DD/MM/YYYY");
            let todayDate = moment(newdate, "DD/MM/YYYY");
            var isEqual = moment(fromDate).isSame(todayDate);
            var isAfter = moment(fromDate).isAfter(todayDate);
            // match time
            if (isEqual == true) {
                // let now = moment()
                // let currentTime = moment(now.format("h:mma"), "h:mma")
                fixedTime = moment("8:00am", "h:mma")
                compareTime = currentTime.isBefore(fixedTime)
                if (compareTime == false) {
                    throw new Error('date_already_passed')
                }
            }
            // if date passed
            if (isAfter == false && isEqual == false) {
                throw new Error('date_already_passed')
            }

            // if single day leave
            if (model.fullDayType && model.fullDayType == 'single') {
                tempFromDate = model.fromDate;
                tempFromDateParts = tempFromDate.split('/');
                tempToDate = model.toDate;
                tempToDateParts = tempToDate.split('/');
                if (tempFromDateParts[0] != tempToDateParts[0] || tempFromDateParts[1] != tempFromDateParts[1] || tempFromDateParts[2] != tempFromDateParts[2]) {
                    throw new Error('fromDate and toDate should be same for single leave')
                }

            } else {
                // if multiple days  leave
                tempFromDate = model.fromDate;
                tempFromDateParts = tempFromDate.split('/');
                tempToDate = model.toDate;
                tempToDateParts = tempToDate.split('/');
                if (tempFromDateParts[0] == tempToDateParts[0] && tempFromDateParts[1] == tempFromDateParts[1] && tempFromDateParts[2] == tempFromDateParts[2]) {
                    throw new Error('fromDate and toDate should be different for multiple leave')
                }
            }
            // find leave if already exist
            leave = await db.leave.findOne({
                where: {
                    userId: model.userId,
                    fromDate: model.fromDate,
                    toDate: model.toDate
                }
            })
            if (leave) {
                throw new Error('already_applied')
            } else {
                //find leave if from date is same
                leave = await db.leave.findOne({
                    where: {
                        userId: model.userId,
                        fromDate: {
                            $gte: model.fromDate
                        },
                        toDate: {
                            $lte: model.toDate
                        }
                    }
                })
                if (leave) {
                    leave = await db.leave.destroy({
                        where: {
                            id: leave.id
                        }
                    })
                    console.log("leave:", leave);
                }
                // find user
                user = await db.user.findOne({
                    where: {
                        id: model.userId
                    }
                })
                // call build fuction 
                leave = await build(model, ' ', context)
                tempLeaveResponseObj = await createTempLeaveObj(leave, user)
            }
        }
    }
    return tempLeaveResponseObj
}


const getById = async (query, context) => {
    const log = context.logger.start(`services/leaves/get:${query}`)
    // find leave by user id
    if (query.id) {
        const leave = await db.leave.findOne({
            where: {
                userId: query.id,
            }
        })
        let user = await db.user.findOne({
            where: {
                id: leave.userId,
            }
        })
        const tempLeaveResponseObj = await createTempLeaveObj(leave, user)

        return tempLeaveResponseObj
    }
    log.end()
    return null
}

const get = async (req, context) => {
    const log = context.logger.start(`services/leaves/get`)
    var leaveObj = []
    let leave
    let params = req.query
    let queryModel = {}

    // get leave through status and userId
    if (params && ((params.status != undefined && params.status != null) && (params.userId != undefined && params.userId != null) && (params.leaveType != undefined && params.leaveType != null))) {
        queryModel = {
            status: params.status,
            userId: params.userId,
            leaveType: params.leaveType
        }
        log.end()
    } else if (params && (params.leaveType && params.userId && (params.status == null && params.status == undefined))) {
        queryModel = {
            userId: params.userId,
            leaveType: params.leaveType
        }
        log.end()
    } else if (params && (params.leaveType && params.status && (params.userId == null && params.userId == undefined))) {
        queryModel = {
            status: params.status,
            leaveType: params.leaveType
        }
        log.end()
    } else if (params && (params.userId && params.status && (params.leaveType == null && params.leaveType == undefined))) {
        queryModel = {
            status: params.status,
            userId: params.userId,
        }
        log.end()
    } else if (params && (params.leaveType && (params.userId == null && params.userId == undefined) && (params.status == null && params.status == undefined))) {
        queryModel = {
            leaveType: params.leaveType
        }
        log.end()
    } else if (params && (params.status && (params.userId == null && params.userId == undefined) && (params.leaveType == null && params.leaveType == undefined))) {
        queryModel = {
            status: params.status
        }
        log.end()
    } else if (params && (params.userId && (params.status == null && params.status == undefined) && (params.leaveType == null && params.leaveType == undefined))) {
        queryModel = {
            userId: params.userId
        }
        log.end()
    } else {
        queryModel = {}
    }
    // find leaves
    leave = await db.leave.findAll({
        where: queryModel,
        order: [
            ['updatedAt', 'DESC']
        ]
    })
    if (leave.length != 0) {
        for (const item of leave) {
            leave = await db.leave.findOne({
                where: {
                    id: item.id,
                }
            })
            if (leave.userId) {
                let user = await db.user.findOne({
                    where: {
                        id: leave.userId,
                    }
                })
                leaveObj.push({
                    id: leave.id,
                    name: user.firstName + " " + "" + user.lastName,
                    classId: user.classId,
                    class: user.className,
                    rollNo: user.rollNo,
                    section: user.classSection,
                    status: leave.status,
                    rejectionReason: leave.rejectionReason,
                    reason: leave.reason,
                    otp: leave.otp,
                    leaveToken: leave.leaveToken,
                    leaveType: leave.leaveType,
                    fullDayType: leave.fullDayType,
                    fromDate: leave.fromDate,
                    toDate: leave.toDate,
                    date: leave.date
                })
            }
        }
    }
    log.end()
    return leaveObj
}


const verifyOtp = async (model, context) => {
    const log = context.logger.start(`services/leaves/verifyOtp`)
    let query = {}
    // find leave through token
    if (model.leaveToken) {
        query.leaveToken = model.leaveToken
    }
    let leave = await db.leave.findOne({
        where: {
            leaveToken: query.leaveToken
        }
    })
    if (!leave) {
        log.end()
        throw new Error('LEAVE NOT FOUND')
    }
    // check otp match or not
    if (model.otp !== leave.otp) {
        log.end();
        throw new Error('otp did not match')
    }

    // update leave status
    let detail = await set(model, leave, context)
    let user = await db.user.findOne({
        where: {
            id: leave.userId,
        }
    })


    const tempLeaveResponseObj = await createTempLeaveObj(detail, user)
    return tempLeaveResponseObj

}

const update = async (id, model, context) => {
    const log = context.logger.start(`services/leaves/update/id`)
    // find leave by id
    let entity = await db.leave.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new Error('invalid leave')
    }
    if (model.body && (model.body.status == "rejected")) {
        if (!model.body.rejectionReason) {
            throw new Error('Rejection reason is required')
        }
    }
    // update subject
    let leave = await set(model.body, entity, context)
    log.end();
    return leave.save()

}

exports.create = create
exports.get = get
exports.getById = getById
exports.verifyOtp = verifyOtp
exports.update = update












// compare dates
// let startDate = moment(model.fromDate, "DD/MM/YYYY");
// let nowDate = moment(newdate, "DD/MM/YYYY");
// let difference = JSON.parse(startDate.diff(nowDate, 'days'));

// if (difference < 3) {
//     throw new Error('Leave should be applied before three days.')
// }