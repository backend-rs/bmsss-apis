'use strict'
const fs = require('fs')
const path = require('path')
const appRoot = require('app-root-path')
const fileStoreConfig = require('config').get('providers.document-store')
const response = require('../exchange/response')

const build = async (req, context) => {
    const log = context.logger.start('services/documents/build')

    // call upload fuction to file upload
    let data = await upload(req.files.file)
    const document = await db.document.build({
        file_url: data.url
    }).save()

    log.end();
    return document
}


const create = async (req, context) => {
    const log = context.logger.start('services/documents')
    // call build function
    const document = await build(req, context)
    log.end();

    return document
}

const getById = async (req,query, context) => {
    const log = context.logger.start(`services/documents/getById ${query.id}`)

    if (query.id) {
        const document = await db.document.find({
            where: {
                id: query.id,
            }
        })

        log.end()
        return document
    }
}

const upload = async (file, context) => {

    let parts = file.name.split('.')

    let name = parts[0]
    let ext = parts[1]

    let destDir = path.join(appRoot.path, fileStoreConfig.dir)

    let fileName = `${name}-${Date.now()}.${ext}`

    let destination = `${destDir}/${fileName}`
    let url = `${fileStoreConfig.root}/${fileName}`

    await move(file.path, destination)

    file.path = destination

    return {
        url: url
    }
}

const move = async (oldPath, newPath) => {
    const copy = (cb) => {
        var readStream = fs.createReadStream(oldPath)
        var writeStream = fs.createWriteStream(newPath)

        readStream.on('error', cb)
        writeStream.on('error', cb)

        readStream.on('close', function () {
            fs.unlink(oldPath, cb)
        })

        readStream.pipe(writeStream)
    }

    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, function (err) {
            if (err) {
                if (err.code === 'EXDEV') {
                    copy(err => {
                        if (err) {
                            reject(err)
                        } else {
                            resolve()
                        }
                    })
                } else {
                    reject(err)
                }
            } else {
                resolve()
            }
        })
    })
}

exports.create = create
exports.getById = getById
exports.upload = upload