'use strict'
const service = require('../services/configs');
const feeReceiptsService = require('../services/feeReceipts');

const createTempFeeModel = async (fee, feeModel) => {
    var feeDetails = {
        id: fee.id,
        dueId: fee.dueId,
        feeChargesId: fee.feeChargesId,
        feeChargesName: feeModel.feeChargesName,
        admissionNo: fee.admissionNo,
        userRegId: fee.userRegId,
        dueAmount: fee.dueAmount,
        payAmount: fee.payAmount,
        balanceAmount: fee.balanceAmount,
        feeType: fee.feeType,
        month: fee.month,
        status: fee.status
    }
    return feeDetails
}

const set = async (model, fee, context) => {

    const log = context.logger.start('services/fees/set')

    if (model.dueAmount) {
        fee.dueAmount = model.dueAmount
        fee.balanceAmount = (model.dueAmount) - (fee.payAmount)
    }
    if (model.payAmount) {
        fee.payAmount = (fee.payAmount) + (model.payAmount)
        fee.balanceAmount = (fee.balanceAmount) - (model.payAmount)
    }
    if (model.feeType) {
        fee.feeType = model.feeType
    }

    log.end()
    console.log('feeeeeeeeeeeeeee:', fee)
    await fee.save()

    return fee
}
const build = async (model, feeModel, context) => {
    const log = context.logger.start('services/fees/build')

    // find config
    let config = await db.config.find({
        where: {
            organization: "bmsss"
        }
    })
    let tempDetail = {
        updateDueId: true
    }
    // update dueId in config
    let configDetail = await service.update(config.id, tempDetail, context)

    const fee = await db.fee.build({

        dueId: configDetail.dueId || 0,
        feeChargesId: model.feeChargesId || 0,

        admissionNo: model.admissionNo,
        userRegId: feeModel.userRegId,

        dueAmount: model.dueAmount || 0,
        payAmount: model.payAmount || 0,
        balanceAmount: model.dueAmount || 0,

        feeType: model.feeType || "school",
        month: model.month,
        status: model.status || 'pending'
    })

    fee.save()
    console.log(fee)
    log.end()
    return fee
}

const create = async (model, context) => {
    const log = context.logger.start('services/fees/create')
    let fee
    // if admission no & feeChargesId exists
    if (model.admissionNo && model.feeChargesId) {
        fee = await db.fee.findOne({
            where: {
                admissionNo: model.admissionNo,
                feeChargesId: model.feeChargesId
            }
        })
        if (fee) {
            throw new Error('EXIST')
        } else {
            // find user if admissionNo exists in user
            let user = await db.user.findOne({
                where: {
                    admissionNo: model.admissionNo
                }
            })
            if (!user) {
                throw new Error('user not found for given admission no.')
            }

            // find headId in feeCharges to get headName/feeChargesName
            let feeCharge = await db.feeCharge.findOne({
                where: {
                    headId: model.feeChargesId
                }
            })
            if (!feeCharge) {
                throw new Error('feeCharges not found')
            }

            let feeModel = {
                userRegId: user.regNo,
                feeChargesName: feeCharge.headName
            }
            fee = await build(model, feeModel, context)
            const tempFeeModel = await createTempFeeModel(fee, feeModel)
            fee = tempFeeModel
        }
    } else {
        throw new Error('admissionNo and feeCharges are required')
    }

    log.end()
    return fee
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/fees/get:${query}`)
    if (query.id) {
        const fees = await db.fee.findOne({
            where: {
                id: query.id
            }
        })
        return fees
    }
    log.end()
    return null
}

const get = async (req, context) => {
    const log = context.logger.start(`services/fees/get`)
    let fee
    let totalDueAmount = 0
    let totalBalanceAmount = 0
    let feeTemp = []
    let params = req.query
    if (params && (params.admissionNo !== undefined && params.admissionNo !== null)) {
        fee = await db.fee.findAll({
            where: {
                admissionNo: params.admissionNo
            }
        })
        console.log(fee)
        if (!fee) {
            log.end()
            throw new Error('Fee NOT FOUND')
        }
        for (const item of fee) {
            if (item) {

                // find headId in feeCharges to get headName/feeChargesName
                let feeCharge = await db.feeCharge.findOne({
                    where: {
                        headId: item.feeChargesId
                    }
                })
                if (!feeCharge) {
                    throw new Error('feeCharges not found')
                }
                let feeModel = {
                    feeChargesName: feeCharge.headName
                }

                // model for single feeItem
                const tempFeeModel = await createTempFeeModel(item, feeModel)
                feeTemp.push(tempFeeModel)

                // total dueAmount
                if (item.dueAmount) {
                    totalDueAmount = totalDueAmount + item.dueAmount
                }
                // total balanceAmount
                if (item.balanceAmount) {
                    totalBalanceAmount = totalBalanceAmount + item.balanceAmount
                }
            }
        }

        // model for fee response
        let result = {
            fee: feeTemp,
            totalDueAmount: totalDueAmount,
            totalBalanceAmount: totalBalanceAmount
        }
        fee = result
        log.end()
    } else {
        fee = await db.fee.findAll({})
        if (!fee) {
            log.end()
            throw new Error('Fee NOT FOUND')
        }
        for (const item of fee) {
            if (item) {
                if (item.dueAmount) {
                    totalDueAmount = totalDueAmount + item.dueAmount
                }
                if (item.balanceAmount) {
                    totalBalanceAmount = totalBalanceAmount + item.balanceAmount
                }
            }
        }
        let result = {
            fee: fee,
            totalDueAmount: totalDueAmount,
            totalBalanceAmount: totalBalanceAmount
        }

        fee = result
        log.end()
    }
    log.end()

    return fee

}


const update = async (id, model, context) => {
    const log = context.logger.start(`services/fees/update`)
    console.log("modelllll:", model)
    let fee
    let transType
    let entity = await db.fee.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new Error('fee does not exist')
    } else if (entity.admissionNo !== model.admissionNo) {
        fee = await create(model, context)
    } else {
        fee = await set(model, entity, context)

        if (fee.balanceAmount == 0) {
            fee.status = "complete"
            fee.save()
        }
        if (model.transType) {
            transType = model.transType
        } else {
            transType = "cash"
        }
        // generate receipt
        if (model.payAmount) {
            let receiptModel = {
                receiptNo: model.receiptNo,
                admissionNo: fee.admissionNo,
                userRegId: fee.userRegId,
                feeChargesId: fee.feeChargesId,
                feeType: fee.feeType,
                payAmount: model.payAmount,
                dueAmount: fee.dueAmount,
                balanceAmount: fee.balanceAmount,
                remark: model.remark,
                transRemark: model.transRemark,
                transType: transType,
                transNo: model.transNo,
                bankName: model.bankName
            }
            // call receipt create service
            let receipt = await feeReceiptsService.create(receiptModel, context)
            console.log(receipt)
        }

        log.end()
        return fee
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
















// find headId in feeCharges to get headName/feeChargesName
// let feeCharge = await db.feeCharge.findOne({
//     where: {
//         headId: fee.feeChargesId
//     }
// })
// if (!feeCharge) {
//     throw new Error('feeCharges not found')
// }


// if(payStatus==true){
//     let detail={
//         admissionNo:fee.admissionNo,
//         userRegId:fee.userRegId,
//         feeChargesId:fee.feeChargesId,
//         feeChargesName:fee.feeChargesName,
//         status:fee.status,
//         receiptNo:fee.receiptNo,
//         payAmount:fee.payAmount,
//         dueAmount:fee.dueAmount,
//         balanceAmount:fee.balanceAmount,
//         remark:fee.remark,
//         transRemark:fee.transRemark,
//         transDate:fee.transDate

//     }

//     let receipt=await feeReceiptsService .create(detail, context)

// }













//     await sequelize.query(`SELECT dueAmount,balanceAmount FROM fees WHERE admissionNo='${model.admissionNo}'`, {
//         type: Sequelize.QueryTypes.SELECT
//     })
//         .then(i => {
//             isRowExist = true
//             temp = i
//             console.log(temp)
//             return i
//         }).catch(err => {
//             response.failure(res, err)
//         })
// }
// if (isRowExist == true) {
//     if (temp.length != 0) {
//         for (const item of temp) {
//             if (item.dueAmount) {
//                 totalDue += item.dueAmount
//                 console.log("totalDue:", totalDue)

//             }
//             if (item.balanceAmount) {
//                 totalBalance += item.balanceAmount
//                 console.log("totalBalanceeeeee:", totalBalance)
//                 // console.log("lBalanceeeeee:", balanceAmount)


//             }
//         }

//     }
// }
// fee.totalDueAmount = totalDue,
// fee.totalBalanceAmount=totalBalance