'use strict'
const response = require('../exchange/response')
const imageUploadService = require('../services/events')

const set = async (body, req, entity, context) => {
    const log = context.logger.start('services/classes/set')
    let data
    if (req) {
        if (req.files != null && req.files != undefined) {
            if (req.files.file != null && req.files.file != undefined) {

                if (body.shouldImageUpdate == "true") {

                    data = await imageUploadService.upload(req.files.file)

                    if (body.name) {
                        entity.name = body.name
                    }
                    if (body.event_id) {
                        entity.event_id = body.event_id
                    }
                    if (data.url) {
                        entity.image_url = data.url
                    }
                    if (data.thumbnail) {
                        entity.image_thumbnail = data.thumbnail
                    }
                    if (data.resize_url) {
                        entity.resize_url = data.resize_url
                    }
                    if (data.resize_thumbnail) {
                        entity.resize_thumbnail = data.resize_thumbnail
                    }
                } else {
                    if (body.name) {
                        entity.name = body.name
                    }
                    if (body.event_id) {
                        entity.event_id = body.event_id
                    }
                }
            }
        }
    }

    log.end()
    await entity.save()
    return entity
}


const build = async (req, body, context) => {
    const log = context.logger.start('services/events/build')
    let data = await imageUploadService.upload(req.files.file)
    const event = await db.file.build({
        name: body.name,
        shouldImageUpdate: body.shouldImageUpdate || 'false',
        isUpdate: body.isUpdate || 'false',
        event_id: body.event_id,
        image_url: data.url,
        image_thumbnail: data.thumbnail,
        resize_url: data.resize_url,
        resize_thumbnail: data.resize_thumbnail
    }).save()
    log.end();
    return event
}

const create = async (req, body, context) => {
    const log = context.logger.start('services/files/create')
    let file
    if (body.isUpdate == "true") {
        file = await update(req, body.file_id, body, context)
        log.end();
    } else {
        file = await build(req, body, context)
        log.end();
    }
    return file
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/files/getById ${query.id}`)
    if (query.id) {
        const file = await db.file.find({
            where: {
                id: query.id
            }
        })
        log.end();
        return file
    }
}

const getByEvent = async (req, context) => {
    const log = context.logger.start(`services/files/getByEvent`)
    let params = req.query
    let file
    if (params && (params.id != null && params.id != undefined)) {
        file = await db.file.findAll({
            where: {
                event_id: params.id
            }
        })
        log.end();

    } else {
        file = await db.file.findAll({})
        log.end();
    }
    return file
}

const update = async (req, id, body, context) => {
    const log = context.logger.start(`services/files/update/id`)
    const entity = await db.file.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!entity) {
        throw new error('Invalid file')
    }
    let file = await set(body, req, entity, context)
    log.end();
    return file.save()
}

const deleteFile = async (id, context, res) => {
    const log = context.logger.start(`services/files/delete/id`)
    const file = await db.file.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (file) {
        await db.file.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }
    res.message = 'File removed sucessfully'
    log.end()
    return response.data(res, '')
}
exports.create = create
exports.getById = getById
exports.getByEvent = getByEvent
exports.update = update
exports.deleteFile = deleteFile