'use strict'
const service = require('../services/messages')

const build = async (model, context) => {
    const log = context.logger.start('services/birthdays/build')
    let temp = {}

    const birthday = await db.birthday.build({
        classId: model.classId,
        userType: model.userType,
        birthDayList: model.birthDayList,


    })
    let obj = []
    if (model.classId && model.userType == 'student') {
        let classes = await db.class.findOne({
            where: {
                id: model.classId
            }
        })
        if (!classes) {
            log.end()
            throw new Error('CLASS NOT FOUND')

        } else if (model.userType == 'student') {
            users = await db.user.findAll({
                where: {
                    // classId: model.classId,
                    userType: model.userType
                }
            })
            for (const user of users) {
                if (user) {
                    temp.userId = user.id,
                        temp.name = user.firstName + " " + " " + user.lastName,
                        temp.className = user.className,
                        temp.section = user.classSection,
                        temp.userType = user.userType,
                        temp.birthdayDate = user.dateOfBirth
                    obj.push(temp);

                    birthday.birthDayList = JSON.stringify(obj)
                }
            }
        } else {
            users = await db.user.findAll({
                where: {
                    classId: model.classId,
                    // userType: model.userType
                }
            })
            for (const user of users) {
                if (user) {
                    temp.userId = user.id,
                        temp.name = user.firstName + " " + "" + user.lastName,
                        temp.className = user.className,
                        temp.section = user.classSection,
                        temp.userType = user.userType,
                        temp.birthdayDate = user.dateOfBirth
                    obj.push(temp);

                    birthday.birthDayList = JSON.stringify(obj)
                }
            }
        }

    }

    birthday.save()
    log.end()
    return birthday

}

const create = async (model, context) => {
    const log = context.logger.start('services/birthdays')

    const birthday = await build(model, context)

    log.end()
    return birthday
}


const getById = async (query, context) => {
    const log = context.logger.start(`services/birthdays/get:${query}`)

    if (query.id) {
        const birthday = await db.birthday.findOne({
            where: {
                id: query.id,
            }

        })

        return birthday
    }
    log.end()
    return null
}

const get = async (req, context) => {
    const log = context.logger.start(`services/birthdays/get`)
    var birthdayObj = []
    let users
    let isBirthdayToday = false

    // current date creation
    let dateObj = new Date()

    let date = dateObj.getDate()
    date = date < 10 ? `0${date}` : date

    let monthName = dateObj.getMonth()
    let month = dateObj.getMonth() + 1
    month = month < 10 ? `0${month}` : month

    let year = dateObj.getFullYear()
    let currentDate = year + "-" + month + "-" + date;

    let params = req.query
    // get birthDay list by classId
    if (params && (params.classId != undefined && params.classId != null) && (params.classSection != undefined && params.classSection != null)) {
        let classes = await db.class.findOne({
            where: {
                id: params.classId,

            }
        })
        if (!classes) {
            log.end()
            throw new Error('CLASS NOT FOUND')
        } else {

            users = await db.user.findAll({
                where: {
                    classId: params.classId,
                    userType: "student",
                    classSection: params.classSection,
                }
            })

        }
        for (const user of users) {
            if (user) {
                // is birthday is today
                if (user.dateOfBirth == currentDate) {
                    isBirthdayToday = true
                } else {
                    isBirthdayToday = false
                }

                birthdayObj.push({
                    userId: user.id,
                    name: user.firstName + " " + "" + user.lastName,
                    class: user.className,
                    rollNo: user.rollNo,
                    section: user.classSection,
                    userType: user.userType,
                    birthdayDate: user.dateOfBirth,
                    isBirthdayToday: isBirthdayToday

                })
                // if (isBirthdayToday == true) {
                //     let request = {
                //         isBirthdayToday: true,
                //         askedBy: user.id,
                //         question: "It's" + " " + user.firstName + " " + user.lastName + " " + "birthday today. Wish" + " " + user.firstName + " " + user.lastName + " " + "the best!",
                //         classId: user.classId,
                //         classSection: user.classSection
                //     }
                //     const msges = await service.create(request, context)
                // }
            }
        }
    } else if ((params && (params.classId != undefined && params.classId != null))) {
        users = await db.user.findAll({
            where: {
                classId: params.classId,
                userType: "student",

            }
        })

        for (const user of users) {
            if (user) {
                birthdayObj.push({
                    userId: user.id,
                    name: user.firstName + " " + "" + user.lastName,
                    class: user.className,
                    rollNo: user.rollNo,
                    section: user.classSection,
                    userType: user.userType,
                    birthdayDate: user.dateOfBirth
                })

            }

        }
    } else {
        // get all student birthday list
        users = await db.user.findAll({
            where: {
                userType: "student"
            }
        })

        for (const user of users) {
            if (user) {
                birthdayObj.push({
                    userId: user.id,
                    name: user.firstName + " " + "" + user.lastName,
                    class: user.className,
                    rollNo: user.rollNo,
                    section: user.classSection,
                    userType: user.userType,
                    birthdayDate: user.dateOfBirth

                })

            }

        }
    }
    log.end()
    return birthdayObj

}


exports.create = create
exports.getById = getById
exports.get = get