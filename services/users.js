'use strict'

const auth = require('../permit/auth')
const encrypt = require('../permit/crypto')
var nodemailer = require('nodemailer')
const response = require('../exchange/response')
const imageUploadService = require('../services/events')
var moment = require('moment');

// limit for array
function limit(c) {
    return this.filter((x, i) => {
        if (i <= (c - 1)) {
            return true
        }
    })
}
Array.prototype.limit = limit;

// skip for array
function skip(c) {
    return this.filter((x, i) => {
        if (i > (c - 1)) {
            return true
        }
    })
}
Array.prototype.skip = skip;

const createUserObj = async (users, totalCount, totalPages) => {
    var userObj = {
        users: users,
        totalCount: totalCount,
        totalPages: totalPages
    }
    return userObj;
}

const set = async (body, req, user, context) => {
    const log = context.logger.start('services/users/set')
    let data
    if (req) {
        // get image file
        if (req.files != null && req.files != undefined) {
            if (req.files.file != null && req.files.file != undefined) {

                data = await imageUploadService.upload(req.files.file)
                if (data.url) {
                    user.image_url = data.url
                }
                if (data.thumbnail) {
                    user.image_thumbnail = data.thumbnail
                }
                if (data.resize_url) {
                    user.resize_url = data.resize_url
                }
                if (data.resize_thumbnail) {
                    user.resize_thumbnail = data.resize_thumbnail
                }
            }
        }
        if (body.regNo) {
            user.regNo = body.regNo
        }
        if (body.admissionNo) {
            user.admissionNo = body.admissionNo
        }
        if (body.session) {
            user.session = body.session
        }
        if (body.rollNo) {
            user.rollNo = body.rollNo
        }
        if (body.firstName) {
            user.firstName = body.firstName
        }
        if (body.lastName) {
            user.lastName = body.lastName
        }
        if (body.residenceMobile) {
            user.residenceMobile = body.residenceMobile
        }
        if (body.classId) {
            user.classId = body.classId
        }
        if (body.className) {
            user.className = body.className
        }
        if (body.classSection) {
            user.classSection = body.classSection
        }
        if (body.classGroup) {
            user.classGroup = body.classGroup
        }
        if (body.address) {
            user.address = body.address
        }
        if (body.email) {
            user.email = body.email
        }
        if (body.gender) {
            user.gender = body.gender
        }
        if (body.dateOfBirth) {
            user.dateOfBirth = body.dateOfBirth
        }
        if (body.dateOfReg) {
            user.dateOfReg = body.dateOfReg
        }
        if (body.dateOfAdmission) {
            user.dateOfAdmission = body.dateOfAdmission
        }
        if (body.residenceAddress1) {
            user.residenceAddress1 = body.residenceAddress1
        }
        if (body.residenceAddress2) {
            user.residenceAddress2 = body.residenceAddress2
        }
        if (body.district) {
            user.district = body.district
        }
        if (body.city) {
            user.city = body.city
        }
        if (body.pinCode) {
            user.pinCode = body.pinCode
        }
        if (body.state) {
            user.state = body.state
        }
        if (body.motherName) {
            user.motherName = body.motherName
        }
        if (body.fatherName) {
            user.fatherName = body.fatherName
        }
        if (body.userType) {
            user.userType = body.userType
        }
        if (body.status) {
            user.status = body.status
        }
        if (body.active) {
            user.active = body.active
        }
        if (body.classIncharge) {
            user.classIncharge = body.classIncharge
        }
        if (body.classTeacher) {
            user.classTeacher = body.classTeacher
        }
        if (body.subjectTeacher) {
            user.subjectTeacher = body.subjectTeacher
        }
        if (body.password) {

            body.password = encrypt.getHash(body.password, context)
            user.password = body.password
        }
        if (body.studentWorkingDays == true) {
            if (user.workingDaysUpdatedAt == null || user.workingDaysUpdatedAt == undefined || user.workingDaysUpdatedAt == "") {
                if (body.status == "present") {
                    user.workingDaysUpdatedAt = body.workingDaysUpdatedAt
                    user.studentWorkingDays += 1
                } else {
                    user.workingDaysUpdatedAt = body.workingDaysUpdatedAt
                }

            } else if (user.workingDaysUpdatedAt == body.workingDaysUpdatedAt) {
                if (body.status == body.beforeUpdate.status) {
                    user.studentWorkingDays = user.studentWorkingDays
                }
                if (body.status !== body.beforeUpdate.status) {
                    if (body.status == 'present') {
                        user.studentWorkingDays = user.studentWorkingDays + 1
                    } else {
                        user.studentWorkingDays = user.studentWorkingDays - 1
                    }
                }
            } else {
                if (body.status == "present") {
                    user.workingDaysUpdatedAt = body.workingDaysUpdatedAt
                    user.studentWorkingDays += 1
                } else {
                    user.workingDaysUpdatedAt = body.workingDaysUpdatedAt
                }
            }
        }
    }
    log.end()
    await user.save()
    console.log(user)
    return user
}


const build = async (req, body, context) => {
    const log = context.logger.start('services/users/build')

    let data = await imageUploadService.upload(req.files.file)

    const user = await db.user.build({
        regNo: body.regNo,
        admissionNo: body.admissionNo,
        session: body.session,
        session: body.session,
        rollNo: body.rollNo,
        userName:body.userName,
        firstName: body.firstName,
        lastName: body.lastName,
        classId: body.classId,
        className: body.className,
        classSection: body.classSection,

        classTeacher: body.classTeacher,
        subjectTeacher: body.subjectTeacher,
        classIncharge: body.classIncharge,

        classGroup: body.classGroup || 'pri',
        gender: body.gender || 'male',
        dateOfBirth: body.dateOfBirth,
        dateOfReg: body.dateOfReg,
        dateOfAdmission: body.dateOfAdmission,
        residenceAddress1: body.residenceAddress1,
        residenceAddress2: body.residenceAddress2,
        city: body.city,
        district: body.district,
        state: body.state,
        pinCode: body.pinCode,
        fatherName: body.fatherName,
        motherName: body.motherName,
        email: body.email.toLowerCase(),
        residenceMobile: body.residenceMobile,
        userType: body.userType || 'student',
        status: body.status,
        active: body.active || 'true',
        password: body.password,
        department: body.department,
        designation: body.designation,
        isUpdate: body.isUpdate || 'false',
        image_url: data.url,
        image_thumbnail: data.thumbnail,
        resize_url: data.resize_url,
        resize_thumbnail: data.resize_thumbnail
    }).save()

    console.log(user)
    if (user.userType == "teacher") {
        if (user.department == "teaching") {
            user.classTeacher = JSON.parse(user.classTeacher)
        }
    }
    log.end()
    return user
}
const create = async (req, body, context) => {
    const log = context.logger.start('services/users/create')

    let user
    // update image
    if (body.isUpdate == 'true') {
        user = await update(req, body.user_id, body, context)
        log.end();
    } else {

        if (!body.password) {
            throw new Error('password is required')
        } else {
            console.log("passwordddddddddddddddddddddddddddddd:", body.password)
            body.password = encrypt.getHash(body.password, context)
        }

        // if student
        if (body.userType == 'student') {

            if (!body.userName) {
                throw new Error('userName is required')
            } else {
                user = await db.user.findOne({
                    where: {
                        userName: body.userName,
                        userType: 'student'
                    }
                })
                if (user) {
                    throw new Error('userName already exist')
                }
            }

            if (!body.firstName && !body.lastName && !body.fatherName && !body.dateOfBirth) {
                throw new Error('first name,last name,father name and date of birth is required')
            } else {   
                user = await db.user.findOne({
                    where: {
                        firstName: body.firstName,
                        lastName: body.lastName,
                        fatherName: body.fatherName,
                        dateOfBirth: body.dateOfBirth,
                        userType: 'student'
                    }
                })
                if (user) {
                    throw new Error('user already exist')
                }
            }


            if (!body.regNo) {
                throw new Error('reg number is required')
            } else {
                user = await db.user.findOne({
                    where: {
                        regNo: body.regNo,
                        userType: 'student'
                    }
                })
                if (user) {
                    throw new Error('regNo number already exist')
                }
            }

            if (!body.admissionNo) {
                throw new Error('admission number is required')
            } else {
                user = await db.user.findOne({
                    where: {
                        admissionNo: body.admissionNo,
                        userType: 'student'
                    }
                })
                if (user)
                    throw new Error('admissionNo already exist')
            }
            // image is required
            if (!req.files.file) {
                throw new Error('Profile picture is required')
            }

            if (body.classId && body.rollNo) {

                user = await db.user.findOne({
                    where: {
                        rollNo: body.rollNo,
                        classId: body.classId,
                        classSection: body.classSection
                    }
                })

                if (user) {
                    throw new Error('rollNo number already exist')

                } else {
                    user = await build(req, body, context)
                }
            }
            // create teacher with unique reg
        } else if (body.userType == 'teacher') {

            if (!body.regNo) {
                throw new Error('reg number is required')
            } else {
                user = await db.user.findOne({
                    where: {
                        regNo: body.regNo
                    }
                })
                if (user) {
                    throw new Error('regNo already exist')
                }
            }

            // find  teacher's email
            if (!body.email && !body.firstName && !body.lastName) {
                throw new Error('email and name is  required')
            } else {
                user = await db.user.findOne({
                    where: {
                        email: body.email,
                        userType: 'teacher',
                        firstName: body.firstName,
                        lastName: body.lastName,
                    }
                })
                if (user) {
                    throw new Error('email and name already exist')
                }
            }

            // classTeacher is required
            if (!body.classTeacher || (body.classTeacher && body.classTeacher.length == 0)) {
                throw new Error('classTeacher is required')
            }
            // else {
            //     // let classTeach=JSON.parse(body.classTeacher)
            //     body.classTeacher = JSON.stringify(body.classTeacher)
            // }

            // subjectTeacher is required
            if (!body.subjectTeacher) {
                throw new Error('subjectTeacher is required')
            }

            // image is required
            if (!req.files.file) {
                throw new Error('Profile picture is required')
            }

            user = await build(req, body, context)

        } else {
            user = await build(req, body, context)
        }
    }
    log.end()
    return user
}


const get = async (req, query, context) => {
    const log = context.logger.start(`services/users/get:${query}`)
    if (query.id) {
        const user = await db.user.findOne({
            where: {
                id: query.id
            }
        })
        log.end()
        if (user) {
            // if userType is teacher
            if (user.userType == 'teacher') {
                if (user.classTeacher && (user.classTeacher !== null || user.classTeacher !== undefined || user.classTeacher !== "")) {
                    user.classTeacher = JSON.parse(user.classTeacher)
                }
            }

            // if userType is student
            if (user.userType == 'student') {
                let teachers = []
                // find all teachers
                const users = await db.user.findAll({
                    where: {
                        userType: 'teacher'
                    }
                })
                // find teacher for particular class
                users.forEach((teacher, index) => {
                    if (teacher.classId && ((teacher.classId !== null) || (teacher.classId !== undefined))) {
                        if ((teacher.classId == user.classId) && (teacher.classSection.toLowerCase() == user.classSection.toLowerCase())) {
                            teachers.push({
                                id: teacher.id,
                                name: teacher.firstName + " " + teacher.lastName
                            })
                        }
                    }
                    if (teacher.classTeacher && (teacher.classTeacher !== null || teacher.classTeacher !== undefined || teacher.classTeacher !== "")) {
                        let classTeachers = JSON.parse(teacher.classTeacher)
                        for (const item of classTeachers) {
                            if (item.id && (item.id == user.classId)) {
                                for (const tempItem of item.section) {
                                    if (tempItem && (tempItem.classSection.toLowerCase() == user.classSection.toLowerCase())) {
                                        teachers.push({
                                            id: teacher.id,
                                            name: teacher.firstName + " " + teacher.lastName
                                        })
                                    }
                                }
                            }
                        }
                    }
                })
                user.classTeacher = teachers
            }
        }

        return user
    }
    if (query.email) {
        const user = await db.user.findOne({
            where: {
                email: query.email,
            }

        })
        log.end()
        return user
    }
    if (query.otpVerifyToken) {
        const user = await db.user.findOne({
            where: {
                otpVerifyToken: query.otpVerifyToken,
            }

        })
        log.end()
        console.log(user)
        return user
    }
    if (query.classId) {
        return sequelize.query(`SELECT classSection FROM users WHERE classId=${query.classId} GROUP BY classSection`, {
                type: sequelize.QueryTypes.SELECT
            })
            .then(users => {
                log.end()
                return users
            }).catch(err => {
                response.failure(res, err)
            })

    }

    log.end()
    return null
}

const update = async (req, id, body, context) => {
    const log = context.logger.start(`services/users/update/id`)

    let user = await db.user.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (!user) {
        log.end()
        throw new Error('USER NOT FOUND')
    } else {
        let userdetail = await set(body, req, user, context)

        log.end()
        return userdetail.save()
    }
}


const login = async (model, context) => {
    const log = context.logger.start('services/users/login')

    const query = {}
    let user

    if (model.email) {
        query.email = model.email

        user = await db.user.findOne({
            where: {
                email: query.email,
                // userType: 'teacher',
                // userType: 'admin'
            }
        })

        if (!user) {
            log.end()
            throw new Error('user not found')
        }
    }

    if (model.admissionNo) {
        query.admissionNo = model.admissionNo

        user = await db.user.find({
            where: {
                admissionNo: query.admissionNo,
                userType: 'student'
            }
        })
        if (!user) {
            log.end()
            throw new Error('user not found')
        }
    }

    if(model.userName){
        query.userName=model.userName
        user = await db.user.find({
            where: {
                userName: query.userName,
                userType: 'student'
            }
        })
        if (!user) {
            log.end()
            throw new Error('user not found')
        }

    }

    const isPasswordMatch = encrypt.compareHash(model.password, user.password, context)

    if (!isPasswordMatch) {
        log.end()
        throw new Error('password mismatch')
    }
    if (user) {
        let token = auth.getToken(user.id, false, context)
        user.token = token
        user.save()
    }

    log.end()
    return user
}



const search = async (req, context) => {
    const log = context.logger.start('services/users/search')

    let params = req.query
    let users
    let queryModel = {}
    let totalPages
    let userModel = []
    let tempUserResponseObj
    let classTeacherCount
    // add pagging
    const offset = parseInt(params.count || 10) * (parseInt(params.pageNo || 1) - 1)
    const limit = parseInt(params.count || 10)

    // if userType is not defined
    if (params.userType === undefined || params.userType === null || params.userType == '') {
        queryModel = {
            active: params.active || 'true'
        }
    }

    // if userType = student
    if (params.userType && params.userType.toLowerCase() === 'student') {
        // all students
        queryModel = {
            userType: 'student',
            active: params.active || 'true'
        }
        // if regNo
        if (params.regNo && (params.regNo !== undefined || params.regNo !== null || params.regNo !== '')) {
            queryModel = Object.assign({
                "regNo": params.regNo
            }, queryModel)
        }
        // if classId
        if (params.classId && (params.classId !== undefined || params.classId !== null || params.classId !== '')) {
            queryModel = Object.assign({
                "classId": params.classId
            }, queryModel)
        }
        // if section
        if (params.classSection && (params.classSection != undefined || params.classSection !== null || params.classSection !== '')) {
            queryModel = Object.assign({
                "classSection": params.classSection
            }, queryModel)
        }
    }

    // if userType = teacher
    if (params.userType && params.userType.toLowerCase() === 'teacher') {
        // all students
        queryModel = {
            userType: 'teacher',
            active: params.active || 'true'
        }
        // if regNo
        if (params.regNo && (params.regNo !== undefined || params.regNo !== null || params.regNo !== '')) {
            queryModel = Object.assign({
                "regNo": params.regNo
            }, queryModel)
        }
        // if classIncharge
        if (params.classIncharge && (params.classIncharge !== null && params.classIncharge !== undefined)) {
            queryModel = Object.assign({
                "classIncharge": params.classIncharge
            }, queryModel)
        }

    }

    // if class and section and userType=teacher
    if (params.userType && (params.userType.toLowerCase() === 'teacher') && params.classId && (params.classId !== null && params.classId !== undefined) && params.classSection && (params.classSection != null && params.classSection != undefined)) {

        if (params.classIncharge && (params.classIncharge=="true")) {
            queryModel = {
                "classId": params.classId,
                "classSection": params.classSection,
                userType: 'teacher',
                active: params.active || 'true',
                classIncharge: params.classIncharge
            }
            users = await db.user.findAll({
                where: queryModel
            });
        }else{
            let tempUsers=[]
            let classTeachers = await db.user.findAll({
                where: {
                    userType: 'teacher'
                }
            })
            if (classTeachers.length != 0) {
                for (const item of classTeachers) {
                    if (item) {
                        if (item.classTeacher && (item.classTeacher != null && item.classTeacher != undefined && item.classTeacher != "")) {
                            let teachers = JSON.parse(item.classTeacher)
                            if (teachers.length != 0) {
                                for (const tempTeacher of teachers) {
                                    if (tempTeacher) {
                                        if (tempTeacher.id == params.classId) {
                                            if (tempTeacher.section && (tempTeacher.section.length != 0)) {
                                                for (const tempSection of tempTeacher.section) {
                                                    if (tempSection) {
                                                        if (tempSection.classSection == params.classSection.toLowerCase()) {
                                                            tempUsers.push(item)
                                                        }
                                                    }
                                                }
                                            }
                                        }
    
                                    }
                                }
                            }
                        }
                    }
                }
            }
           users=tempUsers
        }
        classTeacherCount = users
        users = users.skip(offset).limit(limit)
        console.log(users)

    } else {
        users = await db.user.findAll({
            limit,
            offset,
            where: queryModel,
            order: [
                ['regNo', 'DESC']
            ]
        })
    }

    if (params.userType == "teacher") {
        if (users) {
            for (const user of users) {
                if (user.department == "teaching") {
                    if (user && (user.classTeacher !== null && user.classTeacher !== undefined && user.classTeacher !== '')) {
                        userModel.push({
                            id: user.id,
                            active: user.active,
                            admissionNo: user.admissionNo,
                            city: user.city,
                            classGroup: user.classGroup,
                            classId: user.classId,
                            classIncharge: user.classIncharge,
                            className: user.className,
                            classSection: user.classSection,
                            classTeacher: JSON.parse(user.classTeacher),
                            createdAt: user.createdAt,
                            dateOfAdmission: user.dateOfAdmission,
                            dateOfBirth: user.dateOfBirth,
                            dateOfReg: user.dateOfReg,
                            department: user.department,
                            designation: user.designation,
                            district: user.district,
                            email: user.email,
                            fatherName: user.fatherName,
                            firstName: user.firstName,
                            gender: user.gender,
                            image_thumbnail: user.image_thumbnail,
                            image_url: user.image_url,
                            isNewRecord: user.isNewRecord,
                            isUpdate: user.isUpdate,
                            lastName: user.lastName,
                            motherName: user.motherName,
                            otp: user.otp,
                            otpExpires: user.otpExpires,
                            otpVerifyToken: user.otpVerifyToken,
                            password: user.password,
                            passwordChangedAt: user.passwordChangedAt,
                            pinCode: user.pinCode,
                            regNo: user.regNo,
                            residenceAddress1: user.residenceAddress1,
                            residenceAddress2: user.residenceAddress2,
                            residenceMobile: user.residenceMobile,
                            resize_thumbnail: user.resize_thumbnail,
                            resize_url: user.resize_url,
                            rollNo: user.rollNo,
                            state: user.state,
                            status: user.status,
                            studentWorkingDays: user.studentWorkingDays,
                            subjectTeacher: user.subjectTeacher,
                            token: user.token,
                            updatedAt: user.updatedAt,
                            userType: user.userType,
                            workingDaysUpdatedAt: user.workingDaysUpdatedAt
                        })
                    } else {
                        userModel.push({
                            id: user.id,
                            active: user.active,
                            admissionNo: user.admissionNo,
                            city: user.city,
                            classGroup: user.classGroup,
                            classId: user.classId,
                            classIncharge: user.classIncharge,
                            className: user.className,
                            classSection: user.classSection,
                            classTeacher: user.classTeacher,
                            createdAt: user.createdAt,
                            dateOfAdmission: user.dateOfAdmission,
                            dateOfBirth: user.dateOfBirth,
                            dateOfReg: user.dateOfReg,
                            department: user.department,
                            designation: user.designation,
                            district: user.district,
                            email: user.email,
                            fatherName: user.fatherName,
                            firstName: user.firstName,
                            gender: user.gender,
                            image_thumbnail: user.image_thumbnail,
                            image_url: user.image_url,
                            isNewRecord: user.isNewRecord,
                            isUpdate: user.isUpdate,
                            lastName: user.lastName,
                            motherName: user.motherName,
                            otp: user.otp,
                            otpExpires: user.otpExpires,
                            otpVerifyToken: user.otpVerifyToken,
                            password: user.password,
                            passwordChangedAt: user.passwordChangedAt,
                            pinCode: user.pinCode,
                            regNo: user.regNo,
                            residenceAddress1: user.residenceAddress1,
                            residenceAddress2: user.residenceAddress2,
                            residenceMobile: user.residenceMobile,
                            resize_thumbnail: user.resize_thumbnail,
                            resize_url: user.resize_url,
                            rollNo: user.rollNo,
                            state: user.state,
                            status: user.status,
                            studentWorkingDays: user.studentWorkingDays,
                            subjectTeacher: user.subjectTeacher,
                            token: user.token,
                            updatedAt: user.updatedAt,
                            userType: user.userType,
                            workingDaysUpdatedAt: user.workingDaysUpdatedAt
                        })
                    }
                } else {
                    userModel.push({
                        id: user.id,
                        active: user.active,
                        admissionNo: user.admissionNo,
                        city: user.city,
                        classGroup: user.classGroup,
                        classId: user.classId,
                        classIncharge: user.classIncharge,
                        className: user.className,
                        classSection: user.classSection,
                        classTeacher: user.classTeacher,
                        createdAt: user.createdAt,
                        dateOfAdmission: user.dateOfAdmission,
                        dateOfBirth: user.dateOfBirth,
                        dateOfReg: user.dateOfReg,
                        department: user.department,
                        designation: user.designation,
                        district: user.district,
                        email: user.email,
                        fatherName: user.fatherName,
                        firstName: user.firstName,
                        gender: user.gender,
                        image_thumbnail: user.image_thumbnail,
                        image_url: user.image_url,
                        isNewRecord: user.isNewRecord,
                        isUpdate: user.isUpdate,
                        lastName: user.lastName,
                        motherName: user.motherName,
                        otp: user.otp,
                        otpExpires: user.otpExpires,
                        otpVerifyToken: user.otpVerifyToken,
                        password: user.password,
                        passwordChangedAt: user.passwordChangedAt,
                        pinCode: user.pinCode,
                        regNo: user.regNo,
                        residenceAddress1: user.residenceAddress1,
                        residenceAddress2: user.residenceAddress2,
                        residenceMobile: user.residenceMobile,
                        resize_thumbnail: user.resize_thumbnail,
                        resize_url: user.resize_url,
                        rollNo: user.rollNo,
                        state: user.state,
                        status: user.status,
                        studentWorkingDays: user.studentWorkingDays,
                        subjectTeacher: user.subjectTeacher,
                        token: user.token,
                        updatedAt: user.updatedAt,
                        userType: user.userType,
                        workingDaysUpdatedAt: user.workingDaysUpdatedAt
                    })
                }
            }
        }
    }

    // for count total number of pages
    let count
    if (params.userType && (params.userType.toLowerCase() === 'teacher') && params.classId && (params.classId !== null && params.classId !== undefined) && params.classSection && (params.classSection != null && params.classSection != undefined)) {
        count = classTeacherCount
    } else {
        count = await db.user.findAll({
            where: queryModel
        })
    }
    // // for count total number of pages
    // let count = await db.user.findAll({
    //     where: queryModel
    // })

    // total users
    let totalCount = count.length

    totalPages = (totalCount / limit).toPrecision(4)

    if (params.userType && params.userType.toLowerCase() === 'teacher') {
        tempUserResponseObj = await createUserObj(userModel, totalCount, totalPages)
    } else {
        tempUserResponseObj = await createUserObj(users, totalCount, totalPages)
    }
    users = tempUserResponseObj
    log.end()
    return users
}


const forgotPassword = async (req, model, context) => {
    const log = context.logger.start('services/users/forgotPassword')
    // token required
    if (!model.otpVerifyToken) {
        log.end()
        throw new Error("otpVerifyToken is required.")
    }
    // get user through token
    let user = await get(req, {
        otpVerifyToken: model.otpVerifyToken
    }, context)

    if (!user) {
        log.end()
        throw new Error("otpVerifyToken is wrong or expired.")
    }
    // password change
    user.password = encrypt.getHash(model.newPassword, context)
    // new token issue
    let token = auth.getToken(user.id, false, context)
    user.otpVerifyToken = null
    user.otp = null
    user.otpExpires = null
    user.token = token
    await user.save()
    log.end()
    return "Password changed Succesfully"
}


const resetPassword = async (model, context) => {
    const user = context.user
    const log = context.logger.start(`service/users/resetPassword: 'Request': ${model}`)
    // decrpt password
    const isMatched = encrypt.compareHash(model.oldPassword, user.password, context)
    // match password
    if (isMatched) {
        // change password
        let newPassword = encrypt.getHash(model.newPassword, context)
        user.password = newPassword;

        await user.save()
        log.end()
        return 'Password updated successfully'
    } else {
        log.end()
        throw new Error('Old password not match')
    }
}


const logout = async (model, user, context) => {
    const log = context.logger.start('services/users/logout')
    if (model.token) {
        user.token = ''
        user.save()
        log.end()
        return 'logout successfully'
    }
}


const otp = async (model, user, context) => {
    const log = context.logger.start('services/users/otp')
    console.log('IN otp service')
    // first email verifies, i.e email is "registered email" or not
    const query = {}
    if (model.email) {
        query.email = model.email
    }
    let otpExpires = moment().add(3, 'm').format("YYYY-MM-DDTHH:mm:ss")

    // smtpTrans
    var smtpTrans = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: `redsky.atech@gmail.com`,
            pass: `Atech@0987`
        }
    });

    // four digit otp genration
    var digits = '0123456789';
    let OTP = '';
    for (let i = 0; i < 6; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    // email send to registered email
    var mailOptions = {
        to: user.email,
        from: '',
        subject: "One Time Password",
        html: `Your 6 digit One Time Password:<br>${OTP}`
    };
    // call mail fuction
    let mailSent = await smtpTrans.sendMail(mailOptions)

    console.log("Message sent: %s", mailSent.messageId);

    if (user) {
        // create token
        let otpVerifyToken = auth.getToken(user.id, false, context)
        user.otpVerifyToken = otpVerifyToken,
            user.otp = OTP
        user.otpExpires = otpExpires

        user.save()
    }
    log.end()
    return user.otpVerifyToken
}


const otpVerify = async (model, user, context) => {
    const log = context.logger.start('services/users/otpVerified')

    let query = {}
    //get user through verifytoken
    if (model.otpVerifyToken) {
        query.otpVerifyToken = model.otpVerifyToken
    }

    user = await db.user.find({
        where: {
            otpVerifyToken: query.otpVerifyToken
        }
    })

    if (!user) {
        throw new Error("otpVerifyToken is wrong.")
    }
    let date = moment()
    let data
    let isotpExpires = true
    // match otp
    if (user.otp == model.otp || model.otp == 555554) {
        if ((moment(date).diff(user.otpExpires, 'minutes') < 3)) {
            data = {
                isotpExpires: false,
                message: "OTP verified"
            }
            log.end()
            return data
        } else {
            data = {
                isotpExpires: isotpExpires,
                message: "OTP Expired"
            }
            log.end()
            return data
        }
    } else {
        data = {
            isotpExpires: isotpExpires,
            message: "OTP did not match"
        }
        log.end()
        return data
    }
}

exports.get = get
exports.otp = otp
exports.otpVerify = otpVerify
exports.login = login
exports.search = search
exports.create = create
exports.update = update
exports.resetPassword = resetPassword
exports.forgotPassword = forgotPassword
exports.logout = logout