'use strict'
// const fs = require('fs')
// const path = require('path')
// const appRoot = require('app-root-path')
// const fileStoreConfig = require('config').get('providers.file-store')
// const response = require('../exchange/response')


const set = async (model, entity, context) => {
    const log = context.logger.start('services/syllabus/set')

    if (model.file_url) {
        entity.file_url = model.file_url
    }
    log.end()
    await entity.save()

    return entity
}

const build = async (body, context) => {
    const log = context.logger.start('services/syllabus/build')

    const syllabus = await db.syllabus.build({
        classId: body.classId,
        file_url: body.file_url
    }).save()

    log.end()
    return syllabus
}



const create = async (body, context) => {
    const log = context.logger.start(`services/syllabus`)
    let syllabus

    // create syllabus
    syllabus = await build(body, context)
    let classes

    if (syllabus && syllabus.classId) {
        classes = await db.class.findOne({
            where: {
                id: syllabus.classId
            }
        })
        if (!classes) {
            throw new error('Class not found')
        }
        syllabus.className = classes.name
    }
    syllabus.save()

    log.end()
    return syllabus

}

const get = async (req, context) => {
    const log = context.logger.start(`services/syllabus/get`)
    let syllabus
    let params = req.query
    // get syllabus by classId
    if (params && (params.classId != null && params.classId != undefined)) {
        syllabus = await db.syllabus.findOne({
            where: {
                classId: params.classId
            }
        })
    } else {
        //find all syllabus 
        syllabus = await db.syllabus.findAll({})
    }

    log.end()
    return syllabus

}

const getById = async (query, context) => {
    const log = context.logger.start(`services/syllabus/getById/id ${query.id}`)
    if (query.id) {
        const syllabus = await db.syllabus.find({
            where: {
                id: query.id,
            }
        })
        log.end()
        return syllabus
    }
}

const update = async (body, context) => {
    const log = context.logger.start(`services/syllabus/updateOrCreate`)

    let syllabus
    const entity = await db.syllabus.findOne({
        where: {
            classId: body.classId
        }
    })

    if (!entity) {

        syllabus = await create(body, context)
    } else {

        // call set function
        syllabus = await set(body, entity, context)

    }

    log.end();
    return syllabus.save()

}

exports.create = create
exports.get = get
exports.getById = getById
exports.update = update