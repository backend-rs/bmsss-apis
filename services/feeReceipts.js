const auth = require('../permit/auth')
const response = require('../exchange/response')
const service = require('../api/fees')

const build = async (model, context) => {
    const log = context.logger.start('services/feeReceipts/build')
    const receipt = await db.feeReceipt.build({

        receiptNo: model.receiptNo,
        admissionNo: model.admissionNo,
        userRegId: model.userRegId,

        feeChargesId: model.feeChargesId,
        // feeChargesName: model.feeChargesName,

        dueAmount: model.dueAmount || 0,
        payAmount: model.payAmount || 0,
        balanceAmount: model.balanceAmount || 0,

        feeType: model.feeType || "school",

        remark: model.remark || "",
        transRemark: model.transRemark || "",
        transType: model.transType,
        transNo: model.transNo,
        bankName: model.bankName,
        transDate: new Date()

    })
    // receipt.transDate = 
    receipt.save()
    log.end()
    return receipt
}


const create = async (model, context) => {
    const log = context.logger.start('services/feeReceipts/create')

    const feeReceipt = await build(model, context)

    log.end()
    return feeReceipt
}

const get = async (req, context) => {
    const log = context.logger.start(`services/feeReceipts/get`)
    let receipt
    let params = req.query
    let receipts
    let tempReceipt = []
    if (params&&(params.admissionNo !== null || params.admissionNo != undefined)) {
        receipts = await db.feeReceipt.findAll({
            where: {
                admissionNo: params.admissionNo
            }
        })
        if (!receipts) {
            throw new Error('No receipt exist for this admission no')
        }
        for (const receipt of receipts) {
            if (receipt) {
                if (receipt.feeChargesId) {

                    // find headId in feeCharges to get headName/feeChargesName
                    let feeCharge = await db.feeCharge.findOne({
                        where: {
                            headId: receipt.feeChargesId
                        }
                    })
                    if (!feeCharge) {
                        throw new Error('feeCharges not found')
                    }
                    if (receipt.transType == "cash" || receipt.transType == "other") {
                        tempReceipt.push({
                            id: receipt.id,
                            receiptNo: receipt.receiptNo,
                            admissionNo: receipt.admissionNo,
                            userRegId: receipt.userRegId,
                            feeChargesId: receipt.feeChargesId,
                            feeCharges: feeCharge.headName,
                            feeType: receipt.feeType,
                            remark: receipt.remark,
                            transType: receipt.transType,
                            transDate: receipt.transDate,
                            dueAmount: receipt.dueAmount,
                            payAmount: receipt.payAmount,
                            balanceAmount: receipt.balanceAmount
                        })

                    } else {
                        tempReceipt.push({
                            id: receipt.id,
                            receiptNo: receipt.receiptNo,
                            admissionNo: receipt.admissionNo,
                            userRegId: receipt.userRegId,
                            feeChargesId: receipt.feeChargesId,
                            feeCharges: feeCharge.headName,
                            feeType: receipt.feeType,
                            remark: receipt.remark,
                            transType: receipt.transType,
                            transDate: receipt.transDate,
                            dueAmount: receipt.dueAmount,
                            payAmount: receipt.payAmount,
                            balanceAmount: receipt.balanceAmount,
                            bankName: receipt.bankName,
                            transRemark: receipt.transRemark
                        })
                    }

                }
            }
        }
        receipts = tempReceipt
    }

    log.end()
    return receipts

}

exports.create = create
exports.get = get


