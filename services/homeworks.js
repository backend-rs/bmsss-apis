'use strict'
var dateFormat = require('dateformat');
const response = require('../exchange/response')

const createTempHomeworkObj = async (homework, classes, subject) => {
    var homeworkObj = {
        id: homework.id,
        classId: homework.classId,
        className: classes.name,
        section: homework.section,
        subjectId: homework.subjectId,
        subjectName: subject.name,
        classWork: homework.classWork,
        homeWork: homework.homework
        // description: homework.description
    }

    return homeworkObj;
}
const set = async (model, entity, context) => {
    const log = context.logger.start('services/homeworks/set')
    if (model.classId) {
        entity.classId = model.classId
    }
    if (model.section) {
        entity.section = model.section
    }
    if (model.subjectId) {
        entity.subjectId = model.subjectId
    }
    if (model.homeWork) {
        entity.homeWork = model.homeWork
    }
    if (model.classWork) {
        entity.classWork = model.classWork
    }
    // if (model.description) {
    //     entity.description = model.description
    // }
    log.end()
    await entity.save()
    return entity
}

const build = async (model, newdate, context) => {
    const log = context.logger.start('services/subjects/build')
    const homework = await db.homework.build({
        classId: model.classId,
        section: model.section,
        subjectId: model.subjectId,
        classWork: model.classWork || '',
        homeWork: model.homeWork || '',
        // description: model.description,
        date: newdate
    }).save()
    log.end()
    return homework
}

const create = async (model, context, res) => {
    const log = context.logger.start('services/homeworks/create')
    let homework
    let classes
    let subject
    // find class
    if (model.classId) {
        classes = await db.class.findOne({
            where: {
                id: model.classId
            }
        })
        if (!classes) {
            throw new Error('Class not found')
        }
    }
    // find subject
    if (model.subjectId) {
        subject = await db.subject.findOne({
            where: {
                id: model.subjectId
            }
        })
        if (!subject) {
            throw new Error('Subject not found')
        }
    }

    // get date
    let dateObj = new Date()
    let date = dateObj.getDate()
    let month = dateObj.getMonth() + 1
    let year = dateObj.getFullYear()
    let newdate = date + "/" + month + "/" + year
    // create homwork
    if (model.classId && model.section && model.subjectId && newdate) {
        homework = await db.homework.findOne({
            where: {
                classId: model.classId,
                section: model.section,
                subjectId: model.subjectId,
                date: newdate
            }
        })
        if (!homework) {
            homework = await build(model, newdate, context)
            log.end()
        } else {
            res.message = 'homework already exists'
            log.end()
            return response.exists(res, '')
        }

    }
    console.log(homework);
    log.end()
    return homework
}

const getById = async (query, context) => {
    const log = context.logger.start(`services/homeworks/getById/id ${query.id}`)
    let subject
    let classes
    if (query.id) {
        const homework = await db.homework.find({
            where: {
                id: query.id,
            }
        })
        if (!homework) {
            throw new Error('Homework not found')
        } else {
            if (homework.classId) {
                classes = await db.class.findOne({
                    where: {
                        id: homework.classId
                    }
                })
            }
            if (homework.subjectId) {
                subject = await db.subject.findOne({
                    where: {
                        id: homework.subjectId
                    }
                })
            }

        }
        const tempHomeworkResponseObj = await createTempHomeworkObj(homework, classes, subject)
        log.end()
        return tempHomeworkResponseObj
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/homeworks/get`)
    let homework
    let classes
    let subject
    var homeworkObj = []
    let params = req.query
    if (params && (params.classId && (params.section == null && params.section == undefined) && (params.date == null && params.date == undefined))) {
        homework = await db.homework.findAll({
            where: {
                classId: params.classId
            }
        })
        log.end()
    } else if (params && (params.section && (params.classId == null && params.classId == undefined) && (params.date == null && params.date == undefined))) {
        homework = await db.homework.findAll({
            where: {
                section: params.section
            }
        })
        log.end()

    } else if (params && (params.date && (params.classId == null && params.classId == undefined) && (params.section == null && params.section == undefined))) {
        homework = await db.homework.findAll({
            where: {
                date: params.date
            }
        })
        log.end()

    } else if (params && (params.classId && params.section && (params.date == null && params.date == undefined))) {
        homework = await db.homework.findAll({
            where: {
                classId: params.classId,
                section: params.section
            }
        })
        log.end()

    } else if (params && (params.classId && params.date && (params.section == null && params.section == undefined))) {
        homework = await db.homework.findAll({
            where: {
                classId: params.classId,
                date: params.date
            }
        })
        log.end()

    } else if (params && (params.section && params.date && (params.classId == null && params.classId == undefined))) {
        homework = await db.homework.findAll({
            where: {
                section: params.section,
                date: params.date
            }
        })
        log.end()

    } else if (params && (params.classId && params.section && params.date)) {
        homework = await db.homework.findAll({
            where: {
                classId: params.classId,
                section: params.section,
                date: params.date
            }
        })
        if (!homework) {
            throw new Error('Homework not found')
        } else {
            for (const item of homework) {
                if (item) {
                    if (item.classId) {
                        classes = await db.class.findOne({
                            where: {
                                id: item.classId
                            }
                        })
                        console.log('className:', classes.name)
                    }
                    if (item.subjectId) {
                        subject = await db.subject.findOne({
                            where: {
                                id: item.subjectId
                            }
                        })
                        console.log('subject:', subject)
                    }
                    homeworkObj.push({
                        id: item.id,
                        classId: item.classId,
                        className: classes.name,
                        section: item.section,
                        subjectId: item.subjectId,
                        subjectName: subject.name,
                        classWork: item.classWork,
                        homeWork: item.homeWork
                        // description: item.description
                    })

                }
            }
            console.log(homeworkObj)
            homework = homeworkObj

        }
    } else {
        homework = await db.homework.findAll({})
    }
    log.end()
    return homework
}


const update = async (id, model, context) => {
    const log = context.logger.start(`services/homeworks/update/id`)

    let entity = await db.homework.find({
        where: {
            id: parseInt(id)
        }
    })

    let homework;

    if (!entity) {
        entity = await build(model, context)
    }

    homework = await set(model.body, entity, context)

    log.end();
    return homework

}

const deleteHomework = async (id, context, res) => {
    const log = context.logger.start(`services/homeworks/delete/id`)
    const homework = await db.homework.findOne({
        where: {
            id: parseInt(id)
        }
    })
    if (homework) {
        await db.homework.destroy({
            where: {
                id: parseInt(id)
            }
        })
    }

    log.end()

}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteHomework = deleteHomework