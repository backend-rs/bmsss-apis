'use strict'
var moment = require('moment')

const build = async (model, context) => {
    const log = context.logger.start('services/messages/build')
    let msges
    if (model.userType == 'student') {
        msges = await db.msg.build({
            userType: model.userType,
            classId: model.classId,
            classSection: model.classSection,
            userName: model.userName,
            askedBy: model.askedBy,
            askedTo: model.askedTo,
            sendDate: model.sendDate,
            question: model.question,
            date: model.date,
            replyDate: model.replyDate,

        }).save()
        if (model.askedBy) {
            let user = await db.user.findOne({
                where: {
                    id: model.askedBy,
                    classId: model.classId
                }
            })
            log.end()
            if (!user) {
                log.end()
                throw new Error('user not found')
            }
            msges.userName = user.firstName + " " + "" + user.lastName
        }
    } else {
        let tempName = context.user.firstName + " " + "" + context.user.lastName
        let tempAdminId = context.user.id
        let classes = []
        if (model.classes) {
            classes = model.classes
        }
        if (classes.length != 0) {
            for (let tempClass of classes) {
                if (tempClass) {
                    let sections = tempClass.sections
                    if (sections.length != 0) {
                        for (let tempSection of sections) {
                            if (tempSection) {
                                msges = await db.msg.build({
                                    userType: model.userType,
                                    classId: tempClass.classId,
                                    classSection: tempSection.classSection,
                                    userName: tempName,
                                    adminId: tempAdminId,
                                    askedBy: model.askedBy,
                                    askedTo: model.askedTo,
                                    sendDate: model.sendDate,
                                    question: model.question,
                                    date: model.date,
                                    replyDate: model.replyDate
                                }).save()
                            }
                        }
                    }
                }
            }
        }
    }
    msges.save()
    log.end();
    return msges
}

const set = async (model, entity, context) => {
    const log = context.logger.start('services/messages/set')

    if (model.repliedBy) {
        entity.repliedBy = model.repliedBy
    }
    if (model.answer) {
        entity.answer = model.answer
    }
    if (model.replyDate) {
        entity.replyDate = model.replyDate
    }
    if (model.askedTo) {
        entity.askedTo = model.askedTo
    }
    log.end()
    await entity.save()
    return entity
}

const create = async (model, context) => {
    const log = context.logger.start('services/messages/create')

    var tempDate = new Date();

    tempDate.setFullYear(tempDate.getUTCFullYear());
    tempDate.setMonth(tempDate.getUTCMonth());
    tempDate.setDate(tempDate.getUTCDate());
    tempDate.setHours(tempDate.getUTCHours() + 5);
    tempDate.setMinutes(tempDate.getUTCMinutes() + 30);
    tempDate.setMilliseconds(tempDate.getUTCMilliseconds());

    console.log("Converted time: " + tempDate);

    model.date = tempDate
    const msg = await build(model, context)

    log.end();
    return msg
}

const sendMsg = async (model, context) => {
    const log = context.logger.start(`services/messages/sendMsg/id`)
    let query = {}
    if (model.id) {
        query.id = model.id
    }
    const entity = await db.msg.findOne({
        where: {
            id: query.id
        }
    })
    if (!entity) {
        throw new Error('Invalid msg')
    }

    var tempDate = new Date();

    tempDate.setFullYear(tempDate.getUTCFullYear());
    tempDate.setMonth(tempDate.getUTCMonth());
    tempDate.setDate(tempDate.getUTCDate());
    tempDate.setHours(tempDate.getUTCHours() + 5);
    tempDate.setMinutes(tempDate.getUTCMinutes() + 30);
    tempDate.setMilliseconds(tempDate.getUTCMilliseconds());

    model.replyDate = tempDate

    let msges = await set(model, entity, context)

    if (model.repliedBy) {
        let user = await db.user.findOne({
            where: {
                id: model.repliedBy
            }
        })
        msges.teacherName = user.firstName + " " + "" + user.lastName
    }
    log.end();
    return msges.save()
}


const getById = async (query, context) => {
    const log = context.logger.start(`services/messages/getById ${query.id}`)
    if (query.id) {
        const msg = await db.msg.findOne({
            where: {
                id: query.id
            }
        })
        log.end();
        return msg
    }
}

const get = async (req, context) => {
    const log = context.logger.start(`services/messages/get`)
    let msg
    const params = req.query;
    console.log("paramsssssssssssssssssssss:", params)
    let adminUser = await db.user.findOne({
        where: {
            userType: "admin"
        }
    })


    // add pagging
    const offset = parseInt(params.count || 10) * (parseInt(params.pageNo || 1) - 1)
    const limit = parseInt(params.count || 10)
    if (params && (params.type == null && params.type == undefined) && (params.classId != null && params.classSection != null)) {

        // if messageStatus is not defined||messageStatus=today
        if (((params.messageStatus == null) && (params.messageStatus == undefined)) || (params.messageStatus && (params.messageStatus == 'today'))) {
            if (((params.isDescending == null) && (params.isDescending == undefined)) || (params.isDescending && (params.isDescending == "true"))) {
                msg = await db.msg.findAll({
                    where: {
                        $or: [{
                            askedTo: params.teacherId
                        }, {
                            adminId: adminUser.id
                        }],
                        classId: parseInt(params.classId),
                        classSection: params.classSection,
                        date: {
                            $gte: moment(new Date()).format('YYYY-MM-DD 00:00:01'),
                            $lte: moment(new Date()).format('YYYY-MM-DD 23:59:59')
                        }
                    },
                    limit,
                    offset,
                    order: [
                        ['updatedAt', 'DESC']
                    ]
                })
                console.log("msg........", msg)
            } else {
                msg = await db.msg.findAll({
                    where: {
                        $or: [{
                            askedTo: params.teacherId
                        }, {
                            adminId: adminUser.id
                        }],
                        classId: parseInt(params.classId),
                        classSection: params.classSection,
                        date: {
                            $gte: moment(new Date()).format('YYYY-MM-DD 00:00:01'),
                            $lte: moment(new Date()).format('YYYY-MM-DD 23:59:59')
                        }
                    },
                    limit,
                    offset,
                    order: [
                        ['updatedAt', 'ASC']
                    ]
                })
                console.log("msg........", msg)
            }
        } else if (params.messageStatus && (params.messageStatus == 'custom')) {
            if (params.date && ((params.date !== null) || (params.date !== undefined))) {
                if (((params.isDescending == null) && (params.isDescending == undefined)) || (params.isDescending && (params.isDescending == "true"))) {
                    msg = await db.msg.findAll({
                        where: {
                            $or: [{
                                askedTo: params.teacherId
                            }, {
                                adminId: adminUser.id
                            }],
                            classId: parseInt(params.classId),
                            classSection: params.classSection,
                            date: {
                                $gte: moment(params.date).format('YYYY-MM-DD 00:00:01'),
                                $lte: moment(params.date).format('YYYY-MM-DD 23:59:59')
                            }
                        },
                        limit,
                        offset,
                        order: [
                            ['updatedAt', 'DESC']
                        ]
                    })
                    console.log("msg........", msg)
                } else {
                    msg = await db.msg.findAll({
                        where: {
                            $or: [{
                                askedTo: params.teacherId
                            }, {
                                adminId: adminUser.id
                            }],
                            classId: parseInt(params.classId),
                            classSection: params.classSection,
                            date: {
                                $gte: moment(params.date).format('YYYY-MM-DD 00:00:01'),
                                $lte: moment(params.date).format('YYYY-MM-DD 23:59:59')
                            }
                        },
                        limit,
                        offset,
                        order: [
                            ['updatedAt', 'ASC']
                        ]
                    })
                    console.log("msg........", msg)
                }
            }
        } else if (params.messageStatus && (params.messageStatus == "sevenday")) {
            if (((params.isDescending == null) && (params.isDescending == undefined)) || (params.isDescending && (params.isDescending == "true"))) {

                msg = await db.msg.findAll({
                    where: {
                        $or: [{
                            askedTo: params.teacherId
                        }, {
                            adminId: adminUser.id
                        }],
                        classId: parseInt(params.classId),
                        classSection: params.classSection,
                        date: {
                            $lte: moment(new Date()).format('YYYY-MM-DD 00:00:01'),
                            $gte: moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD 23:59:59')
                        }
                    },
                    limit,
                    offset,
                    order: [
                        ['updatedAt', 'DESC']
                    ]
                })
                console.log("msg........", msg)
            } else {
                msg = await db.msg.findAll({
                    where: {
                        $or: [{
                            askedTo: params.teacherId
                        }, {
                            adminId: adminUser.id
                        }],
                        classId: parseInt(params.classId),
                        classSection: params.classSection,
                        date: {
                            $lte: moment(new Date()).format('YYYY-MM-DD 23:59:59'),
                            $gte: moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD 23:59:59')
                        }
                    },
                    limit,
                    offset,
                    order: [
                        ['updatedAt', 'ASC']
                    ]
                })
                console.log("msg........", msg)
            }
        }
    } else {
        if (params && (params.type == 'admin' && params.classId != null && params.classSection != null)) {
            msg = await db.msg.findAll({
                where: {
                    userType: params.type,
                    classId: parseInt(params.classId),
                    classSection: params.classSection
                },
                limit,
                offset,
                order: [
                    ['updatedAt', 'DESC']
                ]
            })
            console.log("msg........", msg)
        }
    }
    log.end()
    return msg
}

exports.create = create
exports.sendMsg = sendMsg
exports.getById = getById
exports.get = get