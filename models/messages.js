'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('msg', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userType: {
            type: DataTypes.ENUM,
            values: ['admin', 'student'],
            DEFAULT: 'student'
        },
        adminId: DataTypes.INTEGER,
        // classes: DataTypes.STRING,
        classSection: DataTypes.STRING,
        userName: DataTypes.STRING,
        teacherName: DataTypes.STRING,
        date: DataTypes.DATE,
        replyDate: DataTypes.DATE,
        classId: DataTypes.INTEGER,
        repliedBy: DataTypes.INTEGER,
        askedBy: DataTypes.INTEGER,
        askedTo: DataTypes.INTEGER,
        answer: DataTypes.STRING,
        question: DataTypes.STRING,
        updatedAt: DataTypes.DATE,
        createdAt: DataTypes.DATE
    })
}


// {
//     "userType": "admin",
//     "classes": [{
//         "classId": 1,
//         "sections": [{
//             "classSection": "A"
//         }, {
//             "classSection": "B"
//         }]
//     }, {
//         "classId": 2,
//         "sections": [{
//             "classSection": "A"
//         }]
//     }],
//     "question": "Submit your notebooks"
// }