'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('syllabus', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        classId:DataTypes.INTEGER,
        className:DataTypes.STRING,
        file_url: DataTypes.TEXT('long'),
        isUpdate:{
            type: DataTypes.ENUM,
            values: ['true','false'],
            DEFAULT: 'false'
        }
       
    })
}