'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('homework', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        classId:DataTypes.INTEGER,
        section:DataTypes.STRING,
        subjectId:DataTypes.INTEGER,
        classWork:DataTypes.STRING,
        homeWork:DataTypes.STRING,
        // description:DataTypes.STRING,
        date:DataTypes.STRING
    })
}



