'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('subject', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        classId:DataTypes.INTEGER,
        name:DataTypes.STRING,
        status:{
            type: DataTypes.ENUM,
            values: ['active','inactive','remove'],
            DEFAULT:'active'
        }
       
    })
}