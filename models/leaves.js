'use strict'
// var bcrypt = require('bcrypt-nodejs')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('leave', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true

        },
        leaveType: {
            type: DataTypes.ENUM,
            values: ['halfDay', 'fullDay'],
            DEFAULT: 'halfDay'
        },
        fullDayType: {
            type: DataTypes.ENUM,
            values: ['single', 'multiple'],
            DEFAULT: 'single'
        },
        fromDate: DataTypes.STRING,
        toDate: DataTypes.STRING,
        leaveToken: DataTypes.STRING,
        otp: DataTypes.STRING,
        reason: DataTypes.STRING,
        userId: DataTypes.INTEGER,
        date: DataTypes.STRING,
        status: {
            type: DataTypes.ENUM,
            values: ['pending', 'confirmed', 'rejected'],
            DEFAULT: 'pending'
        },
        rejectionReason: DataTypes.STRING
    })
}