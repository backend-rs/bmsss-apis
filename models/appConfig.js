
'use strict'
// var bcrypt = require('bcrypt-nodejs')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('appConfig', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
        appIncomeCutOffRate: DataTypes.STRING,
        accountBank: DataTypes.STRING,
        accountNo: DataTypes.STRING,
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
    })
}
