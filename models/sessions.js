
'use strict'
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('session', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        year: DataTypes.STRING
        // name:DataTypes.TEXT('long')

    })
}