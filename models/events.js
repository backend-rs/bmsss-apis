'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('event', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
        image_url: DataTypes.TEXT('long'),
        image_thumbnail: DataTypes.TEXT('long'),
        resize_url: DataTypes.TEXT('long'),
        resize_thumbnail: DataTypes.TEXT('long'),
        files: DataTypes.STRING,
        shouldImageUpdate:{
            type: DataTypes.ENUM,
            values: ['true','false'],
            DEFAULT: 'false'
        },
        isUpdate:{
            type: DataTypes.ENUM,
            values: ['true','false'],
            DEFAULT: 'false'
        }
    })

}