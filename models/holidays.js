
'use strict'
// var bcrypt = require('sbcrypt-nodejs')

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('holiday', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        isLeapYear:{
            type:DataTypes.BOOLEAN
        },
        year:{
            type: DataTypes.INTEGER,        
        },
        january: {
            type: DataTypes.TEXT('long'),        
        },
        february:{
            type:DataTypes.TEXT('long'),
        },
        march:{
            type:DataTypes.TEXT('long'),
        },
        april:{
            type:DataTypes.TEXT('long'),
        },
        may:{
            type:DataTypes.TEXT('long'),
        },
        june:{
            type:DataTypes.TEXT('long'),
                
        },
        july:{
            type:DataTypes.TEXT('long'),
        },
        august:{
            type:DataTypes.TEXT('long'),
        },
        september:{
            type:DataTypes.TEXT('long'),
        }, 
        october:{
            type:DataTypes.TEXT('long'),
        },
        november:{
            type:DataTypes.TEXT('long'),
        },
        december:{
            type:DataTypes.TEXT('long'),
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE
   
    })
}