'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('feeReceipt', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        receiptNo:DataTypes.INTEGER,
        admissionNo: DataTypes.STRING,
        userRegId:DataTypes.STRING,
        feeChargesId:DataTypes.INTEGER,
        feeType: {
            type: DataTypes.ENUM,
            values: ['school', 'hostel'],
            DEFAULT: 'school'
        },
        dueAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        payAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        balanceAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        remark:DataTypes.STRING,
        transRemark:DataTypes.STRING,
        transType:{
            type:DataTypes.ENUM,
            values:['cash','cheque','other'],
            DEFAULT:'cash'
        }, 
        transNo:{
            type:DataTypes.STRING,
        },
        transDate:{
            type:DataTypes.DATE

        },
        bankName:{
            type:DataTypes.STRING
        }

    })
}
