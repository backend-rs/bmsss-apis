'use strict'
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        regNo: {
            type: DataTypes.STRING,
            primaryKey: true,

        },
        admissionNo: {
            type: DataTypes.STRING,
        },

        userType: {
            type: DataTypes.ENUM,
            values: ['student', 'admin', 'teacher'],
            DEFAULT: 'student'
        },
        active: {
            type: DataTypes.ENUM,
            values: ['true', 'false'],
            DEFAULT: 'true'
        },
        status: DataTypes.STRING,
        userName:DataTypes.STRING,
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,

        classId: DataTypes.INTEGER,
        className: DataTypes.STRING,
        classSection: DataTypes.STRING,
        classGroup: {
            type: DataTypes.ENUM,
            values: ['pri', 'pre', 'mid', 'hig'],
            DEFAULT: 'pri'
        },
        gender: {
            type: DataTypes.ENUM,
            values: ['male', 'female', 'others'],
            DEFAULT: 'male'
        },
        dateOfBirth: DataTypes.DATEONLY,
        dateOfReg: DataTypes.DATEONLY,
        dateOfAdmission: DataTypes.DATEONLY,
        fatherName: DataTypes.STRING,
        motherName: DataTypes.STRING,
        email: DataTypes.STRING,
        residenceAddress1: DataTypes.STRING,
        residenceAddress2: DataTypes.STRING,
        city: DataTypes.STRING,
        district: DataTypes.STRING,
        state: DataTypes.STRING,
        pinCode: DataTypes.STRING,
        residenceMobile: DataTypes.STRING,
        password: DataTypes.STRING,
        token: DataTypes.STRING,
        otp: DataTypes.STRING,
        otpExpires: DataTypes.DATE,
        otpVerifyToken: DataTypes.STRING,
        rollNo: DataTypes.STRING,
        passwordChangedAt: DataTypes.DATE,
        department: {
            type: DataTypes.ENUM,
            values: ['teaching', 'non-teaching', 'others'],
            DEFAULT: 'teaching'
        },
        designation: {
            type: DataTypes.ENUM,
            values: ['lecturer', 'master', 'primary'],
            DEFAULT: 'lecturer'
        },
        classTeacher: DataTypes.STRING,
        subjectTeacher: DataTypes.STRING,
        classIncharge: {
            type: DataTypes.ENUM,
            values: ['true', 'false'],
            DEFAULT: 'false'
        },
        isUpdate: {
            type: DataTypes.ENUM,
            values: ['true', 'false'],
            DEFAULT: 'false'
        },
        image_url: DataTypes.TEXT('long'),
        image_thumbnail: DataTypes.TEXT('long'),
        resize_url: DataTypes.TEXT('long'),
        resize_thumbnail: DataTypes.TEXT('long'),
        studentWorkingDays: {
            type: DataTypes.INTEGER
        },
        workingDaysUpdatedAt: {
            type: DataTypes.STRING
        }
    })
}