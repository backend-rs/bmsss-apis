'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('document', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        file_url: DataTypes.TEXT('long'),
    })
}