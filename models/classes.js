'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('class', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: DataTypes.STRING,
        status:{
            type: DataTypes.ENUM,
            values: ['active','inactive','remove'],
            DEFAULT:'active'
        }
    })
}