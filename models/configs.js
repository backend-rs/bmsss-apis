'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('config', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dueId: DataTypes.INTEGER,
        organization:DataTypes.STRING,
        schoolWorkingDays:DataTypes.INTEGER,
        workingDaysUpdatedAt:DataTypes.STRING
    })
}