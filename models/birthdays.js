
module.exports = function (sequelize, DataTypes) {
    return sequelize.define('birthday', {
        id:{
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        birthDayList:{
            type:DataTypes.TEXT('long')
        },
        userType:{
            type:DataTypes.STRING
        },
        classId:{
            type:DataTypes.INTEGER
        },
    })
}