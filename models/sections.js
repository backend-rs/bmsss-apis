'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('section', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        classId: DataTypes.INTEGER,
        name: DataTypes.STRING,
        status: {
            type: DataTypes.ENUM,
            values: ['active', 'inactive'],
            DEFAULT: 'active'
        }
    })
}