'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('attendanceRecord', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        tableName: DataTypes.STRING
    })
}