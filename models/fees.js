'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('fee', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        dueId: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        feeChargesId: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        admissionNo: DataTypes.STRING,
        userRegId: DataTypes.STRING,
        dueAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        payAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        balanceAmount: {
            type: DataTypes.INTEGER,
            DEFAULT: 0
        },
        feeType: {
            type: DataTypes.ENUM,
            values: ['school', 'hostel'],
            DEFAULT: 'school'
        },
        month: DataTypes.STRING,
        status: {
            type: DataTypes.ENUM,
            values: ['complete', 'pending', 'none'],
            DEFAULT: 'pending'
        }

    })
}








































// module.exports = function (sequelize, DataTypes) {
//     return sequelize.define('fee', {
//         id: {
//             type: DataTypes.INTEGER,
//              primaryKey: true,
//              autoIncrement:true
//         },
//         dueId: {
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         receiptNo:{
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         feeChargesId:{
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         feeChargesName: DataTypes.STRING,

//         admissionNo: DataTypes.STRING,
//         userRegId: DataTypes.STRING,
//         dueAmount: {
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         payAmount:{
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         balanceAmount:{
//             type: DataTypes.INTEGER,
//             DEFAULT: 0
//         },
//         feeType: {
//             type: DataTypes.ENUM,
//             values: ['school', 'hostel'],
//             DEFAULT: 'school'
//         },
//         remark:DataTypes.STRING,
//         transRemark:DataTypes.STRING,

//         transType:{
//             type:DataTypes.ENUM,
//             values:['cash','cheque','other'],
//             DEFAULT:'cash'
//         }, 
//         transNo:{
//             type:DataTypes.STRING,
//         },
//         transDate:{
//             type:DataTypes.DATE

//         },
//         bankName:{
//             type:DataTypes.STRING
//         },

//         month: DataTypes.STRING,



//         status: {
//             type: DataTypes.ENUM,
//             values: ['complete', 'pending', 'none'],
//             DEFAULT: 'pending'
//         }

//     })
// }

