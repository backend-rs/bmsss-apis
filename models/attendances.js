'use strict'

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('attendance', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        userId:DataTypes.INTEGER,
        classId: DataTypes.INTEGER,
        section: DataTypes.STRING,
        name: DataTypes.STRING,
        rollNo: DataTypes.STRING,
        status: {
            type: DataTypes.ENUM,
            values: ['absent', 'leave', 'present'],
            DEFAULT: 'present'
        }
    })
}