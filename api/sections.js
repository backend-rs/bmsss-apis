'use strict'
const service = require('../services/sections')
const response = require('../exchange/response')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/sections`)
    try {
        const section = await service.create(req.body, req.context)
        log.end()
        return response.data(res, section)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const getById = async (req, res) => {
    const log = req.context.logger.start(`api/sections/getById/${req.params.id}`)
    try {
        const section = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, section)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/sections/get`)

    try {
        const section = await service.get(req, req.context)
        log.end()

        return response.data(res, section)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/sections/update/${req.params.id}`)
    try {
        const section = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, section)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update