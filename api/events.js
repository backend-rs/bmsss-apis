'use strict'
const service = require('../services/events')
const response = require('../exchange/response')
const mapper = require('../mappers/event')


const create = async (req, res) => {
    const log = req.context.logger.start(`api/events/create`)
    try {
        const event = await service.create(req, req.body, req.context)
        log.end()
        return response.success(res, event)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create event')
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/events/get`)

    try {
        const event = await service.get(req.context)
        log.end()

        return response.data(res, event)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/events/getById/${req.params.id}`)
    try {
        const event = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, event)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/events/update/${req.params.id}`)
    try {
        const event = await service.update(req,req.params.id, req.body, req.context)
        log.end()
        return response.data(res, event)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteFile=async(req,res)=>{
    const log = req.context.logger.start(`api/events/delete/${req.params.id}`)
    try{
        const event=await service.deleteFile(req.params.id,req.context,res)
        log.end()
        return response.data(res,event)

    }catch(err){
        log.error(err.message)
        log.end()
        return response.failure(res,err.message)
    }
}

const upload = async (req, res) => {
    try {
        let data = await service.upload(req.files.file)
        return response.data(res, data)
    } catch (err) {
        return response.failure(res, err.message)
    }

}



exports.create = create
exports.get = get
exports.getById=getById
exports.update=update
exports.deleteFile=deleteFile
exports.upload = upload