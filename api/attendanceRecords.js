'use strict'
const service = require('../services/attendanceRecords')
const response = require('../exchange/response')
const mapper = require('../mappers/attendanceRecord')

const create = async (req, res) => {
    // const log = req.context.logger.start(`api/attendanceRecords/create`)
    try {
        const attendanceRecord = await service.create(req.body, req.context)
        // log.end()
        return response.success(res, attendanceRecord)
    } catch (err) {
        // log.error(err.message)
        // log.end()
        return response.failure(res, 'Unable to create attendanceRecord')
    }
}

const search = async (req, res) => {
    const log = req.context.logger.start('api/attendanceRecord/search')
    try {
        const attendanceRecord = await service.search(req, req.context,res)
        log.end()
        return response.data(res, attendanceRecord)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/attendanceRecord/update`)
    try {
        const attendanceRecord = await service.update(req, req.body, req.context)
        log.end()
        return response.data(res, attendanceRecord)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create = create
exports.search = search
exports.update = update