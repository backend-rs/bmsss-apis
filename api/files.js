'use strict'
const service = require('../services/files')
const response = require('../exchange/response')
const mapper = require('../mappers/file')


const create = async (req, res) => {
    const log = req.context.logger.start(`api/files/create`)
    try {
        const file = await service.create(req, req.body, req.context)
        log.end()
        return response.success(res, file)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create event')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/files/getById/${req.params.id}`)
    try {
        const file = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, file)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getByEvent = async (req, res) => {
    const log = req.context.logger.start(`api/files/getByEvent/${req.query}`)
    try {
        const file = await service.getByEvent(req, req.context)
        log.end()
        return response.data(res, file)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/files/update/${req.params.id}`)
    try {
        const file = await service.update(req, req.params.id, req.body, req.context)
        log.end()
        return response.data(res, file)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteFile=async(req,res)=>{
    const log = req.context.logger.start(`api/files/delete/${req.params.id}`)
    try{
        const file=await service.deleteFile(req.params.id,req.context,res)
        log.end()
        return response.data(res,file)

    }catch(err){
        log.error(err.message)
        log.end()
        return response.failure(res,err.message)
    }
}

exports.create = create
exports.getById = getById
exports.getByEvent = getByEvent
exports.update = update
exports.deleteFile = deleteFile