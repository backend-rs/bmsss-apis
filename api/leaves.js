'use strict'
const service = require('../services/leaves')
const response = require('../exchange/response')
const mapper = require('../mappers/leave')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/leaves/create`)
    try {
        const message = await service.create(req.body, req.context)
        log.end()
        return response.success(res, message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/leaves/get/${req.params.id}`)
    try {
        const leaves = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, leaves)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const get = async (req, res) => {
    const log = req.context.logger.start(`api/leaves/get`)
    try {
        const leaves = await service.get(req, req.context)
        log.end()
        return response.data(res, leaves)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const verifyOtp = async (req, res) => {
    const log = req.context.logger.start(`api/leaves/verifyOtp`)
    try {
        const leave = await service.verifyOtp(req.body, req.context)
        log.end()
        return response.data(res, leave)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const update = async (req, res) => {
    const log = req.context.logger.start(`api/leaves/update/${req.params.id}`)
    try {
        const leave = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, leave)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


exports.create = create
exports.getById = getById
exports.get = get
exports.verifyOtp = verifyOtp
exports.update = update