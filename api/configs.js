'use strict'
const service = require('../services/configs')
const response = require('../exchange/response')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/configs`)
    try {
        const config = await service.create(req.body, req.context)
        log.end()
        return response.success(res, config)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const update = async (req, res) => {
    const log = req.context.logger.start(`api/configs/update/${req.params.id}`)
    try {
        const config = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, config)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/configs/get`)

    try {
        const configs = await service.get(req,req.context)
        log.end()

        return response.data(res, configs)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create=create
exports.update=update
exports.get=get