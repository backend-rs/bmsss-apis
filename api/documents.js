'use strict'
const service = require('../services/documents')
const response = require('../exchange/response')
// const mapper = require('../mappers/document')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/documents`)
    try {
        const document = await service.create(req, req.context)
        log.end()
        return response.success(res, document)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create document')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/documents/getById/${req.params.id}`)
    try {
        const document = await service.getById(req, {
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, document)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const upload = async (req, res) => {
    try {
        let data = await service.upload(req.files.file)
        return response.data(res, data)
    } catch (err) {
        return response.failure(res, err.message)
    }

}

exports.create = create
exports.getById = getById
exports.upload = upload