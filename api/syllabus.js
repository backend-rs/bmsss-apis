'use strict'
const service = require('../services/syllabus')
const response = require('../exchange/response')
const mapper = require('../mappers/syllabus')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/syllabus`)
    try {
        const syllabus = await service.create(req.body, req.context)
        log.end()
        return response.success(res, syllabus)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create Syllabus')
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/syllabus/get`)

    try {
        const syllabus = await service.get(req,req.context)
        log.end()

        return response.data(res, syllabus)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/syllabus/getById/${req.params.id}`)
    try {
        const syllabus = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, syllabus)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/syllabus/updateOrCreate`)
    try {
        const syllabus = await service.update( req.body, req.context)
        log.end()
        return response.data(res, syllabus)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.get = get
exports.getById = getById
exports.update = update
