'use strict'
const service = require('../services/feeCharges')
const response = require('../exchange/response')
const mapper = require('../mappers/feeCharges')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/feeCharges/create`)
    try {
        const message = await service.create(req.body, req.context)
        log.end()
        return response.success(res, message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'unable to create')
    }
}


const getById = async (req, res) => {
    const log = req.context.logger.start(`api/feeCharges/get/${req.params.id}`)
    try {
        const feeCharges = await service.getById({ id: req.params.id }, req.context)
        log.end()
        return response.data(res,feeCharges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const get = async (req, res) => {
    const log = req.context.logger.start(`api/feeCharges/get`)
    try {
        const feeCharges = await service.get(req.context)
        log.end()
        return response.data(res, feeCharges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/feeCharges/update/${req.params.id}`)
    try {
        const feeCharges = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res,feeCharges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteFeeCharges= async (req, res) => {
    const log = req.context.logger.start(`api/feeCharges/delete/${req.params.id}`)
    try {
        const feeCharges = await service.deleteFeeCharges(req.params.id, req.context, res)
        log.end()
        return response.data(res,feeCharges)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update=update
exports.deleteFeeCharges=deleteFeeCharges