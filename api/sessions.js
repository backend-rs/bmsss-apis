'use strict'
const service = require('../services/sessions')
const response = require('../exchange/response')
const mapper = require('../mappers/session')


const create = async (req, res) => {
    // const log = req.context.logger.start(`api/sessions/create`)
    try {
        const session = await service.create(req.body, req.context)
        // log.end()
        return response.success(res,session)
    } catch (err) {
        // log.error(err.message)
        // log.end()
        return response.failure(res, 'unable to createeeeee')
    }
}



const get = async (req, res) => {
    const log = req.context.logger.start(`api/sessions/get`)
    try {
        const session = await db.session.findAll()
        log.end()
        return response.data(res,mapper.toListModel(session))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create = create
exports.get=get