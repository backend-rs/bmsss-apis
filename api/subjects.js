'use strict'
const service = require('../services/subjects')
const response = require('../exchange/response')
const mapper = require('../mappers/subject')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/subjects/create`)
    try {
        const subject = await service.create(req.body, req.context)
        log.end()
        return response.success(res, subject)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/subjects/getById/${req.params.id}`)
    try {
        const subject = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, subject)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/syllabus/get`)

    try {
        const subject = await service.get(req, req.context)
        log.end()

        return response.data(res, subject)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/subjects/update/${req.params.id}`)
    try {
        const subject = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, subject)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteSubject = async (req, res) => {
    const log = req.context.logger.start(`api/subjects/delete/${req.params.id}`)
    try {
        const subject = await service.deleteSubject(req.params.id, req.context, res)
        log.end()
        return response.data(res, subject)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteSubject = deleteSubject