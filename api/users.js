'use strict'
const service = require('../services/users')
const response = require('../exchange/response')
const mapper = require('../mappers/user')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/users/create`)
    try {
        const message = await service.create(req, req.body, req.context)
        log.end()
        return response.success(res, mapper.toModel(message))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/users/getById/${req.params.id}`)
    try {
        const user = await service.get(req, {id:req.params.id }, req.context)
        log.end()
        return response.data(res, mapper.toModel(user))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/users/get`)
    try {
        let user
        if (req.query.classId) {
            user = await service.get(res, { classId: req.query.classId }, req.context)
        } else {
            user = await db.user.findAll()
        }
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const login = async (req, res) => {
    const log = req.context.logger.start('api/users/login')
    try {
        const user = await service.login(req.body, req.context)
        log.end()
        return response.authorized(res, mapper.toLoginModel(user), user.token)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/users/update/${req.params.id}`)
    try {
        const user = await service.update(req, req.params.id, req.body, req.context)
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const resetPassword = async (req, res) => {

    console.log('REQ::', req)
    const log = req.context.logger.start(`api/users/resetPassword: 'Request':oldPassword ${req.body.oldPassword} : newPassword: ${req.body.newPassword}`)
    try {
        const user = await service.resetPassword({
            oldPassword: req.body.oldPassword,
            newPassword: req.body.newPassword,
            token: req.context.user.token
        }, req.context)
        log.end()
        return response.success(res, user)
    } catch (err) {
        log.error(err)
        log.end()
        return response.failure(res, err.message)
    }
}


const logout = async (req, res) => {
    const log = req.context.logger.start('api/users/logout')
    try {
        const message = await service.logout({ token: req.context.user.token }, req.context.user, req.context)
        return response.success(res, message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


const search = async (req, res) => {
    const log = req.context.logger.start('api/users/search')

    try {
        const user = await service.search(req, req.context)
        log.end()
        return response.data(res, user)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const otp = async (req, res) => {
    const log = req.context.logger.start(`api/users/otp`)
    try {
        let user = await service.get(req, { email: req.body.email }, req.context)
        if (!user) {
            log.end()
            return response.failure(res, "The entered email donot match with registered email")
        }
        const message = await service.otp(req.body, user, req.context)
        res.message = "your one time password on email"
        log.end()
        return response.data(res, { otpVerifyToken: message })
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err)
    }
}

const otpVerify = async (req, res) => {
    const log = req.context.logger.start("api/users/otpVerify")

    try {
        const isOtpVerify = await service.otpVerify(req.body, req, req.context)
        if (!isOtpVerify.isotpExpires) {
            res.message = isOtpVerify.message
            log.end()
            return response.data(res, {})
        } else {
            log.error(isOtpVerify.message)
            log.end()
            return response.failure(res, isOtpVerify.message)
        }

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err)
    }
}

const forgotPassword = async (req, res) => {
    const log = req.context.logger.start("api/users/forgotPassword")
    try {
        const passwordChanged = await service.forgotPassword(req, req.body, req.context)
        res.message = passwordChanged
        log.end()
        return response.data(res, {})
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err)
    }
}



exports.getById = getById
exports.get = get
exports.login = login
exports.search = search
exports.forgotPassword = forgotPassword
exports.create = create
exports.otp = otp
exports.logout = logout
exports.otpVerify = otpVerify
exports.update = update
exports.resetPassword = resetPassword

