'use strict'
const service = require('../services/fees')
const response = require('../exchange/response')
 const mapper = require('../mappers/fee')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/fees/create`)
    try {
        const message = await service.create(req.body, req.context)
        log.end()
        return response.success(res,message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/fees/get/${req.params.id}`)
    try {
        const fees = await service.getById({ id: req.params.id }, req.context)
        log.end()
        return response.data(res, mapper.toModel(fees))
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const get = async (req, res) => {
    const log = req.context.logger.start(`api/fees/get`)
    try {
        const fees = await service.get(req,req.context)
        log.end()
        return response.data(res, fees)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/fees/update/${req.params.id}`)
    try {
        const fees = await service.update(req.params.id, req.body, req.context)
        log.end()
        return response.data(res, fees)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}



exports.create = create
exports.getById = getById
exports.get = get
exports.update = update