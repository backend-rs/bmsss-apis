'use strict'
const service = require('../services/holidays')
const response = require('../exchange/response')
const mapper = require('../mappers/holiday')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/holidays/create`)
    try {
        const message = await service.create(req.body, req.context)
        log.end()
        return response.success(res, message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'unable to create')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/holidays/getById/${req.params.id}`)
    try {
        const holidays = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, holidays)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/holidays/get`)

    try {
        const holidays = await service.get(req, req.context)
        log.end()

        return response.data(res, holidays)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}


const update = async (req, res) => {
    const log = req.context.logger.start(`api/holidays/update`)
    try {
        const holiday = await service.update(req.body, req.context)
        log.end()
        return response.data(res, holiday)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteHoliday = async (req, res) => {
    const log = req.context.logger.start(`api/deleteHoliday/delete`)
    try {
        const holiday = await service.deleteHoliday(req.query, req.context, res)
        log.end()
        return response.data(res, 'holiday deleted successfully')
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create = create
exports.update = update
exports.get = get
exports.getById = getById
exports.deleteHoliday = deleteHoliday