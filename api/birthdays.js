'use strict'
const service = require('../services/birthdays')
const response = require('../exchange/response')
const mapper = require('../mappers/birthday')


const create = async (req, res) => {
    const log = req.context.logger.start(`api/birthdays/create`)
    try {
        const message = await service.create(req.body, req.context)
        log.end()
        return response.success(res,message)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'unable to create')
    }
}
const getById = async (req, res) => {
    const log = req.context.logger.start(`api/birthdays/get/${req.params.id}`)
    try {
        const birthdays = await service.getById({ id: req.params.id }, req.context)
        log.end()
        return response.data(res,birthdays)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const get = async (req, res) => {
    const log = req.context.logger.start(`api/birthdays/get`)
    try {
        const birthdays = await service.get(req,req.context)
        log.end()
        return response.data(res, birthdays)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create=create
exports.getById = getById
exports.get = get