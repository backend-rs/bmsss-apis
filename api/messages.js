'use strict'
const service = require('../services/messages')
const response = require('../exchange/response')
const mapper = require('../mappers/message')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/messages/create`)
    try {
        const msges = await service.create(req.body, req.context)
        log.end()
        return response.success(res, msges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create msg')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/messages/getById/${req.params.id}`)
    try {
        const msges = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, msges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/messages/get`)

    try {
        const msges = await service.get(req,req.context)
        log.end()

        return response.data(res, msges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const sendMsg = async (req, res) => {
    const log = req.context.logger.start(`api/messages/sendMsg`)
    try {
        const msges = await service.sendMsg(req.body, req.context)
        log.end()
        return response.data(res, msges)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.sendMsg = sendMsg