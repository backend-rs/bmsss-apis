'use strict'

const service = require('../services/attendances')
const response = require('../exchange/response')
const mapper = require('../mappers/attendance')

const create = async (req, res) => {
    // const log = req.context.logger.start(`api/attendances/create`)
    try {
        const attendance = await service.create(req.body, req.context)
        // log.end()
        return response.success(res, attendance)

    } catch (err) {
        // log.error(err.message)
        // log.end()
        return response.failure(res, 'Unable to mark attendance')
    }
}

// const getById = async (req, res) => {
//     const log = req.context.logger.start(`api/attendances/getById/${req.params.id}`)
//     try {
//         const attendance = await service.getById({
//             id: req.params.id
//         }, req.context)
//         log.end()
//         return response.data(res, attendance)
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }

// const get = async (req, res) => {
//     const log = req.context.logger.start(`api/classes/get`)

//     try {
//         const attendance = await service.get(req,req.context)
//         log.end()
//         return response.data(res, attendance)
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }

// const update = async (req, res) => {
//     const log = req.context.logger.start(`api/attendances/update/${req.params.id}`)
//     try {
//         const attendance = await service.update(req.params.id, req, req.context)
//         log.end()
//         return response.data(res, attendance)
//     } catch (err) {
//         log.error(err.message)
//         log.end()
//         return response.failure(res, err.message)
//     }
// }

exports.create = create
// exports.get = get
// exports.getById = getById
// exports.update = update