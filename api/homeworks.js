'use strict'
const service = require('../services/homeworks')
const response = require('../exchange/response')
const mapper = require('../mappers/homework')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/homeworks/create`)
    try {
        const homework = await service.create(req.body, req.context,res)
        log.end()
        return response.success(res, homework)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create homework')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/homeworks/getById/${req.params.id}`)
    try {
        const homework = await service.getById({
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, homework)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/homeworks/get`)

    try {
        const homework = await service.get(req,req.context)
        log.end()

        return response.data(res, homework)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/homeworks/update/${req.params.id}`)
    try {
        const homework = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, homework)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
const deleteHomework = async (req, res) => {
    const log = req.context.logger.start(`api/homeworks/delete/${req.params.id}`)
    try {
        const homework = await service.deleteHomework(req.params.id, req.context, res)
        log.end()
        return response.data(res, homework)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteHomework=deleteHomework