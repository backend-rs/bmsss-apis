'use strict'
const service = require('../services/classes')
const response = require('../exchange/response')
const mapper = require('../mappers/class')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/classes/create`)
    try {
        const classes = await service.create(req.body, req.context)
        log.end()
        return response.success(res, classes)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create class')
    }
}

const getById = async (req, res) => {
    const log = req.context.logger.start(`api/classes/getById/${req.params.id}`)
    try {
        const classes = await service.getById(req,{
            id: req.params.id
        }, req.context)
        log.end()
        return response.data(res, classes)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const get = async (req, res) => {
    const log = req.context.logger.start(`api/classes/get`)

    try {
        const classes = await service.get(req,req.context)
        log.end()

        return response.data(res, classes)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const update = async (req, res) => {
    const log = req.context.logger.start(`api/classes/update/${req.params.id}`)
    try {
        const classes = await service.update(req.params.id, req, req.context)
        log.end()
        return response.data(res, classes)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

const deleteClass = async (req, res) => {
    const log = req.context.logger.start(`api/classes/delete/${req.params.id}`)
    try {
        const classes = await service.deleteClass(req.params.id, req.context, res)
        log.end()
        return response.data(res, classes)

    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}

exports.create = create
exports.getById = getById
exports.get = get
exports.update = update
exports.deleteClass=deleteClass