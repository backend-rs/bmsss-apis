'use strict'
const service = require('../services/feeReceipts')
const response = require('../exchange/response')
const mapper = require('../mappers/feeReceipt')

const create = async (req, res) => {
    const log = req.context.logger.start(`api/feeReceipts/create`)
    try {
        const receipts= await service.create(req.body, req.context)
        log.end()
        return response.success(res, receipts)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, 'Unable to create receipt')
    }
}
const get = async (req, res) => {
    const log = req.context.logger.start(`api/feeReceipt/get`)
    try {
        const receipt = await service.get(req,req.context)
        log.end()
        return response.data(res, receipt)
    } catch (err) {
        log.error(err.message)
        log.end()
        return response.failure(res, err.message)
    }
}
exports.create=create
exports.get=get