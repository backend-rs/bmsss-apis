module.exports = [{
        url: '/',
        get: {
            summary: 'get User List',
            description: 'get All Users',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/search',

        get: {
            summery: 'get user list by userType, className and classSection,',
            description: 'Get user list by userType, className and classSection,',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }, {
                    in: 'query',
                    name: 'pageNo',
                    description: 'pageNo',
                    required: true,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'count',
                    description: 'get number of user ',
                    required: false,
                    type: 'string'
                },{
                    in: 'query',
                    name: 'regNo',
                    description: 'registration number',
                    required: false,
                    type: 'string'
                }, {
                    in: 'query',
                    name: 'userType',
                    description: 'user type',
                    required: false,
                    type: 'string'
                }, {
                    in: 'query',
                    name: 'classId',
                    description: 'class id',
                    required: false,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'classSection',
                    description: 'classSection',
                    required: false,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'active',
                    description: 'active',
                    required: false,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'classIncharge',
                    description: 'classIncharge',
                    required: false,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'get User by Id',
            description: 'get User by Id',
            parameters: [{
                    in: 'path',
                    name: 'id',
                    description: 'userId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/otp',
        post: {
            summary: 'Send otp',
            description: 'Send otp through registered email',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'Model of User creation',
                required: true,
                schema: {
                    $ref: '#/definitions/otp'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/forgotPassword',
        put: {
            summary: 'forgotPassword',
            description: 'forgotPassword',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'Model of User creation',
                required: true,
                schema: {
                    $ref: '#/definitions/forgotPassword'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/otp/verify',
        post: {
            summary: 'Otp Verification',
            description: 'otp verification',
            parameters: [{
                in: 'query',
                name: 'otp',
                description: 'otp',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'otpVerifyToken',
                description: 'otpVerifyToken',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/resetPassword',
        post: {
            summary: 'resetPassword',
            description: 'resetPassword',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'Model of User creation',
                required: true,
                schema: {
                    $ref: '#/definitions/resetPassword'
                }
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/login',
        post: {
            summary: 'Login user',
            description: 'user login into system and get its token to access apis',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'Model of login user(email or userName)',
                required: true,
                schema: {
                    $ref: '#/definitions/login'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/logout',
        post: {
            summary: 'Logout user',
            description: 'user logout from system',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
]