module.exports=[
    {
        url: '/',
        post: {
            summary: 'Upload document',
            description: 'Upload document',
            parameters: [{
                "name": "file",
                "in": "formData",
                "description": "please choose a document",
                "required": true,
                "type": "file"
            },{
                in: 'body',
                name: 'body',
                description: 'create document',
                required: true,
                schema: {
                    $ref: '#/definitions/documentCreate'
                }
            },
            {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url:'/{id}',
        get:{
            summery:'get document by Id',
            description:'get document by Id',
            parameters:[{
                in:'path',
                name:'id',
                description:'documentId',
                required:true,
                type:'string'
            },
            {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses:{
                default:{
                    description:'Unexpected error',
                    schema:{
                        $ref:'#/definitions/Error'
                    }
                }
            }
        }
    }
]