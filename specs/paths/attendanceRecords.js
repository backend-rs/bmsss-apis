module.exports = [{
        url: '/search',
        get: {
            summery: 'get attendance record',
            description: 'get attendance record',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    type: 'string'
                }, {
                    in: 'query',
                    name: 'pageNo',
                    description: 'pageNo',
                    required: false,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'count',
                    description: 'get number of user ',
                    required: false,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'date',
                    description: 'date',
                    required: false,
                    type: 'integer',
                }, {
                    in: 'query',
                    name: 'classId',
                    description: 'classId',
                    required: false,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'section',
                    description: 'section',
                    required: false,
                    type: 'string'
                }, {
                    in: 'query',
                    name: 'rollNo',
                    description: 'rollNo',
                    required: false,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'monthYear',
                    description: 'monthYear',
                    required: false,
                    type: 'integer'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/update',
        put: {
            summary: 'Update attendance',
            description: 'update attendance record',
            parameters: [{
                in: 'query',
                name: 'date',
                description: 'date',
                type: 'integer'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId',
                type: 'integer'
            }, {
                in: 'query',
                name: 'section',
                description: 'section',
                type: 'string'
            }, {
                in: 'body',
                name: 'body',
                description: 'Model of attendance update',
                required: true,
                schema: {
                    $ref: '#/definitions/attendanceRecordUpdateReq'
                }
            }]
        }
    }
]