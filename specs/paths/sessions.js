module.exports = [
    {
        url: '/',
        post: {
            summary: 'session',
            description: 'session',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'send msg',
                required: true,
                schema: {
                    $ref: '#/definitions/session'
                }
            },

            {  in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
        },
    ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        get: {
            summery: 'Get Student',
            description: 'Get Student by session',
            parameters: [{
                in: 'query',
                name: 'sessionId',
                description: 'session',
                required: true,
                type: 'integer'
            }, 
            {
                in: 'query',
                name: 'year',
                description: 'year',
                required: true,
                type: 'string'
            },
            {  in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
        }, 
            //     in: 'query',
            //     name: 'count',
            //     description: 'get msg',
            //     required: true,
            //     type: 'string'
            // },

                // in: 'header',
                // name: 'x-access-token',
                // description: 'token to access api',
                // required: true,
                // type: 'string'
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    // {
    //     url: '/sendMsg',
    //     post: {
    //         summary: 'send reply',
    //         description: 'send reply',
    //         parameters: [{
    //             in: 'body',
    //             name: 'body',
    //             description: 'send reply',
    //             required: true,
    //             schema: {
    //                 $ref: '#/definitions/sendMsg'
    //             }
    //         }],
    //         responses: {
    //             default: {
    //                 description: 'Unexpected error',
    //                 schema: {
    //                     $ref: '#/definitions/Error'
    //                 }
    //             }
    //         }
    //     }
    // },
    // {
    //     url: '/{id}',
    //     get: {
    //         summery: 'get user by Id',
    //         description: 'get user by Id',
    //         parameters: [{
    //             in: 'path',
    //             name: 'id',
    //             description: 'get msg by id',
    //             required: true,
    //             type: 'integer'
    //         }],
    //         responses: {
    //             default: {
    //                 description: 'Unexpected error',
    //                 schema: {
    //                     $ref: '#/definitions/Error'
    //                 }
    //             }
    //         }

    //     }
    // },

]