module.exports = [{
        url: '/create',
        post: {
            summary: 'Create Class',
            description: 'Create Class',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'Model of Class creation',
                required: true,
                schema: {
                    $ref: '#/definitions/classCreate'
                }
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }


        }
    },
    {
        url: '/{id}',
        get: {
            summery: 'get class by Id',
            description: 'get class by Id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'classId',
                required: true,
                type: 'string'
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },
    {
        url: '/',
        get: {
            summery: 'get Classes list',
            description: 'get Classes list',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId',
                required: false,
                type: 'string'
            }, {
                in: 'query',
                name: 'status',
                description: 'status',
                required: false,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/update/{id}',
        put: {
            summary: 'Update Class',
            description: 'update class details',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }, {
                    in: 'path',
                    name: 'id',
                    description: 'classId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of class update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/classUpdateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    },
    {
        url: '/delete/{id}',
        delete: {
            summery: 'delete class by id',
            description: 'delete class by id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'classId',
                required: true,
                type: 'string'
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
]