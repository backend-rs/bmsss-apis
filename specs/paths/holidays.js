module.exports = [{
        url: '/',
        get: {
            summary: 'get holiday List',
            description: 'get All holidays',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'year',
                description: 'year ',
                type: 'string',
                required: false

            }, {
                in: 'query',
                name: 'month',
                description: 'month ',
                type: 'string',
                required: false

            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        // url: "/",
        put: {
            summary: 'add holiday',
            description: 'create holiday by month',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'create holiday',
                required: true,
                schema: {
                    $ref: '#/definitions/holidayCreate'
                }
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/delete',
        delete: {
            summery: 'delete holiday by id',
            description: 'delete holiday by id',
            parameters: [ {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'year',
                description: 'year ',
                type: 'string',
                required: true
            }, {
                in: 'query',
                name: 'month',
                description: 'month ',
                type: 'string',
                required: true
            },
            {
                in: 'query',
                name: 'id',
                description: 'holidayId',
                required: true,
                type: 'string'
            },],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }

]