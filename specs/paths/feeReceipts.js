module.exports = [
    {
        url: '/',


        url: '/',
        get: {
            summary: 'get User List',
            description: 'get All Users',
            parameters: [
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'admissionNo',
                    description: 'to get Receipt',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }


]