module.exports = [
    {
        url: '/',
        post: {
            summary: 'create fee',
            description: 'create fee',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'create fee',
                required: true,
                schema: {
                    $ref: '#/definitions/feeCreate'
                }
            }, {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        url: '/',
        get: {
            summary: 'get User List',
            description: 'get All Users',
            parameters: [
                {  in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
        },
            {
                in: 'query',
                name: 'admissionNo',
                description: 'admissionNo',
                required: false,
                type: 'string'
            }
        
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        put: {
            summary: 'fee update',
            description: 'fee update',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'fee update',
                required: true,
                schema: {
                    $ref: '#/definitions/feeUpdate'
                }
            }, {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },



        get: {
            summery: 'get fee by Id',
            description: 'get fee by Id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'feeId',
                required: true,
                type: 'string'
            }, {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },

]