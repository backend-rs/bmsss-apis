module.exports = [
    {
        url: '/',
        get: {
            summary: 'get syllabus List',
            description: 'get All syllabus List',
            parameters: [{
                in: 'query',
                name: 'classId',
                description: 'classId',
                // required: true,
                type: 'integer'
            },
            {  in: 'header',
            name: 'x-access-token',
            description: 'token to access api',
            required: true,
            type: 'string'
    }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/updateOrCreate',
        put: {
            summary: 'updateOrCreate syllabus',
            description: 'updateOrCreate syllabus',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'updateOrCreate syllabus',
                required: true,
                schema: {
                    $ref: '#/definitions/syllabusUpdateOrCreate'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
    

]