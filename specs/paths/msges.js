module.exports = [{
        url: '/',
        post: {
            summary: 'send msg',
            description: 'send msg',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string',
            }, {
                in: 'body',
                name: 'body',
                description: 'send msg',
                required: true,
                schema: {
                    $ref: '#/definitions/msgCreate'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        get: {
            summery: 'get msg list by classId, section,',
            description: 'Get msg list by classId and section',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'type',
                description: 'admin/teacher',
                required: false,
                type: 'string'
            }, {
                in: 'query',
                name: 'teacherId',
                description: 'teacherId',
                required: false,
                type: 'integer'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId',
                required: true,
                type: 'integer'
            }, {
                in: 'query',
                name: 'classSection',
                description: 'section',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'pageNo',
                description: 'get msg',
                required: true,
                type: 'integer'
            }, {
                in: 'query',
                name: 'count',
                description: 'get msg',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'isDescending',
                description: 'true/false',
                required: false,
                type: 'string'
            }, {
                in: 'query',
                name: 'messageStatus',
                description: 'today/sevenday/custom',
                required: false,
                type: 'string'
            }, {
                in: 'query',
                name: 'date',
                description: 'date',
                required: false,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/sendMsg',
        post: {
            summary: 'send reply',
            description: 'send reply',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'send reply',
                    required: true,
                    schema: {
                        $ref: '#/definitions/sendMsg'
                    }
                }

            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'get user by Id',
            description: 'get user by Id',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'path',
                name: 'id',
                description: 'get msg by id',
                required: true,
                type: 'integer'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },

]