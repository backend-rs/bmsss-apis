module.exports = [{
        url: '/',
        post: {
            summary: 'create leave',
            description: 'create Leave',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'create leave',
                    required: true,
                    schema: {
                        $ref: '#/definitions/leaveCreate'
                    }
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        get: {
            summery: 'get leave list by userId, name and rollNo,',
            description: 'Get leave list by userId and status',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string',
                }, {
                    in: 'query',
                    name: 'userId',
                    description: 'userId',
                    // required: true,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'status',
                    description: 'status ',
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'leaveType',
                    description: 'halfDay/fullDay ',
                    type: 'string'
                }

            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/verifyOtp',
        post: {
            summary: 'leave verification',
            description: 'leave verified or not',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'leave verified or not',
                required: true,
                schema: {
                    $ref: '#/definitions/verifyOtp'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summery: 'get user by Id',
            description: 'get user by Id',
            parameters: [{
                    in: 'path',
                    name: 'userId',
                    description: 'userId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },
    {
        url: '/update/{id}',
        put: {
            summary: 'Update Leave',
            description: 'update leave details',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }, {
                    in: 'path',
                    name: 'id',
                    description: 'leaveId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of leave update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/leaveUpdateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    }
]