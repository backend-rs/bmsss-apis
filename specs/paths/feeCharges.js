module.exports = [
    {
        url: '/',
        post: {
            summary: 'create feeCharges',
            description: 'create feeCharges',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'create feeCharges',
                required: true,
                schema: {
                    $ref: '#/definitions/createFeeCharges'
                }
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },

        url: '/',
        get: {
            summary: 'get feeCharges List',
            description: 'get All feeCharges',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        //    update by id
        put: {
            summary: 'fee update',
            description: 'fee update',
            parameters: [{
                in: 'body',
                name: 'body',
                description: 'fee update',
                required: true,
                schema: {
                    $ref: '#/definitions/update'
                }
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        // get by id
        get: {
            summary: 'get User by Id',
            description: 'get User by Id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'feeChargesId',
                required: true,
                type: 'string'
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }

    },
    {
        url: '/delete/{id}',
        delete: {
            summery: 'delete feeCharge by Id',
            description: 'delete feeCharge by Id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'feeChargeId',
                required: true,
                type: 'string'
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    }

]