module.exports = [{
        url: '/create',
        post: {
            summary: 'Create Subject',
            description: 'Create Subject',
            parameters: [{
                    in: 'body',
                    name: 'body',
                    description: 'Model of Subject creation',
                    required: true,
                    schema: {
                        $ref: '#/definitions/subjectCreateReq'
                    }
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }


        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'get subject by Id',
            description: 'get subject by Id',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'path',
                name: 'id',
                description: 'subjectId',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },
    {
        url: '/',
        get: {
            summary: 'get Subject list by status and classId',
            description: 'Get Subject list by status and classId',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'status',
                description: 'status',
                type: 'string'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId ',
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/update/{id}',
        put: {
            summary: 'Update Subject',
            description: 'update subject details',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }, {
                    in: 'path',
                    name: 'id',
                    description: 'subjectId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of subject update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/subjectUpdateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    },
    {
        url: '/delete/{id}',
        delete: {
            summary: 'delete subject by id',
            description: 'delete subject by id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'subjectId',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    }
]