module.exports = [{
        url: '/',
        get: {
            summery: 'get Section list by status and classId',
            description: 'Get Section list by status and classId',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'status',
                description: 'status',
                type: 'string'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId ',
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        },
        post: {
            summary: 'Create Section',
            description: 'Create Section',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'body',
                name: 'body',
                description: 'Model of Section creation',
                required: true,
                schema: {
                    $ref: '#/definitions/sectionCreateReq'
                }
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summary: 'get section by Id',
            description: 'get section by Id',
            parameters: [
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                },{
                in: 'path',
                name: 'id',
                description: 'sectionId',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },
    {
        url: '/update/{id}',
        put: {
            summary: 'Update Section',
            description: 'update section details',
            parameters: [{
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }, {
                    in: 'path',
                    name: 'id',
                    description: 'sectionId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of section update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/sectionUpdateReq'
                    }
                }
            ],
            responses: {
                default: {
                    description: {
                        schema: {
                            $ref: '#/definitions/Error'
                        }
                    }
                }
            }
        }
    }
    
]