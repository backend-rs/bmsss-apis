module.exports = [{
        url: '/create',
        post: {
            summary: 'Create Homework',
            description: 'Create Homework',
            parameters: [{
                    in: 'body',
                    name: 'body',
                    description: 'Model of Homework creation',
                    required: true,
                    schema: {
                        $ref: '#/definitions/homeworkCreate'
                    }
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/{id}',
        get: {
            summery: 'get homework by Id',
            description: 'get homework by Id',
            parameters: [{
                    in: 'path',
                    name: 'id',
                    description: 'homeworkId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    },
    {
        url: '/',
        get: {
            summery: 'get Homework list by classId, section and date',
            description: 'Get Homework list by classId, section and date',
            parameters: [{
                    in: 'query',
                    name: 'classId',
                    description: 'classId',
                    // required: true,
                    type: 'integer'
                }, {
                    in: 'query',
                    name: 'section',
                    description: 'section ',
                    type: 'string'
                },
                {
                    in: 'query',
                    name: 'date',
                    description: 'date ',
                    type: 'string'
                }, {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/update/{id}',
        put: {
            summery: 'Update homework',
            description: 'update homework details',
            parameters: [{
                    in: 'path',
                    name: 'id',
                    description: 'homeworkId',
                    required: true,
                    type: 'string'
                },
                {
                    in: 'body',
                    name: 'body',
                    description: 'Model of homework update',
                    required: true,
                    schema: {
                        $ref: '#/definitions/homeworkUpdate'
                    }
                },
                {
                    in: 'header',
                    name: 'x-access-token',
                    description: 'token to access api',
                    required: true,
                    type: 'string'
                }
            ],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },
    {
        url: '/delete/{id}',
        delete: {
            summery: 'delete homework by Id',
            description: 'delete homework by Id',
            parameters: [{
                in: 'path',
                name: 'id',
                description: 'homeworkId',
                required: true,
                type: 'string'
            }, {
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }

        }
    }
]