module.exports = [{
        url: '/',
        get: {
            summary: 'get birthDay List',
            description: 'get All users BirthDay List',
            parameters: [{
                in: 'header',
                name: 'x-access-token',
                description: 'token to access api',
                required: true,
                type: 'string'
            }, {
                in: 'query',
                name: 'classId',
                description: 'classId',
                // required: true,
                type: 'integer'
            }, {
                in: 'query',
                name: 'classSection',
                description: 'classSection',
                // required: true,
                type: 'string'

            }],
            responses: {
                default: {
                    description: 'Unexpected error',
                    schema: {
                        $ref: '#/definitions/Error'
                    }
                }
            }
        }
    },


]