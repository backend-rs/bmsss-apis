module.exports = [{
    name: 'sendMsg',
    properties: {
        id: {
            type: 'integer',
            default: ''
        },
        repliedBy: {
            type: 'integer',
            default: ''
        },
        answer: {
            type: 'string',
            default: ''
        },
    }
}]
