
module.exports = {
    attendance:[{
        userId:'integer',
        status:{
         type: 'string',
         enum: ['absent', 'leave', 'present']
        }
    }]
}
