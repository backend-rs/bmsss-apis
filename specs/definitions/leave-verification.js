module.exports = [{
    name: 'verifyOtp',
    properties: {
        otp: {
            type: 'string',
            default: ''
        },

        leaveToken: {
            type: 'string',
            default: ''
        },
        status: {
            type: 'string',
            enum: ['pending', 'confirmed', 'rejected'],
            default: 'pending'
        }
    },
}]
