module.exports = [{
    name: 'homeworkUpdate',
    properties: {
        classID: {
            type: 'integer',
            default: ''
        },
        section: {
            type: 'string',
            default: ''
        },
        subjectId: {
            type: 'integer',
            default: ''
        },
        classWork:{
            type:'string',
            default:''
        },
        homeWork:{
            type:'string',
            default:''
        }
        // description: {
        //     type: 'string',
        //     default: ''
        // }
    }
}]
