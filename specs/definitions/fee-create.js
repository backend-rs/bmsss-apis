module.exports = [{
    name: 'feeCreate',
    properties: {

        dueAmount: {
            type: 'integer',
            default: ''
        },
        month: {
            type: 'string',
            default: ''
        },

        feeChargesId: {
            type: 'integer',
            default: ''
        },

        admissionNo: {
            type: 'string',
            default: ''
        },
       
        status: {
            type: 'string',
            ENUM: ['complete', 'pending', 'none'],
            default: 'pending'
        },
        feeType: {
            type: 'string',
            ENUM: ['school', 'hostel'],
            default: 'school'
        }

    }
}]
