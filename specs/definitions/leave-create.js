module.exports = [{
    name: 'leaveCreate',
    properties: {
        userId: {
            type: 'integer',
            default: ''
        },
        leaveType: {
            type: 'string',
            enum: ['halfDay', 'fullDay'],
            default: 'halfDay'
        },
        fullDayType: {
            type: 'string',
            enum: ['single', 'multiple'],
            default: 'single'
        },
        fromDate: {
            type: 'string',
            default: ''
        },
        toDate: {
            type: 'string',
            default: ''
        },
        reason: {
            type: 'string',
            default: ''
        }
    }
}]