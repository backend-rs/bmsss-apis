module.exports = [{
    name: 'userCreate',
    properties: {
        regNo: {
            type: 'string',
            default: ''
        },
        admissionNo: {
            type: 'string',
            default: ''

        },
        firstName: {
            type: 'string',
            default: ''
        },
        lastName: {
            type: 'string',
            default: ''
        },
        userType: {
            type: 'string',
            enum: ['student', 'admin', 'teacher'],
            default:'student'
        },
        active: {
            type: 'string',
            enum: ['yes', 'no'],
            default: 'yes'
        },
        status: {
            type: 'string',
            default: ''
        },
        
        imageUrl: {
            type: 'string',
            default: ''
        },
        thumbnail: {
            type: 'string',
            default: ''
        },
        classId:{
            type: 'integer',
            default: ''
        },
        className:  {
            type: 'string',
            default: ''
        },
        classSection: {
            type: 'string',
            default: ''
        },
        classGroup: {
            type: 'string',
            enum: ['pri', 'pre', 'mid', 'hig'],
            default: 'pri'
        },
        gender: {
            type: 'string',
            enum: ['male', 'female', 'others'],
            default: 'male'
        },
        dateOfBirth:{
            type: 'dateonly',
            default: ''
        },
        dateOfReg: {
            type: 'dateonly',
            default: ''
        },
        dateOfAdmission: {
            type: 'dateonly',
            default: ''
        },
        fatherName: {
            type: 'string',
            default: ''
        },
        motherName:  {
            type: 'string',
            default: ''
        },
        email: {
            type: 'string',
            default: ''
        },
        residenceAddress1: {
            type: 'string',
            default: ''
        },
        residenceAddress2:  {
            type: 'string',
            default: ''
        },
        city:  {
            type: 'string',
            default: ''
        },
        district: {
            type: 'string',
            default: ''
        },
        state: {
            type: 'string',
            default: ''
        },
        pinCode: {
            type: 'string',
            default: ''
        },
        residenceMobile:  {
            type: 'string',
            default: ''
        },
        rollNo: {
            type: 'string',
            default: ''
        },
        // passwordChangedAt: DataTypes.DATE,
        // createdAt: DataTypes.DATE,
        // updatedAt: DataTypes.DATE


        
        department: {
            type: 'string',
            enum: ['teaching', 'non-teaching', 'others'],
            default: 'teaching'
        },
        designation: {
            type: 'string',
            enum: ['lecturer', 'master', 'primary'],
            default: 'lecturer'
        },
        password: {
            type: 'string',
            default: ''
        }
    }
}]
