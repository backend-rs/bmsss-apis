module.exports = [{
    name: 'msgCreate',
    properties: {
        userType: {
            type: 'string',
            enum: ['student', 'admin'],
            default: 'student'
        },
        classes: {
            type: 'string',
            default: ''
        },
        askedBy: {
            type: 'integer',
            default: ''
        },
        askedTo: {
            type: 'integer',
            default: ''
        },
        question: {
            type: 'string',
            default: ''
        },
        classId: {
            type: 'integer',
            default: ''
        },
        classSection: {
            type: 'string',
            defult: ''
        }
    }
}]