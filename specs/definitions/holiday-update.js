module.exports = [{
    name: 'holidayCreate',
    properties: {
        year: {
            type: 'string',
            default: ''
        },
        month: {
            type: 'integer'
        },
        fromDate: {
            type: 'string'
        },
        toDate: {
            type: 'string'
        },
        title: {
            type: 'string',
            default: ''
        },
        description: {
            type: 'string',
            default: ''
        },
        isEvent:{
            type:'string',
            default:'false'
        }
    }
}]