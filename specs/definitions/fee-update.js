module.exports = [{
    name: 'feeUpdate',
    properties: {
        feeChargesId: {
            type: 'integer',
            default: ''
        },
        admissionNo: {
            type: 'string',
            default: ''
        },
        payAmount: {
            type: 'integer',
            default: ''
        },
        dueAmount: {
            type: 'integer',
            default: ''
        },
        receiptNo:{
            type:'string',
            defaulf:''
        },
        remark:{
            type:'integer',
            defaulf:''
        },
        transRemark:{
            type:'string',
            defaulf:''
        },
        transType:{
            type:'string',
            ENUM:['cash','cheque','other'],
            default:'cash'
        },
        transNo:{
            type:'string',
            defaulf:''
        },
        bankName:{
            type:'string',
            defaulf:''
        }
        

    }
}]