module.exports = {
    status: {
        type: 'string',
        enum: ['pending', 'confirmed', 'rejected'],
        default: 'pending'
    },
    rejectionReason:{
        type: 'string',
        default: '' 
    }
}